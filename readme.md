[![Build Status](https://drone.io/bitbucket.org/Jerady/adm4ee/status.png)](https://drone.io/bitbucket.org/Jerady/adm4ee/latest)

# Welcome to **adm4ee**

A JavaFX based tool to manage Java EE Application Server.

## Features:

### Domain
* **Setup new domain** according to domain template folder (support for JavaScript based setup script(**Nashorn**)) 
* Configure the domain
* Start the domain
* Stop the domain
* Check if a domain is up and running
* Restart the domain automatically in case of failure

### JDBC Pool
* Reconfigure JDBC Pool settings: Username, Password, URL, Ping JDBC Pools

### EE Applications
* **Check application** version against remote update server
* **Update application** with new version from remote update server (support for update script)

### Misc
* **Configure Service Ports** (e.g. for HTTP listener, admin listener,...) with PortBase support
* **Command Line Interface** for usage during scripted installation procedure
* Settings are stored in a config-File (e.g. URL for Online-Updates, preferred GlassFish Ports, ...)
* Change admin password
* Show and save server log file
* **Prepared for other JavaEE-Servers** (e.g. WildFly)

### GUI
* **JavaFX based graphical user interface** for full control
* **CDI support** (WELD-se)
