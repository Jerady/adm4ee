/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee;

/**
 *
 * @author Jens Deters
 */
public class ApplicationInfo {
    public static final String APPLICATION_NAME = "adm4ee";
    public static final String APPLICATION_VERSION = "1.0.2";
    public static final String APPLICATION_COPYRIGHT = "(c) copyright 2013-2014 Jens Deters";
    public static final String APPLICATION_CLI_NAME = "adm4ee";
    public static final String APPLICATION_LOGGER = "org.jfx4ee.adm4ee";
}
