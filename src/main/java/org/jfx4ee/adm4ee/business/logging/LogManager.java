/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.logging;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfx4ee.adm4ee.ApplicationInfo;

/**
 *
 * @author Jens Deters
 */
public class LogManager {

    public static final String PARENT_LOGGER = ApplicationInfo.APPLICATION_LOGGER;
    public static final String DEFAULT_LOG_FILE_NAME = ApplicationInfo.APPLICATION_CLI_NAME + ".log";
    
    public static void initLogging(Level logLevel) {
        // Get the global logger to configure it
        Logger log = Logger.getLogger(PARENT_LOGGER);

        log.setLevel(logLevel);
        if (!Level.OFF.equals(logLevel)) {
            FileHandler logFileHandler;
            try {
                logFileHandler = new FileHandler(DEFAULT_LOG_FILE_NAME, 10000000, 1, true);
                logFileHandler.setFormatter(new CustomLogFormatter());
                logFileHandler.setLevel(logLevel);
                log.addHandler(logFileHandler);
            } catch (IOException | SecurityException ex) {
                ex.printStackTrace();
                throw new RuntimeException("Could not initialize logging! " + ex);
            }
        }
    }

}
