/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.logging;

/*
 Copyright 2008 Dino Tsoumakis

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Formats a LogRecord into one compact line.
 *
 * @author Dino Tsoumakis
 */
public class CustomLogFormatter extends Formatter {
    /* --------------------------- Constants ---------------------------- */

    private static final HashMap<String, String> LEVEL_TO_CODE = new HashMap<>();

    static {
        LEVEL_TO_CODE.put("SEVERE", "ERR");
        LEVEL_TO_CODE.put("WARNING", "WRN");
        LEVEL_TO_CODE.put("INFO", "INF");
        LEVEL_TO_CODE.put("CONFIG", "CFG");
        LEVEL_TO_CODE.put("FINE", "FNE");
        LEVEL_TO_CODE.put("FINER", "FNR");
        LEVEL_TO_CODE.put("FINEST", "FNT");
    }
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    /* --------------------------- Attributes --------------------------- */
    private Date currentDate = new Date();

    /* ---------------------------- Methods ----------------------------- */
    /**
     * Returns a 3 character shortcut ID for the given log level.
     *
     * @param level log Level
     * @return String
     */
    public static String getLevelCode(Level level) {
        if (level == null) {
            return "";
        }
        if (!LEVEL_TO_CODE.containsKey(level.getName())) {
            return "";
        }
        return LEVEL_TO_CODE.get(level.getName());
    }

    /**
     * Format the given LogRecord.
     *
     * @param record the log record to be formatted.
     * @return String with formatted version of the log record
     */
    @Override
    public synchronized String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();

        // Minimize memory allocations here.
        currentDate.setTime(record.getMillis());
        sb.append("[").append(FORMATTER.format(currentDate)).append("]");
        sb.append("[").append(getLevelCode(record.getLevel())).append("]");

        // Source
        if (record.getSourceClassName() != null) {
            String source = record.getSourceClassName();
            sb.append("[").append(source.substring(source.lastIndexOf(".") + 1));
            if (record.getSourceMethodName() != null) {
                sb.append(".").append(record.getSourceMethodName()).append("()");
            }
            sb.append("]  ");
        }

        // Message
        String sMessage = formatMessage(record);
        sb.append(sMessage);

        sb.append(LINE_SEPARATOR);
        if (record.getThrown() != null) {
            try {
                try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {
                    record.getThrown().printStackTrace(pw);
                    sb.append(sw.toString());
                }
            } catch (IOException ex) {
                return "Could not create log message: " + ex.getMessage();
            }
        }
        return sb.toString();
    }
}
