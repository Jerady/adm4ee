/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.configuration.entity;

import java.io.Serializable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Jens Deters
 */
public class UIConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;
    private StringProperty lastChoosenDirectoryProperty;
    private BooleanProperty startupWithDefaultDomainProperty;
    private final String appLogoName;
    private final String glassfishLogoName;
    private final String wildflyLogoName;
    private final String jbossLogoName;
    private final String iconEmSize;
    private final String releaseNotesFileName;
    private final String licenseFileName;
    private final String thirdPartyLicenseFileName;
    private String defaultDomainName;
    private int updateInterval;
    private boolean autoUpdate;
    private int logFileCharacterCount;
    private boolean firstStart;

    public UIConfiguration() {
        iconEmSize = "2.8em";
        appLogoName = "/images/adm4ee.png";
        glassfishLogoName = "/images/glassfish_logo.png";
        wildflyLogoName = "/images/wildfly_logo.png";
        jbossLogoName = "/images/jboss_logo.png";
        releaseNotesFileName = "/license/RELEASE-NOTES.txt";
        licenseFileName = "/license/LICENSE.txt";
        thirdPartyLicenseFileName = "/license/THIRD-PARTY.txt";
        firstStart = true;
    }

    public void createDefaultConfig() {
        setLastChoosenDirectory(System.getProperty("user.home"));
        setUpdateInterval(10);
        setAutoUpdate(true);
        setLogFileCharacterCount(5000);
    }

    public String getAppLogoName() {
        return appLogoName;
    }

    public String getWildflyLogoName() {
        return wildflyLogoName;
    }
    
    public String getGlassfishLogoName() {
        return glassfishLogoName;
    }
    
    public String getJBossLogoName() {
        return jbossLogoName;
    }

    public String getIconEmSize() {
        return iconEmSize;
    }

    public boolean isAutoUpdate() {
        return autoUpdate;
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    public int getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(int updateInterval) {
        this.updateInterval = updateInterval;
    }

    public int getLogFileCharacterCount() {
        return logFileCharacterCount;
    }

    public void setLogFileCharacterCount(int logFileCharacterCount) {
        this.logFileCharacterCount = logFileCharacterCount;
    }

    /*
     * Properties Access Methods
     * 
     */
    public StringProperty lastChoosenDirectoryProperty() {
        if (lastChoosenDirectoryProperty == null) {
            lastChoosenDirectoryProperty = new SimpleStringProperty();
        }
        return lastChoosenDirectoryProperty;
    }

    public String getLastChoosenDirectory() {
        return lastChoosenDirectoryProperty().getValue();
    }

    public void setLastChoosenDirectory(String choosenDirectory) {
        lastChoosenDirectoryProperty().setValue(choosenDirectory);
    }

    

    public BooleanProperty startupWithDefaultDomainProperty() {
        if (startupWithDefaultDomainProperty == null) {
            startupWithDefaultDomainProperty = new SimpleBooleanProperty();
        }
        return startupWithDefaultDomainProperty;
    }

    public Boolean isStartupWithDefaultDomain() {
        return startupWithDefaultDomainProperty().getValue();
    }

    public Boolean getStartupWithDefaultDomain() {
        return startupWithDefaultDomainProperty().getValue();
    }

    public void setStartupWithDefaultDomain(Boolean startupWithDefaultDomain) {
        startupWithDefaultDomainProperty().setValue(startupWithDefaultDomain);
    }

    public String getDefaultDomainName() {
        return defaultDomainName;
    }

    public void setDefaultDomainName(String defaultDomainName) {
        this.defaultDomainName = defaultDomainName;
    }

    public String getThirdPartyLicenseFileName() {
        return thirdPartyLicenseFileName;
    }

    public String getReleaseNotesFileName() {
        return releaseNotesFileName;
    }

    public String getLicenseFileName() {
        return licenseFileName;
    }

    public boolean isFirstStart() {
        return firstStart;
    }

    public void setFirstStart(boolean firstStart) {
        this.firstStart = firstStart;
    }
    
    
}
