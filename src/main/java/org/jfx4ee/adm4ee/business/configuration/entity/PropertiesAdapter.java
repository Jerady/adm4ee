package org.jfx4ee.adm4ee.business.configuration.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.jfx4ee.adm4ee.business.configuration.entity.PropertiesAdapter.MapType;

/**
 *
 * @author Jens Deters
 */
public class PropertiesAdapter extends XmlAdapter<MapType, Map<String, String>> {

    @Override
    public MapType marshal(Map<String, String> map) {
        MapType mapType = new MapType();
        map.entrySet().stream().map((entry) -> {
            MapEntry mapEntry = new MapEntry();
            mapEntry.name = entry.getKey();
            mapEntry.value = entry.getValue();
            return mapEntry;
        }).forEach((mapEntry) -> {
            mapType.entryList.add(mapEntry);
        });
        return mapType;
    }

    @Override
    public Map<String, String> unmarshal(MapType type) throws Exception {
        Map<String, String> map = new HashMap<>();
        type.entryList.stream().forEach((entry) -> {
            map.put(entry.name, entry.value);
        });
        return map;
    }

    static class MapType {
        @XmlElement(name = "property")
        public List<MapEntry> entryList = new ArrayList<>();
    }

    static class MapEntry {
        @XmlAttribute
        public String name;
        @XmlValue
        public String value;
    }
}
