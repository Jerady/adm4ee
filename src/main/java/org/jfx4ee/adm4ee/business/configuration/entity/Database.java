
package org.jfx4ee.adm4ee.business.configuration.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Jens Deters
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Database implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String DEFAULT_PING_ACTION_NAME = "Ping";
    public static final String PROPERTY_NAME_URL = "url";
    public static final String PROPERTY_NAME_DRIVER = "driver";
    public static final String PROPERTY_NAME_USERNAME = "username";
    public static final String PROPERTY_NAME_PASSWORD = "password";

    String name;
    String installPath;
    String pingAction;
    @XmlJavaTypeAdapter(PropertiesAdapter.class)
    Map<String, String> properties = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstallPath() {
        return installPath;
    }

    public void setInstallPath(String installPath) {
        this.installPath = installPath;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperty(String name, String value) {
        properties.put(name, value);
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    public String getPingAction() {
        if (pingAction == null) {
            return DEFAULT_PING_ACTION_NAME;
        }
        return pingAction;
    }

    public void setPingAction(String pingAction) {
        this.pingAction = pingAction;
    }

    @Override
    public String toString() {
        return "Database{name=" + name + ", installPath=" + installPath + "}";
    }


}
