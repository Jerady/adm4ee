/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.configuration.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;

/**
 *
 * @author Jens Deters
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {
    private static final long serialVersionUID = 1L;

    String url;
    String domainName;
    ServerType serverType;
    String serverRootPath;
    String domainUser;
    String domainPassword;
    String template;
    boolean ssl;
    boolean defaultDomain;
    @XmlElementWrapper(name = "applications")
    @XmlElement(name="application")
    List<Application> applications = new ArrayList<>();
    @XmlElementWrapper(name = "databases")
    @XmlElement(name="database")
    List<Database> databases = new ArrayList<>();

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public ServerType getServerType() {
        return serverType;
    }

    public void setServerType(ServerType serverType) {
        this.serverType = serverType;
    }

    public String getServerRootPath() {
        return serverRootPath;
    }

    public void setServerRootPath(String serverRootPath) {
        this.serverRootPath = serverRootPath;
    }

    public String getDomainUser() {
        return domainUser;
    }

    public void setDomainUser(String domainUser) {
        this.domainUser = domainUser;
    }

    public String getDomainPassword() {
        return domainPassword;
    }

    public void setDomainPassword(String domainPassword) {
        this.domainPassword = domainPassword;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public boolean hasLocalConfiguration() {
        return serverRootPath != null && domainName != null;
    }

    public boolean hasRemoteConfiguration() {
        return url != null && domainUser != null && domainPassword != null;
    }

    public boolean isDefaultDomain() {
        return defaultDomain;
    }

    public void setDefaultDomain(boolean defaultDomain) {
        this.defaultDomain = defaultDomain;
    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }

    /**
     * Get the application configuration with the specified application name
     * @param applicationName The name of the application
     * @return the application configuration, or null if there is no application with that name
     */
    public Application getApplication(String applicationName) {
        if (applications != null) {
            for (Application application : applications) {
                if (application.getName().equals(applicationName)) {
                    return application;
                }
            }
        }
        return null;
    }

    public List<Database> getDatabases() {
        return databases;
    }

    public void setDatabases(List<Database> databases) {
        this.databases = databases;
    }

    public Database getDatabase(String databaseName) {
        if (databases != null) {
            for (Database database : databases) {
                if (database.getName().equals(databaseName)) {
                    return database;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Domain{" + "url=" + url + ", domainName=" + domainName + ", serverType=" + serverType + ", serverRootPath=" + serverRootPath + ", domainUser=" + domainUser + ", domainPassword=" + domainPassword + ", ssl=" + ssl + '}';
    }
}
