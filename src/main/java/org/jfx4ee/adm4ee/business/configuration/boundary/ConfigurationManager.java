/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.configuration.boundary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.jfx4ee.adm4ee.ApplicationInfo;
import org.jfx4ee.adm4ee.business.configuration.entity.Configuration;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;

/**
 *
 * @author Jens Deters
 */
public class ConfigurationManager {

    public static final String CUSTOM_FXML_ROOT_DIRECTORY = "./FXML";
    public static final String DEFAULT_DOMAINS_ROOT_DIRECTORY = "Domains";
    private static final Logger logger = Logger.getLogger(ConfigurationManager.class.getName());
    private static ConfigurationManager instance;
    Path configFile = Paths.get(ApplicationInfo.APPLICATION_CLI_NAME + "-config.xml");
    private Configuration config;
    private JAXBContext jaxbContext;
    Domain currentDomain;

    @PostConstruct
    public void init() {
        try {
            jaxbContext = JAXBContext.newInstance(Configuration.class);
        } catch (JAXBException ex) {
            logger.log(Level.SEVERE, "Could not initialize configuration parser: {0}", ex);
        }
        loadConfiguration();
    }

    /**
     * Get the configurator instance
     *
     * @return
     */
    public static ConfigurationManager getInstance() {
        if (instance == null) {
            instance = new ConfigurationManager();
        }
        instance.init();
        return instance;
    }

    public String getCustomFxmlRootDirectory() {
        return String.format("%s/%s/",DEFAULT_DOMAINS_ROOT_DIRECTORY, CUSTOM_FXML_ROOT_DIRECTORY);
    }

    /**
     * Get the configured log level
     *
     * @return log level
     */
    public Level getLogLevel() {
        return config.getLogLevel();
    }

    /**
     * Set the log level
     *
     * @param level log level
     */
    public void setLogLevel(Level level) {
        config.setLogLevel(level);
    }

    /**
     * Get all configured domains
     *
     * @return configured domains
     */
    public List<Domain> getDomains() {
        return config.getDomains();
    }

    /**
     * Get the local domain configuration with the specified server root path and domain name
     *
     * @param serverRootPath Server root path
     * @param domainName domain name
     * @return the domain configuration, or null if there is no such domain
     */
    public Domain getDomain(String serverRootPath, String domainName) {
        if (domainName == null) {
            throw new IllegalArgumentException("Invalid domain name '" + domainName + "'!");
        }
        List<Domain> domains = getDomains();
        for (Domain domain : domains) {
            if (domainName.equals(domain.getDomainName())) {
                if (serverRootPath != null && serverRootPath.equals(domain.getServerRootPath())) {
                    return domain;
                } else if (serverRootPath == null) {
                    return domain;
                }
            }
        }
        return null;
    }

    /**
     * Get the remote domain configuration with the specified URL
     *
     * @param url domain management url
     * @return the domain configuration, or null if there is no domain with this URL
     */
    public Domain getDomain(String url) {
        if (url == null) {
            throw new IllegalArgumentException("Invalid domain url '" + url + "'!");
        }
        List<Domain> domains = getDomains();
        for (Domain domain : domains) {
            if (url.equals(domain.getUrl())) {
                return domain;
            }
        }
        return null;
    }

    public Domain getCurrentDomain() {
        return currentDomain;
    }

    public void setCurrentDomain(Domain currentDomain) {
        this.currentDomain = currentDomain;
    }

    public UIConfiguration getUIConfiguration() {
        return config.getUIConfiguration();
    }

    public void setUIConfiguration(UIConfiguration uiConfiguration) {
        config.setUIConfiguration(uiConfiguration);
    }

    public void setConfigFile(Path configFile) {
        this.configFile = configFile;
    }

    /**
     * Load configuration from config file
     *
     * @return true if configuration has been loaded successfully, false otherwise
     */
    public final boolean loadConfiguration() {
        BufferedReader reader;
        try {
            // Load the configuration
            Unmarshaller um = jaxbContext.createUnmarshaller();
            reader = Files.newBufferedReader(configFile, StandardCharsets.UTF_8);
            config = (Configuration) um.unmarshal(reader);
            return true;
        } catch (IOException | JAXBException ex) {
            logger.log(Level.SEVERE, "Could not load config file ''{0}''! Creating a new one.", configFile);

            // File not found, so create a default file
            config = new Configuration();
            config.setLogLevel(Level.OFF);
            Domain domain = new Domain();
            domain.setServerRootPath("/Applications/Netbeans/glassfish-3.1.2.2");
            domain.setUrl("localhost:4848");
            domain.setDomainName("domain1");
            domain.setServerType(ServerType.GLASSFISH);
            domain.setDomainUser("admin");
            domain.setDomainPassword("");
            domain.setSsl(false);
            domain.setTemplate("");

            List<Domain> domains = config.getDomains();
            domains.add(domain);
            UIConfiguration uic = new UIConfiguration();
            uic.createDefaultConfig();
            config.setUIConfiguration(uic);
            saveConfiguration();
        }
        return false;
    }

    /**
     * Save the current configuration to the file
     *
     * @return true if saved successfully, false otherwise
     */
    public boolean saveConfiguration() {
        BufferedWriter writer;
        try {
            Marshaller m = jaxbContext.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            writer = Files.newBufferedWriter(configFile, StandardCharsets.UTF_8);
            m.marshal(config, writer);
            return true;
        } catch (IOException | JAXBException ex) {
            logger.log(Level.SEVERE, "Could not save configuration: {0}", ex);
        }
        return false;
    }
}
