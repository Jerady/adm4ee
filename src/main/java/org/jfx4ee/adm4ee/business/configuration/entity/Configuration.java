/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.configuration.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Jens Deters
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Configuration implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @XmlJavaTypeAdapter(LogLevelAdapter.class)
    Level logLevel;
    @XmlElementWrapper(name = "domains")
    @XmlElement(name="domain")
    List<Domain> domains = new ArrayList<>();
    UIConfiguration uiConfiguration;

    
    public Level getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(Level logLevel) {
        this.logLevel = logLevel;
    }

    public List<Domain> getDomains() {
        return domains;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

    public UIConfiguration getUIConfiguration() {
        return uiConfiguration;
    }

    public void setUIConfiguration(UIConfiguration uiConfiguration) {
        this.uiConfiguration = uiConfiguration;
        }
}
