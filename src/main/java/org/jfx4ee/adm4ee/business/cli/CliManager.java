/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.cli;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jfx4ee.adm4ee.ApplicationInfo;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.server.boundary.Server;
import org.jfx4ee.adm4ee.business.server.boundary.ServerHandler;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;
import org.jfx4ee.adm4ee.business.util.CommandResult;

/**
 *
 * @author Jens Deters
 */
public class CliManager {

    private static final Logger logger = Logger.getLogger(CliManager.class.getName());

    private CommandLineParser parser;
    private Options options;
    private CommandLine commandLine;

    public CliManager() {
        init();
    }

    /**
     * Initialize all available command line options
     *
     * @return
     */
    private void init() {
        parser = new GnuParser();

        final Options opt = new Options();
        Option createDomain = OptionBuilder.withArgName("domain")
                .hasArg()
                .withDescription("Create the specified domain")
                .create("create");
        opt.addOption(createDomain);
        
        Option deleteDomain = OptionBuilder.withArgName("domain")
                .hasArg()
                .withDescription("Delete the specified domain")
                .create("delete");
        opt.addOption(deleteDomain);
        
        Option domainTemplate = OptionBuilder.withArgName("template")
                .hasArg()
                .withDescription("Domain template to be used for create domain")
                .create("template");
        opt.addOption(domainTemplate);
        
        Option portBase = OptionBuilder.withArgName("portbase")
                .hasArg()
                .withDescription("Port base to be used for create domain")
                .create("portbase");
        opt.addOption(portBase);
        
        Option serverRootPath = OptionBuilder.withArgName("serverpath")
                .hasArg()
                .withDescription("Server root path to be used for create domain")
                .create("serverpath");
        opt.addOption(serverRootPath);
        
        Option adminUser = OptionBuilder.withArgName("user")
                .hasArg()
                .withDescription("The admin user for the domain")
                .create("user");
        opt.addOption(adminUser);
        
        Option adminPassword = OptionBuilder.withArgName("password")
                .hasArg()
                .withDescription("The admin password for the domain")
                .create("password");
        opt.addOption(adminPassword);
        
        Option checkPorts = OptionBuilder.withArgName("checkports")
                .hasArg()
                .withDescription("Whether to check the specified server ports for availability or not")
                .create("checkports");
        opt.addOption(checkPorts);
        
        Option startDomain = OptionBuilder.withArgName("domain")
                .hasArg()
                .withDescription("Start the specified domain")
                .create("start");
        opt.addOption(startDomain);

        Option stopDomain = OptionBuilder.withArgName("domain")
                .hasArg()
                .withDescription("Stop the specified domain")
                .create("stop");
        opt.addOption(stopDomain);

        Option logLevel = OptionBuilder.withArgName("level")
                .hasArg()
                .withDescription("log level to be used (default: OFF). Possible values are: OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST.")
                .create("log");
        opt.addOption(logLevel);

        Option watchdog = OptionBuilder.withArgName("domain")
                .hasArg()
                .withDescription("check if the domain is running and restart it in case of a problem")
                .create("watchdog");
        opt.addOption(watchdog);

        Option listApp = OptionBuilder.withArgName("domain")
                .hasArg()
                .withDescription("List all installed applications of the specified domain")
                .create("applications");
        opt.addOption(listApp);

        Option ping = OptionBuilder.withArgName("domain")
                .hasArg()
                .withDescription("Ping the specified domain")
                .create("ping");
        opt.addOption(ping);
        
        opt.addOption("help", false, "print this message");
        opt.addOption("version", false, "show version information");
        this.options = opt;
    }

    /**
     * Process the given command line arguments
     *
     * @param args command line arguments
     */
    public void processCommandLine(List<String> args) {
        parseCommandLine(args);
        
        if (commandLine.hasOption("help")) {
            printHelp();
            System.exit(0);

        } else if (commandLine.hasOption("version")) {
            System.out.println(ApplicationInfo.APPLICATION_NAME + " " + ApplicationInfo.APPLICATION_VERSION);
            System.out.println(ApplicationInfo.APPLICATION_COPYRIGHT);
            System.exit(0);

        } else if (commandLine.hasOption("create")) {
            createDomain();
            
        } else if (commandLine.hasOption("start")) {
            Domain domain = getDomain(commandLine.getOptionValue("start"));
            ServerHandler serverHandler = ServerHandler.getInstance(domain);
            Server server = serverHandler.getServer();
            server.initLocalServer();
            CommandResult result = server.startDomain(domain.getDomainName());
            System.exit(result.getExitCode());

        } else if (commandLine.hasOption("stop")) {
            Domain domain = getDomain(commandLine.getOptionValue("stop"));
            ServerHandler serverHandler = ServerHandler.getInstance(domain);
            Server server = serverHandler.getServer();
            server.initLocalServer();
            server.initLocalDomain();
            CommandResult result = server.stopDomain();
            System.exit(result.getExitCode());

        } else if (commandLine.hasOption("watchdog")) {
            watchdog();

        } else if (commandLine.hasOption("ping")) {
            ping();
            
        } else if (commandLine.hasOption("applications")) {
            listApplications();
        }
    }

    protected void ping() {
        Domain domain = getDomain(commandLine.getOptionValue("ping"));
        ServerHandler serverHandler = ServerHandler.getInstance(domain);
        Server server = serverHandler.getServer();
        server.initLocalServer();
        server.initLocalDomain();
        CommandResult result = server.pingManagementResource();
        if (result.isOk()) {
            System.out.println("SUCCESS");
        } else {
            System.out.println("FAILURE");
        }
        System.exit(result.getExitCode());
    }

    protected void listApplications() {
        Domain domain = getDomain(commandLine.getOptionValue("applications"));
        ServerHandler serverHandler = ServerHandler.getInstance(domain);
        Server server = serverHandler.getServer();
        server.initLocalServer();
        server.initLocalDomain();
        try {
            Set<String> applications = server.getInstalledApplications();
            System.out.println(applications);
            System.exit(0);
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex);
            System.exit(1);
        }
    }

    protected void watchdog() {
        Domain domain = getDomain(commandLine.getOptionValue("watchdog"));
        ServerHandler serverHandler = ServerHandler.getInstance(domain);
        Server server = serverHandler.getServer();
        server.initLocalServer();
        server.initLocalDomain();
        CommandResult result = server.pingManagementResource();
        if (!result.isOk()) {
            System.out.println("Domain is not accessable: " + result.getExitCode() + " " + result.getExitCodeInfo());
            try {
                server.stopDomain();
            } catch (Exception e) {
                // Ignore stop error here
            }
            try {
                // wait some time to be sure it is stopped
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
            result = server.startDomain(domain.getDomainName());
            if (result.isOk()) {
                result = server.pingManagementResource();
            }
        }
        System.exit(result.getExitCode());
    }

    protected void createDomain() throws NumberFormatException {
        if (!commandLine.hasOption("serverpath")) {
            System.err.println("No server root path specified");
            System.exit(1);
        }
        if (!commandLine.hasOption("user")) {
            System.err.println("No admin user specified!");
            System.exit(1);
        }
        if (!commandLine.hasOption("password")) {
            System.err.println("No admin password specified!");
            System.exit(1);
        }
        
        // We need at least a server root path, the desired domain name, user and password here
        Domain domain = new Domain();
        domain.setServerType(ServerType.GLASSFISH);
        domain.setDomainName(commandLine.getOptionValue("create"));
        domain.setServerRootPath(commandLine.getOptionValue("serverpath"));
        domain.setDomainUser(commandLine.getOptionValue("user"));
        domain.setDomainPassword(commandLine.getOptionValue("password"));
        
        // Init the server
        ServerHandler serverHandler = ServerHandler.getInstance(domain);
        Server server = serverHandler.getServer();
        server.initLocalServer();
        
        // Check if there is a template
        File template = null;
        if (commandLine.hasOption("template")) {
            String templateName = commandLine.getOptionValue("template");
            if (templateName != null) {
                template = new File(templateName);
                if (!template.exists()) {
                    System.err.println("Domain template '" + template.getAbsolutePath() + "' does not exist!");
                    System.exit(1);
                }
            }
        }
        
        // Check if there is a port base
        int portBase = -1;
        if (commandLine.hasOption("portbase")) {
            String portbaseStr = commandLine.getOptionValue("portbase");
            if (portbaseStr != null) {
                portBase = Integer.parseInt(portbaseStr);
                if (portBase <= 0) {
                    System.err.println("Invalid portbase '" + portbaseStr + "'!");
                }
            }
        }
        
        boolean checkPorts = true;
        if (commandLine.hasOption("checkports")) {
            String checkPortsStr = commandLine.getOptionValue("checkports");
            if (checkPortsStr != null) {
                checkPorts = Boolean.parseBoolean(checkPortsStr);
            }
        }
        
        // Create the domain
        CommandResult result = server.createDomain(domain.getDomainName(), portBase, template, domain.getDomainUser(), domain.getDomainPassword(), checkPorts);
        if (!result.isOk()) {
            System.err.println(result.getResult());
        }
        System.exit(result.getExitCode());
    }

    protected Domain getDomain(String domainName) {
        ConfigurationManager config = ConfigurationManager.getInstance();
        Domain domain = config.getDomain(null, domainName);
        if (domain == null) {
            System.err.println("No domain configuration for '" + domainName + "' found!");
            System.exit(1);
        }
        return domain;
    }

    /**
     * Parse the given command line arguments
     *
     * @param args command line arguments
     */
    public void parseCommandLine(List<String> args) {
        try {
            commandLine = parser.parse(options, args.toArray(new String[args.size()]));
        } catch (ParseException ex) {
            logger.log(Level.WARNING, "Could not parse command line arguments! {0}", ex);
            System.out.println(ex.getMessage() + "\n");
            printHelp();
            System.exit(1);
        }
    }

    /**
     * Print usage help
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(ApplicationInfo.APPLICATION_CLI_NAME, options);
    }
}
