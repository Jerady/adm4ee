/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.glassfish;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Class that manages the glassfish domain.xml config file
 *
 * @author Jens Deters
 */
public class DomainXmlManager {

    private static final Logger logger = Logger.getLogger(DomainXmlManager.class.getName());
    public static final String CONFIG_NODE_NAME_SERVER_CONFIG = "server-config";

    File domainXmlConfigFile;
    DocumentBuilder docBuilder;
    Document doc;
    XPath xPath;

    /**
     * Build a new domain xml manager instance to handle domain.xml file access
     *
     * @throws ParserConfigurationException
     */
    public DomainXmlManager() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        docBuilder = factory.newDocumentBuilder();
        xPath = XPathFactory.newInstance().newXPath();
    }

    /**
     * Parse the specified domain xml configuration file
     *
     * @param domainXmlConfigFile The configuration file to be parsed
     * @return The parsed domain xml document
     * @throws SAXException
     * @throws IOException
     */
    public Document parseFile(File domainXmlConfigFile) throws SAXException, IOException {
        this.domainXmlConfigFile = domainXmlConfigFile;
        doc = docBuilder.parse(domainXmlConfigFile);
        doc.getDocumentElement().normalize();
        return doc;
    }

    /**
     * Get the configured admin port from the admin-listener out of domain.xml
     *
     * @return The admin port or null
     */
    public Integer getAdminPort() {
        return getServicePort(ServiceType.ADMIN);
    }

    /**
     * Get the configured port for the specified service from the domain.xml
     *
     * @param serviceType The service type
     * @return The service port, or null if not available
     */
    public Integer getServicePort(ServiceType serviceType) {
        Integer port = null;
        try {
            Node node = (Node) xPath.evaluate("//configs/config[@name='" + CONFIG_NODE_NAME_SERVER_CONFIG + "']" + serviceType.getXPathNode(), doc.getDocumentElement(), XPathConstants.NODE);
            port = Integer.parseInt(node.getAttributes().getNamedItem(serviceType.getPortAttributeName()).getTextContent());
        } catch (NullPointerException npe) {
            logger.log(Level.WARNING, "Could not read {0} service port from domain.xml. No configured value available.", serviceType.name());
        } catch (XPathExpressionException | DOMException | NumberFormatException ex) {
            logger.log(Level.WARNING, "Could not read {0} service port from domain.xml: {1}", new Object[]{serviceType.name(), ex});
        }

        return port;
    }

    /**
     * Set the port for the specified service type in the domain.xml to the given value
     *
     * @param serviceType The service type
     * @param port The port value (e.g. 8080)
     * @return The port set in domain.xml, or null in case of an error
     */
    public Integer setServicePort(ServiceType serviceType, int port) {
        Integer newPort = null;
        try {
            Node node = (Node) xPath.evaluate("//configs/config[@name='" + CONFIG_NODE_NAME_SERVER_CONFIG + "']" + serviceType.getXPathNode(), doc.getDocumentElement(), XPathConstants.NODE);
            Node portNode = node.getAttributes().getNamedItem(serviceType.getPortAttributeName());
            if (portNode != null) {
                portNode.setTextContent(Integer.toString(port));
                newPort = port;
            }
        } catch (XPathExpressionException | DOMException | NumberFormatException ex) {
            logger.log(Level.WARNING, "Could not set {0} service port for domain.xml: {1}", new Object[]{serviceType.name(), ex});
        }
        return newPort;
    }

    /**
     * Set the port for the specified service type in the domain.xml according to the given port base value
     *
     * @param serviceType The service type
     * @param portBase The port base value (e.g. 8000)
     * @return The port set in domain.xml, or null in case of an error
     */
    public Integer setServicePortForPortBase(ServiceType serviceType, int portBase) {
        int port = serviceType.getPortForPortBase(portBase);
        return setServicePort(serviceType, port);
    }

    /**
     * Get all services with their corresponding ports as configured in the domain.xml
     *
     * @return services with their ports
     */
    public Map<String, Integer> getServicePorts() {
        Map<String, Integer> ports = new HashMap<>();
        for (ServiceType service : ServiceType.values()) {
            ports.put(service.name(), getServicePort(service));
        }
        return ports;
    }
    
    /**
     * Set all services with their corresponding ports using the given port base
     * @param portBase The port base for the service ports
     */
    public void setServicePorts(int portBase) {
        for (ServiceType service : ServiceType.values()) {
            setServicePortForPortBase(service, portBase);
        }
    }

    /**
     * Save the current domain xml configuration to the domain.xml file using the same file name where it was initially loaded from
     */
    public void save() {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(domainXmlConfigFile);
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            logger.log(Level.WARNING, "Could not write domain.xml config file {0}: {1}", new Object[]{domainXmlConfigFile, ex});
        }
    }
}
