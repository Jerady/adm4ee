package org.jfx4ee.adm4ee.business.server.control.wildfly;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Jens Deters
 */
public class ConfigXmlManager {
    private static final Pattern PORT_PATTERN = Pattern.compile("\\$\\{([^:]*):([0-9]+)\\}");
    private static final Logger logger = Logger.getLogger(ConfigXmlManager.class.getName());

    File configXmlFile;
    DocumentBuilder docBuilder;
    Document doc;
    XPath xPath;

    /**
     * Build a new xml manager instance to handle configuration file access
     *
     * @throws ParserConfigurationException
     */
    public ConfigXmlManager() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        docBuilder = factory.newDocumentBuilder();
        xPath = XPathFactory.newInstance().newXPath();
    }

    /**
     * Parse the specified xml configuration file
     *
     * @param configXmlFile The configuration file to be parsed
     * @return The parsed xml document
     * @throws SAXException
     * @throws IOException
     */
    public Document parseFile(File configXmlFile) throws SAXException, IOException {
        this.configXmlFile = configXmlFile;
        doc = docBuilder.parse(configXmlFile);
        doc.getDocumentElement().normalize();
        return doc;
    }

    /**
     * Get the configured management-http port from the configuration
     *
     * @return The admin port or null
     */
    public Integer getManagementPort() {
        return getServicePort(ServiceType.MANAGEMENT_HTTP);
    }

    /**
     * Get the configured port for the specified service from the configuration
     *
     * @param serviceType The service type
     * @return The service port, or null if not available
     */
    public Integer getServicePort(ServiceType serviceType) {
        Integer port = null;
        try {
            Node node = (Node) xPath.evaluate("//server" + serviceType.getXPathNode(), doc.getDocumentElement(), XPathConstants.NODE);
            String portConfig = node.getAttributes().getNamedItem(serviceType.getPortAttributeName()).getTextContent();
            // Check if this is a vairable string with default value
            Matcher matcher = PORT_PATTERN.matcher(portConfig);
            if (matcher.matches()) {
                String group = matcher.group(2);
                if (group != null) {
                    port = Integer.parseInt(group);
                }
            } else {
                // Try to parse the value directly
                port = Integer.parseInt(portConfig);
            }
        } catch (NullPointerException npe) {
            logger.log(Level.WARNING, "Could not read {0} service port from configuration. No configured value available.", serviceType.name());
        } catch (XPathExpressionException | DOMException | NumberFormatException ex) {
            logger.log(Level.WARNING, "Could not read {0} service port from configuration: {1}", new Object[]{serviceType.name(), ex});
        }

        return port;
    }

    /**
     * Set the port for the specified service type in the domain.xml to the given value
     *
     * @param serviceType The service type
     * @param port The port value (e.g. 8080)
     * @return The port set in domain.xml, or null in case of an error
     */
    public Integer setServicePort(ServiceType serviceType, int port) {
        Integer newPort = null;
        try {
            Node node = (Node) xPath.evaluate("//server" + serviceType.getXPathNode(), doc.getDocumentElement(), XPathConstants.NODE);
            Node portNode = node.getAttributes().getNamedItem(serviceType.getPortAttributeName());
            if (portNode != null) {
                // Try to read variable configuration
                String textContent = portNode.getTextContent();
                Matcher matcher = PORT_PATTERN.matcher(textContent);
                if (matcher.matches()) {
                    String var = matcher.group(1);
                    if (var != null) {
                        portNode.setTextContent("${" + var + ":" + Integer.toString(port) + "}");
                        newPort = port;
                    }
                } else {
                    // Write value directly
                    portNode.setTextContent(Integer.toString(port));
                    newPort = port;
                }
            }
        } catch (XPathExpressionException | DOMException | NumberFormatException ex) {
            logger.log(Level.WARNING, "Could not set {0} service port in configuration: {1}", new Object[]{serviceType.name(), ex});
        }
        return newPort;
    }

    /**
     * Set the port for the specified service type in the domain.xml according to the given port base value
     *
     * @param serviceType The service type
     * @param portBase The port base value (e.g. 8000)
     * @return The port set in domain.xml, or null in case of an error
     */
    public Integer setServicePortForPortBase(ServiceType serviceType, int portBase) {
        int port = serviceType.getPortForPortBase(portBase);
        return setServicePort(serviceType, port);
    }

    /**
     * Get all services with their corresponding ports as configured in the domain.xml
     *
     * @return services with their ports
     */
    public Map<String, Integer> getServicePorts() {
        Map<String, Integer> ports = new HashMap<>();
        for (ServiceType service : ServiceType.values()) {
            ports.put(service.name(), getServicePort(service));
        }
        return ports;
    }

    /**
     * Set all services with their corresponding ports using the given port base
     * @param portBase The port base for the service ports
     */
    public void setServicePorts(int portBase) {
        for (ServiceType service : ServiceType.values()) {
            setServicePortForPortBase(service, portBase);
        }
    }

    /**
     * Save the current domain xml configuration to the domain.xml file using the same file name where it was initially loaded from
     */
    public void save() {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(configXmlFile);
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            logger.log(Level.WARNING, "Could not write config file {0}: {1}", new Object[]{configXmlFile, ex});
        }
    }
}
