/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.wildfly;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.client.filter.CsrfProtectionFilter;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.jfx4ee.adm4ee.ApplicationInfo;
import org.jfx4ee.adm4ee.business.util.CommandResult;

/**
 *
 * @author Jens Deters
 */
public class WildflyRestManager {

    private static final Logger logger = Logger.getLogger(WildflyRestManager.class.getName());
    private static final String APPLICATION_NAME = ApplicationInfo.APPLICATION_NAME;

    private static final String MANAGEMENT_RESOURCE = "/management";
    private static final int DEFAULT_CLIENT_CONNECT_TIMEOUT = 10000; // millis
    private static final int DEFAULT_CLIENT_READ_TIMEOUT = 8000; // millis

    Client client;
    WebTarget managementResource;

    public WildflyRestManager(String url, String username, String password, boolean useSSL) {
        // Try to read username and password for authentication
        client = ClientBuilder.newClient(new ClientConfig()
                // The line bellow that registers JSON-Processing feature can be
                // omitted if FEATURE_AUTO_DISCOVERY_DISABLE is not disabled.
                .register(JsonProcessingFeature.class).property(JsonGenerator.PRETTY_PRINTING, false)
                .register(MultiPartFeature.class)
                .property(ClientProperties.CONNECT_TIMEOUT, DEFAULT_CLIENT_CONNECT_TIMEOUT)
                .property(ClientProperties.READ_TIMEOUT, DEFAULT_CLIENT_READ_TIMEOUT));
        // CSRF Protection (X-Requested-By header) is required
        client.register(new CsrfProtectionFilter(APPLICATION_NAME));
        // Register Authenticator for REST interface
        client.register(HttpAuthenticationFeature.digest(username, password));
        //client.register(new LoggingFilter());
        managementResource = client.target(getProtocol(useSSL) + url + MANAGEMENT_RESOURCE);
    }

    public CommandResult ping() {
        return handleJsonResponse(
                managementResource.path("/")
                .queryParam("operation", "attribute")
                .queryParam("name", "server-state")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Response.class)
        );
    }

    public String getUptime() {
        return managementResource.path("core-service/platform-mbean/type/runtime")
                .queryParam("operation", "attribute")
                .queryParam("name", "uptime")
                .request(MediaType.TEXT_HTML_TYPE, MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);
    }

    public CommandResult getJvmVersion() {
        return handleJsonResponse(
                managementResource.path("core-service/platform-mbean/type/runtime")
                .queryParam("operation", "attribute")
                .queryParam("name", "system-properties")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Response.class)
        );
    }

    public CommandResult stopServer() {
        JsonObject entity = Json.createObjectBuilder().add("operation", "shutdown").build();
        return handleJsonResponse(post("/", entity));
    }

    public String getManagementUrl() {
        return managementResource.getUri().toString();
    }

    public Set<String> getApplications() {
        Set<String> apps = new HashSet<>();
        Response result = managementResource.path("/")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Response.class);
        JsonObject entity = result.readEntity(JsonObject.class);
        JsonObject deployments = entity.getJsonObject("deployment");
        for (Map.Entry<String, JsonValue> entry : deployments.entrySet()) {
            apps.add(entry.getKey());
        }
        return apps;
    }

    private String getProtocol(boolean useSSL) {
        String protocol = "http://";
        if (useSSL) {
            protocol = "https://";
            logger.config("Using SSL");
        }
        return protocol;
    }

    public void close() {
        client.close();
    }

    protected Response post(String path, JsonValue entity) {
        return managementResource.path(path)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(entity, MediaType.APPLICATION_JSON), Response.class);
    }

    protected CommandResult handleJsonResponse(Response response) {
        CommandResult result = new CommandResult();
        if (response != null) {
            // Check HTTP Status
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                result.setExitCode(response.getStatus());
                result.setExitCodeInfo(response.getStatusInfo().toString());
            }
            try {
                String value = response.readEntity(String.class);
                result.setResult(value);
                result.setExitCodeOk();
            } catch (Exception ex) {
                result.setResult("Could not read result from server.");
                result.setThrowable(ex);
            }
        }
        return result;
    }
}
