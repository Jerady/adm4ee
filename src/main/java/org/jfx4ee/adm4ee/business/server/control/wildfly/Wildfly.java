/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.wildfly;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.server.control.AbstractServer;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import org.jfx4ee.adm4ee.business.util.OsHelper;
import org.jfx4ee.adm4ee.business.util.SystemCommandExecutor;
import org.xml.sax.SAXException;

/**
 *
 * @author Jens Deters
 */
public class Wildfly extends AbstractServer {

    private static final Logger logger = Logger.getLogger(Wildfly.class.getName());
    private static final Pattern PATTERN_VERSION_LOCAL = Pattern.compile(".*(JBoss .+|WildFly .+)");
    private static final Pattern PATTERN_JDK_VERSION_LOCAL = Pattern.compile("java.version: (.+)");
    private static final Pattern PATTERN_JDK_VERSION = Pattern.compile("\"java.version\"\\s*:\\s*\"([^\"]+)\"");
    private static final String LOG_PATH = "log";
    private static final String LOG_FILE_NAME = "server.log";
    private static final String CONFIG_DIR = "configuration";
    private static final String CONFIG_FILE = "standalone.xml";
    private static final String STANDALONE = "standalone";
    private static final String ADMIN_TOOL = "jboss-cli";

    ConfigXmlManager configXmlManager;
    String standaloneCommand;
    String adminCommand;
    WildflyRestManager restManager;

    public Wildfly(Domain domain) {
        super(domain);
        try {
            configXmlManager = new ConfigXmlManager();
        } catch (ParserConfigurationException ex) {
            throw new RuntimeException("Could not create domain.xml configuration manager instance! " + ex);
        }
    }

    @Override
    public void initLocalServer() {
        // Get server root path
        String serverRootPath = domain.getServerRootPath();
        if (serverRootPath == null || serverRootPath.isEmpty()) {
            throw new RuntimeException("No server root path specified!");
        }
        // Setup command
        standaloneCommand = serverRootPath + File.separator + "bin" + File.separator + STANDALONE;
        adminCommand = serverRootPath + File.separator + "bin" + File.separator + ADMIN_TOOL;
        if (OsHelper.isWindows()) {
            standaloneCommand += ".bat";
            adminCommand += ".bat";
        } else if (OsHelper.isMac() || OsHelper.isUnix()) {
            standaloneCommand += ".sh";
            adminCommand += ".sh";
        }
        // Check if command is valid
        File f = new File(standaloneCommand);
        File admin = new File(adminCommand);
        if (!f.isFile()) {
            throw new RuntimeException("Startup command " + standaloneCommand + " not found!");
        }
        logger.log(Level.CONFIG, "Using 'standalone' command: {0}", standaloneCommand);
        if (!admin.isFile()) {
            throw new RuntimeException("Administration command " + adminCommand + " not found!");
        }
        logger.log(Level.CONFIG, "Using command line interface command: {0}", adminCommand);
        initLocalVersions();
    }

    @Override
    public void initLocalDomain() {
        if (standaloneCommand == null) {
            throw new RuntimeException("Local server not yet initialized! Call initLocalServer() first!");
        }
        String domainName = domain.getDomainName();
        if (domainName == null || domainName.isEmpty()) {
            throw new RuntimeException("No domain name specified!");
        }

        File configFile = getDomainConfigPath().resolve(CONFIG_FILE).toFile();
        try {
            configXmlManager.parseFile(configFile);
        } catch (SAXException | IOException ex) {
            throw new RuntimeException("Could not read domain configuration file '" + configFile.getAbsolutePath() + "': " + ex);
        }

        Integer adminPort = configXmlManager.getManagementPort();
        if (adminPort == null || adminPort == 0) {
            throw new RuntimeException("No valid admin port specified!");
        }
        domain.setUrl(URL_HOST + ":" + adminPort);

        initRemoteDomain();
        refreshApplicationStatus();
    }

    @Override
    public void initRemoteDomain() {
        if (restManager != null) {
            try {
                restManager.close();
            } catch (Exception ex) {
                logger.log(Level.INFO, "Could not close existing restManager connection: {0}", ex);
            }
        }
        restManager = new WildflyRestManager(domain.getUrl(), domain.getDomainUser(), domain.getDomainPassword(), domain.isSsl());
    }

    @Override
    public File[] listLocalDomains(String serverRootPath) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getVersionString() {
        return localVersion;
    }

    @Override
    public String getJdkVersionString() {
        return localJdkVersion;
    }

    @Override
    public Map<String, Integer> getServicePorts() {
        return configXmlManager.getServicePorts();
    }

    @Override
    public void setServicePorts(int portBase) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer setServicePort(String service, int port) {
        return configXmlManager.setServicePort(ServiceType.valueOf(service), port);
    }

    @Override
    public void saveDomainConfiguration() {
        configXmlManager.save();
        // We have to reinitialize the client with the new config
        initLocalDomain();
    }

    @Override
    public CommandResult verifyDomainConfiguration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult createDomain(String domainName, int portBase, File template, String user, String password, boolean checkPorts) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult deleteDomain(String domainName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult startDomain(String domainName) {
        if (domainName == null) {
            throw new IllegalArgumentException("No domain name specified!");
        }
        int timeoutMsec = 8000;
        boolean destroyAfterTimeout = false;
        CommandResult result = SystemCommandExecutor.execute(Arrays.asList(standaloneCommand), 1024, timeoutMsec, destroyAfterTimeout);
        if (result.isOk()) {
            // It is OK if the process is still running
            // Poll until server is really up
            for (int i = 0; i < 10; i++) {
                CommandResult r = pingManagementResource();
                if (r.isOk()) {
                    break;
                }
                try {
                    Thread.sleep(1200);
                } catch (InterruptedException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
            updateJvmVersion();
// TODO:
//            refreshApplicationStatus();
        }
        return result;
    }

    @Override
    public CommandResult stopDomain() {
        CommandResult result = restManager.stopServer();
        if (result.isOk()) {
            // Poll until server is really down
            for (int i = 0; i < 10; i++) {
                CommandResult r = pingManagementResource();
                if (CommandResult.EXIT_CODE_UNKNOWN == r.getExitCode()) {
                    break;
                }
                try {
                    Thread.sleep(1200);
                } catch (InterruptedException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
        }
        return result;
    }

    @Override
    public Set<String> listLibraries() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult changeAdminPassword(String oldPassword, String newPassword) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult pingManagementResource() {
        CommandResult result;
        try {
            result = restManager.ping();
        } catch (Exception ex) {
            // Could not fetch requested information
            logger.log(Level.FINE, "Could not ping management URL using {0}: {1}", new Object[]{restManager.getManagementUrl(), ex});
            result = new CommandResult(ex);
        }
        return result;
    }

    @Override
    public CommandResult getLogFileContent(long characters) {
        CommandResult result;
        if (standaloneCommand != null) {
            try {
                result = new CommandResult(readBytesFromFileEnd(getDomainPath().resolve(LOG_PATH).resolve(LOG_FILE_NAME).toString(), characters));
            } catch (Exception ex1) {
                result = new CommandResult(ex1);
            }
        } else {
            result = new CommandResult(new Exception("Domain not configured"));
        }
        return result;
    }

    @Override
    public long getUptime() {
        long millis = 0;
        try {
            millis = Long.parseLong(restManager.getUptime());
        } catch (Exception e) {
            logger.log(Level.WARNING, "Could not fetch uptime value: {0}", e);
        }
        return millis;
    }

    @Override
    public Set<String> getInstalledApplications() {
        return restManager.getApplications();
    }

    @Override
    public CommandResult createApplication(String application, String contextroot, File file, String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult deleteApplication(String application) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult updateApplication(String applicationName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getJdbcConnectionPools() {
        // TODO:
        return new HashSet<>();
    }

    @Override
    public CommandResult pingJdbcConnectionPool(String poolName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, String> getJdbcConnectionPoolParameters(String poolName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult setJdbcConnectionPoolParameters(String poolName, Map<String, String> parameters) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult createJdbcResource(String resourceId, String poolName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult deleteJdbcResource(String resourceId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        if (restManager != null) {
            restManager.close();
        }
        super.close();
    }

    protected void updateJvmVersion() {
        CommandResult result = restManager.getJvmVersion();
        if (result.isOk()) {
            Matcher jdkVersionMatcher = PATTERN_JDK_VERSION.matcher(result.getResult());
            if (jdkVersionMatcher.find()) {
                localJdkVersion = jdkVersionMatcher.group(1);
            }
        }
    }

    /**
     * Initialize local version from asadmin command
     */
    protected void initLocalVersions() {
        CommandResult commandResult = SystemCommandExecutor.execute(Arrays.asList(standaloneCommand, "--version"));
        // Tbe return code here is always 1
        if (commandResult.getExitCode() == 1) {
            String output = commandResult.getResult();
            Matcher versionMatcher = PATTERN_VERSION_LOCAL.matcher(output);
            // Get the last match
            while (versionMatcher.find()) {
                localVersion = versionMatcher.group(1);
            }

            CommandResult jdkVersionCommand = SystemCommandExecutor.execute(Arrays.asList(adminCommand, "version"));
            if (jdkVersionCommand.isOk()) {
                String adminOutput = jdkVersionCommand.getResult();
                Matcher jdkVersionMatcher = PATTERN_JDK_VERSION_LOCAL.matcher(adminOutput);
                if (jdkVersionMatcher.find()) {
                    localJdkVersion = jdkVersionMatcher.group(1);
                }
            } else {
                logger.log(Level.SEVERE, "Could not initialize local version information from admin command: {0}: {1}", new Object[]{jdkVersionCommand.getResult(), jdkVersionCommand.getThrowable()});
            }
        } else {
            logger.log(Level.SEVERE, "Could not initialize local version information from admin command: {0}: {1}", new Object[]{commandResult.getResult(), commandResult.getThrowable()});
        }
    }

    private Path getDomainPath() {
        return Paths.get(domain.getServerRootPath(), STANDALONE);
    }

    public Path getDomainConfigPath() {
        return getDomainPath().resolve(CONFIG_DIR);
    }

    protected void refreshApplicationStatus() {
        List<Application> configuredApplications = domain.getApplications();
        if (configuredApplications == null || configuredApplications.isEmpty()) {
            logger.log(Level.CONFIG, "No applications configured, trying to detect installed applications...");
            try {
                Set<String> installedApplications = getInstalledApplications();
                if (installedApplications != null) {
                    List<Application> apps = new ArrayList<>();
                    for (String applicationName : installedApplications) {
                        Application app = new Application(applicationName);
                        apps.add(app);
                    }
                    domain.setApplications(apps);
                }
            } catch (Exception e) {
                logger.log(Level.WARNING, "Could not detect installed applications: {0}", e);
            }
        }

        configuredApplications = domain.getApplications();
        if (configuredApplications != null) {
            for (Application application : configuredApplications) {
//                // Refresh local version info
//                Path applicationManifestPath = getApplicationManifestPath(application.getName());
//                application.setVersion(updater.getInstalledApplicationVersion(applicationManifestPath));
//                String configuredlUpdateUrl = application.getUpdateUrl();
//                if (configuredlUpdateUrl == null || configuredlUpdateUrl.isEmpty()) {
//                    // Try to refresh update URL from Manifest (if set)
//                    application.setUpdateUrl(updater.getInstalledApplicationUpdateUrl(applicationManifestPath));
//                }
//                // Refresh remote version info (ApplicationStatus)
//                application.setStatus(updater.getApplicationStatus(application.getVersion(), application.getUpdateUrl()));
            }
        } else {
            logger.log(Level.INFO, "No applications found.");
        }
    }

}
