/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control;

import org.jfx4ee.adm4ee.business.util.CommandResult;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.server.boundary.Server;
import org.jfx4ee.adm4ee.business.updater.Updater;

/**
 *
 * @author Jens Deters
 */
public class AbstractServer implements Server {
    
    public AbstractServer(Domain domain) {
        this.domain = domain;
        this.updater = new Updater(this);
    }
    
    protected Domain domain;
    protected String localVersion;
    protected String localJdkVersion;
    protected Updater updater;

    @Override
    public void initLocalServer() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initLocalDomain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initRemoteDomain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File[] listLocalDomains(String serverRootPath) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getVersionString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getJdkVersionString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Domain getDomain() {
        return domain;
    }

    @Override
    public Map<String, Integer> getServicePorts() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setServicePorts(int portBase) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer setServicePort(String service, int port) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveDomainConfiguration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult verifyDomainConfiguration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult createDomain(String domainName, int portBase, File template, String user, String password, boolean checkPorts) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult deleteDomain(String domainName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult startDomain(String domainName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult stopDomain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> listLibraries() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult changeAdminPassword(String oldPassword, String newPassword) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult pingManagementResource() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult getLogFileContent(long characters) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long getUptime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getInstalledApplications() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Application> getApplications() {
        return domain.getApplications();
    }

    @Override
    public CommandResult createApplication(String application, String contextroot, File file, String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult deleteApplication(String application) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult updateApplication(String applicationName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getJdbcConnectionPools() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult pingJdbcConnectionPool(String poolName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, String> getJdbcConnectionPoolParameters(String poolName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult setJdbcConnectionPoolParameters(String poolName, Map<String, String> parameters) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult createJdbcResource(String resourceId, String poolName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CommandResult deleteJdbcResource(String resourceId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        if (updater != null) {
            updater = null;
        }
    }
    
    public String readBytesFromFileEnd(String fileName, long bytes) throws Exception {
        if (fileName == null) {
            throw new IllegalArgumentException("No valid file name 'null'.");
        }
        StringBuilder content = new StringBuilder();

        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(fileName, "r");
            long offset = raf.length() - bytes;
            if (offset < 0) {
                offset = 0;
            }
            raf.seek(offset);
            while (raf.getFilePointer() < raf.length()) {
                content.append(raf.readLine()).append("\n");
            }
        } finally {
            try {
                if (raf != null) {
                    raf.close();
                }
            } catch (IOException ex) { /* Nothing to do */ }
        }
        return content.toString();
    }
}
