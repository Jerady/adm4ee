/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.glassfish;

/**
 *
 * @author Jens Deters
 */
public enum ServiceType {
    IIOP(37, "//iiop-service/iiop-listener[@id='orb-listener-1']", "port"),
    IIOP_SSL(38, "//iiop-service/iiop-listener[@id='SSL']", "port"),
    IIOP_SSL_MUTUALAUTH(39, "//iiop-service/iiop-listener[@id='SSL_MUTUALAUTH']", "port"),
    ADMIN(48, "//network-listeners/network-listener[@name='admin-listener']", "port"),
    JMS(76, "//jms-service/jms-host", "port"),
    HTTP(80, "//network-listeners/network-listener[@name='http-listener-1']", "port"),
    HTTPS(81, "//network-listeners/network-listener[@name='http-listener-2']", "port"),
    JMX(86, "//admin-service/jmx-connector", "port");

    int portBaseOffset;
    String xPathNode;
    String portAttributeName;

    ServiceType(int portBaseOffset, String xPathNode, String portAttributeName) {
        this.portBaseOffset = portBaseOffset;
        this.xPathNode = xPathNode;
        this.portAttributeName = portAttributeName;
    }
    
    public String getXPathNode() {
        return xPathNode;
    }
    
    public String getPortAttributeName() {
        return portAttributeName;
    }
    
    public int getPortForPortBase(int portBase) {
        return portBase + portBaseOffset;
    }
}
