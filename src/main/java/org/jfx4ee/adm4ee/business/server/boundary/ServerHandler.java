/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.boundary;

import java.lang.reflect.Constructor;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;

/**
 * The basic server handler class that initializes the server type specific implementations
 *
 * @author Jens Deters
 */
public class ServerHandler {

    private static final Logger logger = Logger.getLogger(ServerHandler.class.getName());
    private static ServerHandler instance = null;
    private ServerType serverType;
    private Server server;

    /**
     * Initialize the server handler with the given domain configuration
     * @param domain the domain configuration
     */
    private ServerHandler(Domain domain) {
        serverType = domain.getServerType();
        if (serverType == null) {
            logger.log(Level.SEVERE, "Unknown configuration value for server type: {0}", serverType);
            return;
        }
        Class<?> serverClass = serverType.getImplementingClass();
        Constructor<?> cons;
        try {
            cons = serverClass.getConstructor(Domain.class);
            server = (Server) cons.newInstance(domain);
            logger.log(Level.CONFIG, "Server class ''{0}'' initialized successfully.", serverClass.getCanonicalName());
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Could not create instance of server class ''{0}''! {1}", new Object[]{serverClass.getCanonicalName(), ex});
        }
    }

    /**
     * Get a server handler instance for the given domain configuration  
     * @param domain the domain configuration
     * @return the server handler instance
     */
    public static ServerHandler getInstance(Domain domain) {
        if (domain == null) {
            throw new IllegalArgumentException("Cannot initialize server instance without domain configuration!");
        }
        if (instance == null || !domain.equals(instance.getServer().getDomain())) {
            instance = new ServerHandler(domain);
        }
        return instance;
    }

    /**
     * Get the server type of this instance
     * @return the server type
     */
    public ServerType getServerType() {
        return serverType;
    }

    /**
     * Get the manageable server instance  
     * @return the server instance
     */
    public Server getServer() {
        return server;
    }
}
