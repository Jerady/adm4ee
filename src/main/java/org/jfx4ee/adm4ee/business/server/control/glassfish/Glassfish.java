/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.glassfish;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.jfx4ee.adm4ee.business.appcast.entity.Appcast;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import static org.jfx4ee.adm4ee.business.server.boundary.Server.URL_HOST;
import org.jfx4ee.adm4ee.business.server.control.AbstractServer;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import org.jfx4ee.adm4ee.business.util.OsHelper;
import org.jfx4ee.adm4ee.business.util.SystemCommandExecutor;
import org.jfx4ee.adm4ee.business.updater.ApplicationStatus;

/**
 *
 * @author Jens Deters
 */
public class Glassfish extends AbstractServer {

    private static final Logger logger = Logger.getLogger(Glassfish.class.getName());
    private static final String DOMAINS_PATH = "/glassfish/domains/";
    private static final String DOMAIN_APPLICATIONS_DIR = "applications";
    private static final String DOMAIN_CONFIG_DIR = "config";
    private static final String DOMAIN_APPLICATION_MANIFEST = "META-INF/MANIFEST.MF";
    private static final String DOMAIN_XML_FILE = "domain.xml";
    private static final String DOMAIN_SETUP_SCRIPT = "domain-setup.js";
    private static final String DOMAIN_TEMPLATE_APPLICATIONS_DIR = "applications";
    private static final Pattern PATTERN_VERSION_LOCAL = Pattern.compile("Version = (.+)");
    private static final Pattern PATTERN_JDK_VERSION_LOCAL = Pattern.compile("asadmin Java .+[Vv]ersion: (.+)");

    DomainXmlManager domainXmlManager;
    String asAdminCommand;
    GlassfishRestManager restManager;

    /**
     * Create a new instance
     *
     * @param domain The domain to be managed
     */
    public Glassfish(Domain domain) {
        super(domain);
        try {
            domainXmlManager = new DomainXmlManager();
        } catch (ParserConfigurationException ex) {
            throw new RuntimeException("Could not create domain.xml configuration manager instance! " + ex);
        }
    }

    @Override
    public void initLocalServer() {
        // Get server root path
        String serverRootPath = domain.getServerRootPath();
        if (serverRootPath == null || serverRootPath.isEmpty()) {
            throw new RuntimeException("No server root path specified!");
        }
        // Setup asAdminCommand
        asAdminCommand = serverRootPath + File.separator + "bin" + File.separator + "asadmin";
        if (OsHelper.isWindows()) {
            asAdminCommand += ".bat";
        }
        // Check if asAdminCommand is valid
        File f = new File(asAdminCommand);
        if (!f.isFile()) {
            throw new RuntimeException("Administration tool " + asAdminCommand + " not found!");
        }
        logger.log(Level.CONFIG, "Using 'asadmin' command: {0}", asAdminCommand);
        initLocalVersions();
    }

    @Override
    public void initLocalDomain() {
        if (asAdminCommand == null) {
            throw new RuntimeException("Local server not yet initialized! Call initLocalServer() first!");
        }
        String domainName = domain.getDomainName();
        if (domainName == null || domainName.isEmpty()) {
            throw new RuntimeException("No domain name specified!");
        }

        File domainXmlConfigFile = getDomainConfigPath().resolve(DOMAIN_XML_FILE).toFile();
        try {
            domainXmlManager.parseFile(domainXmlConfigFile);
        } catch (Exception ex) {
            throw new RuntimeException("Could not read domain configuration file '" + domainXmlConfigFile.getAbsolutePath() + "': " + ex);
        }

        Integer adminPort = domainXmlManager.getAdminPort();
        if (adminPort == null || adminPort == 0) {
            throw new RuntimeException("No valid admin port specified!");
        }
        domain.setUrl(URL_HOST + ":" + adminPort);

        initRemoteDomain();
        refreshApplicationStatus();
    }

    /**
     * Initialize the client for remote communication with the server/domain using REST
     */
    @Override
    public void initRemoteDomain() {
        if (restManager != null) {
            try {
                restManager.close();
            } catch (Exception ex) {
                logger.log(Level.INFO, "Could not close existing restManager connection: {0}", ex);
            }
        }
        restManager = new GlassfishRestManager(domain.getUrl(), domain.getDomainUser(), domain.getDomainPassword(), domain.isSsl());
    }

    @Override
    public File[] listLocalDomains(String serverRootPath) {
        File domainsDir = new File(serverRootPath + DOMAINS_PATH);
        File[] subDirs = domainsDir.listFiles((File d) -> d.isDirectory());
        return subDirs;
    }

    @Override
    public String getVersionString() {
        return getLocalVersionString();
    }

    @Override
    public String getJdkVersionString() {
        return getLocalJdkVersionString();
    }

    /* ------------ domain.xml handling -------------- */
    @Override
    public Map<String, Integer> getServicePorts() {
        return domainXmlManager.getServicePorts();
    }

    @Override
    public void setServicePorts(int portBase) {
        domainXmlManager.setServicePorts(portBase);
    }

    @Override
    public Integer setServicePort(String service, int port) {
        return domainXmlManager.setServicePort(ServiceType.valueOf(service), port);
    }

    @Override
    public void saveDomainConfiguration() {
        domainXmlManager.save();
        // We have to reinitialize the client with the new config
        initLocalDomain();
    }

    @Override
    public CommandResult verifyDomainConfiguration() {
        if (asAdminCommand == null) {
            throw new RuntimeException("Local server not yet initialized!");
        }
        return SystemCommandExecutor.execute(Arrays.asList(asAdminCommand, "verify-domain-xml", domain.getDomainName()));
    }

    /* ----------- Basic domain management ------------- */
    @Override
    public CommandResult createDomain(String domainName, int portBase, final File template, String user, String password, boolean checkPorts) {
        if (domainName == null) {
            throw new IllegalArgumentException("No domain name specified!");
        }
        if (asAdminCommand == null) {
            throw new RuntimeException("Local server not yet initialized!");
        }

        // Create the password temp file
        File temp = createTemporaryAsadminPasswordFile(password);

        CommandResult result = null;
        try {

            // Construct the command line
            List<String> asAdminCommandLine = new ArrayList<>();
            asAdminCommandLine.add(asAdminCommand);
            asAdminCommandLine.add("--user");
            asAdminCommandLine.add(user);
            asAdminCommandLine.add("--passwordfile");
            asAdminCommandLine.add(temp.getAbsolutePath());

            asAdminCommandLine.add("create-domain");
            if (portBase > 0) {
                asAdminCommandLine.add("--portbase");
                asAdminCommandLine.add(Integer.toString(portBase));
            }
            if (!checkPorts) {
                asAdminCommandLine.add("--checkports");
                asAdminCommandLine.add("false");
            }

            // Check if we have a valid template
            if (template != null) {
                if (!template.exists()) {
                    throw new IllegalArgumentException("Domain template '" + template + "' does not exist");
                }
                // Create the domain from the template
                asAdminCommandLine.add("--template");
                if (template.isDirectory()) {
                    // Find domain xml template file
                    File domainXmlTemplate = template.toPath().resolve(DOMAIN_XML_FILE).toFile();
                    if (!domainXmlTemplate.isFile()) {
                        throw new IllegalArgumentException("Domain template file not found in '" + domainXmlTemplate.getAbsolutePath() + "'!");
                    }
                    asAdminCommandLine.add(domainXmlTemplate.getAbsolutePath());
                } else {
                    asAdminCommandLine.add(template.getAbsolutePath());
                }
            }

            // Add the desired domain name
            asAdminCommandLine.add(domainName);

            // Create the domain
            logger.log(Level.CONFIG, "Creating domain with: {0}", asAdminCommandLine);
            result = SystemCommandExecutor.execute(asAdminCommandLine);

        } finally {
            try {
                temp.delete();
            } catch (Exception ex) {
                logger.log(Level.WARNING, "Could not delete temporary password file '" + temp.getAbsolutePath() + "'! ", ex);
            }
        }

        if (result != null && result.isOk()) {
            if (template != null && template.isDirectory()) {
                try {
                    logger.log(Level.CONFIG, "Copying domain template files from {0} to {1}", new Object[]{template.getAbsolutePath(), getDomainPath()});
                    // Copy additional files to destination directories
                    copyDomainTemplateFiles(template);
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, "Could not copy template files to domain directory! {0}", ex);
                    result.setExitCode(1);
                    result.setExitCodeInfo("Could not copy template files to domain directory!");
                    result.setThrowable(ex);
                }

                if (result.isOk()) {
                    setupDomain(template);
                }
            }
        }

        return result;
    }

    @Override
    public CommandResult deleteDomain(String domainName) {
        if (domainName == null) {
            throw new IllegalArgumentException("No domain name specified!");
        }
        if (asAdminCommand == null) {
            throw new RuntimeException("Local server not yet initialized!");
        }
        // We try to stop a running domain before deleting in order to avoid zombie processes
        try {
            stopDomain();
        } catch (Exception ex) {
            logger.log(Level.WARNING, "Could not stop running domain ''{0}''! {1}", new Object[]{domainName, ex});
        }
        return SystemCommandExecutor.execute(Arrays.asList(asAdminCommand, "delete-domain", domainName));
    }

    @Override
    public Set<String> listLibraries() {
        return restManager.listLibraries();
    }

    @Override
    public CommandResult startDomain(String domainName) {
        if (domainName == null) {
            throw new IllegalArgumentException("No domain name specified!");
        }
        CommandResult result = SystemCommandExecutor.execute(Arrays.asList(asAdminCommand, "start-domain", domainName));
        if (result.isOk()) {
            // Refresh the application status
            refreshApplicationStatus();
        }
        return result;
    }

    @Override
    public CommandResult stopDomain() {
        // We can use the restManager here to stop the domain
        // but we prefer to return from here when the domain is actually stopped
        return SystemCommandExecutor.execute(Arrays.asList(asAdminCommand, "stop-domain", domain.getDomainName()));
    }

    @Override
    public CommandResult changeAdminPassword(String oldPassword, String newPassword) {
        CommandResult result = restManager.changeAdminPassword(oldPassword, newPassword);
        if (result.isOk()) {
            // Change the REST client password as well
            restManager.close();
            domain.setDomainPassword(newPassword);
            initRemoteDomain();
        }
        return result;
    }

    @Override
    public CommandResult pingManagementResource() {
        CommandResult result;
        try {
            result = restManager.ping();
        } catch (Exception ex) {
            // Could not fetch requested information
            logger.log(Level.FINE, "Could not ping DAS using {0}: {1}", new Object[]{restManager.getManagementUrl(), ex.toString()});
            result = new CommandResult(ex);
        }
        return result;
    }

    @Override
    public long getUptime() {
        long millis = 0;
        try {
            millis = Long.parseLong(restManager.getUptime());
        } catch (Exception e) {
            logger.log(Level.WARNING, "Could not fetch uptime value: {0}", e);
        }
        return millis;
    }

    /**
     * Get the specified number of characters from the end of the server log file. This functions works in offline and online mode
     *
     * @param characters Number of characters to fetch from the end of the server log file
     * @return command result containing log file content, never null
     */
    @Override
    public CommandResult getLogFileContent(long characters) {
        CommandResult result;
        try {
            // First get the log file position
            long position = restManager.getLogFilePosition();

            // Now read the log
            long offset = position - characters;
            if (offset < 0) {
                offset = 0;
            }
            result = new CommandResult(restManager.getLogFileContent(offset));

        } catch (Exception ex) {
            logger.log(Level.CONFIG, "Could not fetch head from view-log resource {0}: {1}", new Object[]{restManager.getManagementUrl(), ex.toString()});

            // In case of local domain, try local fallback
            if (asAdminCommand != null) {
                try {
                    result = new CommandResult(readBytesFromFileEnd(getDomainPath().resolve("logs").resolve("server.log").toString(), characters));
                } catch (Exception ex1) {
                    result = new CommandResult(ex1);
                }
            } else {
                result = new CommandResult(ex);
            }
        }
        return result;
    }

    @Override
    public Set<String> getInstalledApplications() {
        return restManager.getApplications();
    }

    @Override
    public CommandResult createApplication(String application, String contextroot, File file, String description) {
        return restManager.createApplication(application, contextroot, file, description);
    }

    @Override
    public CommandResult deleteApplication(String application) {
        return restManager.deleteApplication(application);
    }

    @Override
    public CommandResult updateApplication(String applicationName) {
        CommandResult result = new CommandResult();
        try {
            // Check if we have a valid template path
            String template = domain.getTemplate();
            if (template != null && !template.isEmpty()) {
                Path templatePath = Paths.get(template);
                File templateFile = templatePath.toFile();
                if (!templateFile.isDirectory()) {
                    // Not a file
                    logger.log(Level.SEVERE, "Template directory ''{0}'' is not a directory!", templatePath);
                    result.setResult("Invalid Template directory");
                } else {
                    Path templateApplicationsPath = templatePath.resolve(DOMAIN_APPLICATIONS_DIR);
                    Application application = domain.getApplication(applicationName);
                    if (application != null) {
                        ApplicationStatus status = application.getStatus();
                        if (status != null) {
                            Appcast appcast = status.getAppcast();
                            if (appcast != null) {
                                Set<Path> files = updater.update(applicationName, appcast, templateApplicationsPath);
                                result.setExitCodeOk();
                                result.setResult(files.toString());
                            }
                        }
                    }
                }
            } else {
                logger.log(Level.SEVERE, "Template directory not set in domain configuration!");
                result.setResult("Template not set");
            }
        } catch (Exception exception) {
            result.setThrowable(exception);
            result.setResult("Update failed");
        }
        return result;
    }

    /* -------------- JDBC handling --------------- */
    @Override
    public Set<String> getJdbcConnectionPools() {
        return restManager.getJdbcConnectionPools();
    }

    @Override
    public CommandResult pingJdbcConnectionPool(String poolName) {
        return restManager.pingJdbcConnectionPool(poolName);
    }

    @Override
    public Map<String, String> getJdbcConnectionPoolParameters(String poolName) {
        return restManager.getJdbcConnectionPoolParameters(poolName);
    }

    @Override
    public CommandResult setJdbcConnectionPoolParameters(String poolName, Map<String, String> parameters) {
        return restManager.setJdbcConnectionPoolParameters(poolName, parameters);
    }

    @Override
    public CommandResult createJdbcResource(String resourceId, String poolName) {
        return restManager.createJdbcResource(resourceId, poolName);
    }

    @Override
    public CommandResult deleteJdbcResource(String resourceId) {
        return restManager.deleteJdbcResource(resourceId);
    }

    @Override
    public void close() {
        if (restManager != null) {
            restManager.close();
        }
        super.close();
    }

    /**
     * Initialize local version from asadmin command
     */
    protected void initLocalVersions() {
        CommandResult commandResult = SystemCommandExecutor.execute(Arrays.asList(asAdminCommand, "version", "--local", "--verbose"));
        if (commandResult.isOk()) {
            String output = commandResult.getResult();
            Matcher versionMatcher = PATTERN_VERSION_LOCAL.matcher(output);
            if (versionMatcher.find()) {
                localVersion = versionMatcher.group(1);
            }
            Matcher jdkVersionMatcher = PATTERN_JDK_VERSION_LOCAL.matcher(output);
            if (jdkVersionMatcher.find()) {
                localJdkVersion = jdkVersionMatcher.group(1);
            }
        } else {
            logger.log(Level.SEVERE, "Could not initialize local version information from ''asadmin'' command: {0}: {1}", new Object[]{commandResult.getResult(), commandResult.getThrowable()});
        }
    }

    /**
     * Get the glassfish server version from the local class
     *
     * @return Glassfish server version string
     */
    protected String getLocalVersionString() {
        if (localVersion == null) {
            initLocalVersions();
        }
        return localVersion;
    }

    /**
     * Get the JDK version from the local glassfish instance
     *
     * @return glassfish JDK version
     */
    protected String getLocalJdkVersionString() {
        if (localJdkVersion == null) {
            initLocalVersions();
        }
        return localJdkVersion;
    }

    public Path getApplicationManifestPath(String application) {
        return getApplicationsPath().resolve(application).resolve(DOMAIN_APPLICATION_MANIFEST);
    }

    public Path getApplicationsPath() {
        return getDomainPath().resolve(DOMAIN_APPLICATIONS_DIR);
    }

    public Path getDomainPath() {
        return Paths.get(domain.getServerRootPath(), DOMAINS_PATH, domain.getDomainName());
    }

    public Path getDomainConfigPath() {
        return getDomainPath().resolve(DOMAIN_CONFIG_DIR);
    }

    protected void refreshApplicationStatus() {
        List<Application> configuredApplications = domain.getApplications();
        if (configuredApplications == null || configuredApplications.isEmpty()) {
            logger.log(Level.CONFIG, "No applications configured, trying to detect installed applications...");
            try {
                Set<String> installedApplications = getInstalledApplications();
                if (installedApplications != null) {
                    List<Application> apps = new ArrayList<>();
                    for (String applicationName : installedApplications) {
                        Application app = new Application(applicationName);
                        apps.add(app);
                    }
                    domain.setApplications(apps);
                }
            } catch (Exception e) {
                logger.log(Level.WARNING, "Could not detect installed applications: {0}", e);
            }
        }

        configuredApplications = domain.getApplications();
        if (configuredApplications != null) {
            for (Application application : configuredApplications) {
                // Refresh local version info
                Path applicationManifestPath = getApplicationManifestPath(application.getName());
                application.setVersion(updater.getInstalledApplicationVersion(applicationManifestPath));
                String configuredlUpdateUrl = application.getUpdateUrl();
                if (configuredlUpdateUrl == null || configuredlUpdateUrl.isEmpty()) {
                    // Try to refresh update URL from Manifest (if set)
                    application.setUpdateUrl(updater.getInstalledApplicationUpdateUrl(applicationManifestPath));
                }
                // Refresh remote version info (ApplicationStatus)
                application.setStatus(updater.getApplicationStatus(application.getVersion(), application.getUpdateUrl()));
            }
        } else {
            logger.log(Level.INFO, "No applications found.");
        }
    }

    protected void copyDomainTemplateFiles(final File template) throws IOException {
        FileUtils.copyDirectory(template, getDomainPath().toFile(), (File pathname) -> {
            if (pathname.isFile() && (pathname.getAbsolutePath().endsWith(template.getName() + File.separator + DOMAIN_XML_FILE)
                    || pathname.getAbsolutePath().endsWith(template.getName() + File.separator + DOMAIN_SETUP_SCRIPT))) {
                return false;
            } else if (pathname.getAbsolutePath().endsWith(template.getName() + File.separator + DOMAIN_TEMPLATE_APPLICATIONS_DIR)) {
                return false;
            }
            return true;
        });
    }

    protected void setupDomain(final File template) {
        // Setup the domain according to the setup script
        Path templatePath = template.toPath();
        File setupScript = templatePath.resolve(DOMAIN_SETUP_SCRIPT).toFile();
        if (setupScript.isFile()) {
            ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
            // Fallback
            if (engine == null) {
                engine = new ScriptEngineManager().getEngineByExtension("js");
            }
            if (engine != null) {
                logger.log(Level.CONFIG, "Processing domain setup script " + DOMAIN_SETUP_SCRIPT);
                Bindings bindings = engine.createBindings();
                bindings.put("server", this);
                bindings.put("templatePath", templatePath.toString());
                bindings.put("templateApplicationsPath", templatePath.resolve(DOMAIN_TEMPLATE_APPLICATIONS_DIR).toString());
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);

                FileReader fr = null;
                try {
                    fr = new FileReader(setupScript);
                    engine.eval(fr);
                } catch (FileNotFoundException | ScriptException ex) {
                    logger.log(Level.SEVERE, "Could not evaluate domain setup script file ''{0}''! {1}", new Object[]{setupScript.getAbsolutePath(), ex});
                } finally {
                    try {
                        if (fr != null) {
                            fr.close();
                        }
                    } catch (IOException ex) { /* ignore */

                    }
                }
            }
        }
    }

    protected File createTemporaryAsadminPasswordFile(String password) {
        File temp = null;
        BufferedWriter bw = null;
        try {
            temp = File.createTempFile("dm" + System.currentTimeMillis(), ".tmp");
            bw = new BufferedWriter(new FileWriter(temp));
            bw.write("AS_ADMIN_PASSWORD=" + password + "\n");
            bw.write("AS_ADMIN_MASTERPASSWORD=" + password);
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Could not create temporary asadmin password file! ", ex);
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (Exception e) { /* ignore */ }
        }

        return temp;
    }
}
