/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.boundary;

import javax.xml.bind.annotation.XmlEnum;
import org.jfx4ee.adm4ee.business.server.control.glassfish.Glassfish;
import org.jfx4ee.adm4ee.business.server.control.wildfly.Wildfly;

/**
 * Handle different server types
 * 
 * @author Jens Deters
 */
@XmlEnum
public enum ServerType {

    GLASSFISH("Glassfish", Glassfish.class),
    //WILDLFY_DOMAIN("WildFly (domain)", Glassfish.class),
    WILDFLY_STANDALONE("WildFly (standalone)", Wildfly.class),
    JBOSS_STANDALONE("JBoss (standalone)", Wildfly.class);

    private final String name;
    private final Class<?> clazz;

    ServerType(String name, Class<?> clazz) {
        this.name = name;
        this.clazz = clazz;
    }

    /**
     * Get the server type name (e.g. glassfish)
     * @return the server type name
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    
    
    /**
     * Get the implementing class
     * @return the implementing class
     */
    public Class<?> getImplementingClass() {
        return clazz;
    }

    /**
     * Find the specified server type name
     * @param name The server type name (e.g. glassfish)
     * @return the server type
     */
    public static ServerType findByName(String name) {
        for (ServerType serverType : values()) {
            if (serverType.getName().equals(name)) {
                return serverType;
            }
        }
        return null;
    }
}
