/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.wildfly;

/**
 *
 * @author Jens Deters
 */
public enum ServiceType {
    MANAGEMENT_HTTP(9990, "//socket-binding-group/socket-binding[@name='management-http']", "port"),
    MANAGEMENT_HTTPS(9993, "//socket-binding-group/socket-binding[@name='management-https']", "port"),
    HTTP(8080, "//socket-binding-group/socket-binding[@name='http']", "port"),
    HTTPS(8443, "//socket-binding-group/socket-binding[@name='https']", "port");

    int portBaseOffset;
    String xPathNode;
    String portAttributeName;

    ServiceType(int portBaseOffset, String xPathNode, String portAttributeName) {
        this.portBaseOffset = portBaseOffset;
        this.xPathNode = xPathNode;
        this.portAttributeName = portAttributeName;
    }

    public String getXPathNode() {
        return xPathNode;
    }

    public String getPortAttributeName() {
        return portAttributeName;
    }

    public int getPortForPortBase(int portBase) {
        return portBase + portBaseOffset;
    }
}
