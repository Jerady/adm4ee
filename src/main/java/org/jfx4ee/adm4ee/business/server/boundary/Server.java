/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.boundary;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.util.CommandResult;

/**
 *
 * @author Jens Deters
 */
public interface Server {

    public static final String URL_HOST = "localhost";
    
    /**
     * Initialize the local application server client.
     * Before calling this function the server root path must be available!
     */
    public void initLocalServer();
    
    /**
     * Initialize the client for local/remote communication with the domain.
     * The initLocalServer() method needs to be called in advance for server initialization!
     * Before calling this function all required domain information must to be available,
     * e.g. domain name, domain user, domain password
     */
    public void initLocalDomain();
    
    /**
     * Initialize the application server specific client for remote only communication with the server/domain.
     * Before calling this function all required server/domain information must to be available,
     * e.g. local server root path
     */
    public void initRemoteDomain();
    
        /**
     * List all local domains that are available for the server instance under the specified server root path
     * @param serverRootPath The server root path to look for domains
     * @return List of domain directory names
     */
    public File[] listLocalDomains(String serverRootPath);
    
    /**
     * Get the current application server version (e.g. GlassFish Server Open Source Edition 3.1.2.2 (build 5))
     * @return Application server version string
     */
    public String getVersionString();
    
    /**
     * Get the JDK version string used for the currently connected domain (e.g. 1.7.0_40)
     * @return JDK version string
     */
    public String getJdkVersionString();
    
    /**
     * Get the domain configuration this server is initialized for
     * @return The domain configuration of this server instance
     */
    public Domain getDomain();
    
    /* ------------ domain.xml handling -------------- */
    
    /**
     * Get all services with their corresponding ports as configured in the domain.xml
     * @return services with their ports
     */
    public Map<String, Integer> getServicePorts();
    
    /**
     * Set all services with their corresponding ports using the given port base
     * @param portBase The port base for the service ports
     */
    public void setServicePorts(int portBase);
    
    /**
     * Set the port for the specified service type in the domain.xml to the given value
     * @param service The name of the service
     * @param port The port value (e.g. 8080)
     * @return The port set in domain.xml, or null in case of an error
     */
    public Integer setServicePort(String service, int port);
    
    /**
     * Save the current domain configuration
     */
    public void saveDomainConfiguration();
    
    /**
     * Verify the current domain configuration if it is valid
     * @return command result, never null
     */
    public CommandResult verifyDomainConfiguration();
    
    /* ----------- Basic domain management ------------- */
    
    /**
     * Create a domain with the specified domain name and the given portBase according to the
     * given domain template
     * @param domainName The domain name
     * @param portBase The port base
     * @param template The domain template
     * @param user The admin user name
     * @param password The admin user's password
     * @param checkPorts If set to true ports will be checked during creation.
     * @return command result, never null;
     */
    public CommandResult createDomain(String domainName, int portBase, File template, String user, String password, boolean checkPorts);
    
    /**
     * Delete the specified domain and all of its contents
     * @param domainName The domain name to be deleted
     * @return command result, never null
     */
    public CommandResult deleteDomain(String domainName);
    
    /**
     * Start the specified domain
     * @param domainName The domain name to be started
     * @return command result, never null
     */
    public CommandResult startDomain(String domainName);
    
    /**
     * Stop the domain currently connected to.
     * @return command result, never null
     */
    public CommandResult stopDomain();

    /**
     * List all 3rd party libraries that are currently installed for this domain
     * @return 3rd party libraries
     */
    public Set<String> listLibraries();
    
    /**
     * Change the admin password.
     * This command only works when the domain is running.
     * @param oldPassword The current admin password
     * @param newPassword The new admin password
     * @return the command result
     */
    public CommandResult changeAdminPassword(String oldPassword, String newPassword);
    
    /**
     * Ping the management resource of the currently connected domain
     * @return command result, never null
     */
    public CommandResult pingManagementResource();
    
    /**
     * Get the specified number of characters from the end of the server log file.
     * This command works in offline and online mode.
     * @param characters Number of characters to fetch from the end of the server log file
     * @return command result containing log file content, never null
     */
    public CommandResult getLogFileContent(long characters);
    
    /**
     * Get the uptime in milliseconds.
     * This command only works when the domain is running.
     * @return Uptime
     */
    public long getUptime();
    
    /**
     * Get all currently installed applications of this domain.
     * This command only works when the domain is running.
     * @return the installed applications
     */
    public Set<String> getInstalledApplications();
    
    /**
     * Get all configured applications of this domain.
     * @return configured applications
     */
    public List<Application> getApplications();
    
    /**
     * Create (Deploy) the specified application
     * @param application The application name. If this is null, it is taken from the file
     * @param contextroot The context root. If this is null, it is taken from the file
     * @param file The application file to be deployed
     * @param description A description for this application
     * @return the command result
     */
    public CommandResult createApplication(String application, String contextroot, File file, String description);
    
    /**
     * Delete (Undeploy) the specified application.
     * This command only works when the domain is running.
     * @param application The application to be deleted
     * @return the command result
     */
    public CommandResult deleteApplication(String application);
    
    /**
     * Check if there is an update for the given application and update it if so.
     * @param applicationName The name of the application
     * @return the command result
     */
    public CommandResult updateApplication(String applicationName);
    
    /* ------------- JDBC ----------------- */
    
    /**
     * Get all JDBC connection pools of this domain.
     * This command only works when the domain is running.
     * @return The JDBC connection pools
     */
    public Set<String> getJdbcConnectionPools();
    
    /**
     * Ping the specified JDBC connection pool.
     * This command only works when the domain is running.
     * @param poolName The JDBC pool name to be pinged
     * @return command result, never null
     */
    public CommandResult pingJdbcConnectionPool(String poolName);
    
    /**
     * Get all parameters of the specified JDBC connection pool (basically User, Password, URL,...).
     * This command only works when the domain is running.
     * @param poolName The JDBC pool name
     * @return The parameters and their values
     */
    public Map<String, String> getJdbcConnectionPoolParameters(String poolName);
    
    /**
     * Set the parameters of the specified JDBC connection pool (basically User, Password, URL,...).
     * The given parameters overwrite any existing ones!
     * This command only works when the domain is running.
     * @param poolName The JDBC pool name
     * @param parameters The parameters to be set
     * @return command result
     */
    public CommandResult setJdbcConnectionPoolParameters(String poolName, Map<String, String> parameters);
    
    /**
     * Create a JDBC resource with the given resource ID for the specified pool name
     * @param resourceId The JDBC resource ID to be created
     * @param poolName The JDBC pool name to be used
     * @return command result
     */
    public CommandResult createJdbcResource(String resourceId, String poolName);
    
    /**
     * Delete the JDBC resource with the given resource ID
     * @param resourceId The JDBC resource ID to be deleted
     * @return command result
     */
    public CommandResult deleteJdbcResource(String resourceId);
    
    /**
     * Close the initialized connection to the server/domain
     */
    public void close();
}
