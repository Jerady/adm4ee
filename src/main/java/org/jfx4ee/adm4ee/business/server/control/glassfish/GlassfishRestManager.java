/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.glassfish;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.client.filter.CsrfProtectionFilter;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.jfx4ee.adm4ee.ApplicationInfo;
import org.jfx4ee.adm4ee.business.util.CommandResult;

/**
 *
 * @author Jens Deters
 */
public class GlassfishRestManager {

    private static final Logger logger = Logger.getLogger(GlassfishRestManager.class.getName());
    private static final String APPLICATION_NAME = ApplicationInfo.APPLICATION_NAME;
    
    private static final String MANAGEMENT_RESOURCE = "/management/domain";
    private static final String JSON_OBJECT_MESSAGE = "message";
    private static final String JSON_OBJECT_PROPERTIES = "properties";
    private static final String JSON_OBJECT_EXTRA_PROPERTIES = "extraProperties";
    private static final String JSON_OBJECT_CHILD_RESOURCES = "childResources";
    private static final String JSON_ARRAY_PROPERTIES = "properties";
    private static final String JSON_ARRAY_CHILDREN = "children";
    private static final String JSON_OBJECT_NAME = "name";
    private static final String JSON_OBJECT_VALUE = "value";

    private static final Pattern PATTERN_VIEW_LOG_START = Pattern.compile("http://.*?start=(\\d+)");
    private static final String DEFAULT_URL_ENCODING = "UTF-8";
    private static final int DEFAULT_CLIENT_CONNECT_TIMEOUT = 10000; // millis
    private static final int DEFAULT_CLIENT_READ_TIMEOUT = 8000; // millis

    Client client;
    WebTarget managementResource;

    public GlassfishRestManager(String url, String username, String password, boolean useSSL) {
        // Try to read username and password for authentication
        client = ClientBuilder.newClient(new ClientConfig()
                // The line bellow that registers JSON-Processing feature can be
                // omitted if FEATURE_AUTO_DISCOVERY_DISABLE is not disabled.
                .register(JsonProcessingFeature.class).property(JsonGenerator.PRETTY_PRINTING, false)
                .register(MultiPartFeature.class)
                .property(ClientProperties.CONNECT_TIMEOUT, DEFAULT_CLIENT_CONNECT_TIMEOUT)
                .property(ClientProperties.READ_TIMEOUT, DEFAULT_CLIENT_READ_TIMEOUT));

        // CSRF Protection (X-Requested-By header) is required
        client.register(new CsrfProtectionFilter(APPLICATION_NAME));
        // Register Authenticator for REST interface
        client.register(HttpAuthenticationFeature.basic(username, password));
        managementResource = client.target(getProtocol(useSSL) + url + MANAGEMENT_RESOURCE);
    }

    public CommandResult ping() {
        return handleJsonResponse(managementResource.path("version").request(MediaType.APPLICATION_JSON).get(Response.class));
    }

    public CommandResult stopDomain() {
        JsonObject entity = Json.createObjectBuilder().add("force", "true").build();
        return handleJsonResponse(post("stop", entity));
    }

    public CommandResult changeAdminPassword(String oldPassword, String newPassword) {
        JsonObject parameters = Json.createObjectBuilder()
                .add("id", "admin")
                .add("AS_ADMIN_PASSWORD", oldPassword)
                .add("AS_ADMIN_NEWPASSWORD", newPassword)
                .build();
        return handleJsonResponse(post("change-admin-password", parameters));
    }
    
    public CommandResult deleteDomain() {
        return handleJsonResponse(delete(""));
    }
    
    public Set<String> listLibraries() {
        return getChildren("list-libraries");
    }
    
    public Set<String> getApplications() {
        return getChildResources("applications/application");
    }
    
    public CommandResult createApplication(String application, String contextroot, File file, String description) {
        if (file == null) {
            throw new IllegalArgumentException("No valid file 'null'!");
        }
        // Deploy need to be multipart
        // file content must be posted as multipart/form-data
        FileDataBodyPart filePart = new FileDataBodyPart("id", file);
        FormDataMultiPart formpart = new FormDataMultiPart();
        if (application != null) {
            formpart = formpart.field("name", application);
        }
        if (contextroot != null) {
            formpart = formpart.field("contextroot", contextroot);
        }
        if (description != null) {
            formpart = formpart.field("description", description);
        }
        MultiPart multipart = formpart.field("force", "true")
                .bodyPart(filePart);

        return handleJsonResponse(managementResource.path("applications/application")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(multipart, multipart.getMediaType()), Response.class));
    }
    
    public CommandResult deleteApplication(String application) {
        return handleJsonResponse(delete("applications/application/" + urlEncode(application)));
    }

    public Set<String> getJdbcConnectionPools() {
        return getChildResources("resources/jdbc-connection-pool");
    }

    public Map<String, String> getJdbcConnectionPoolParameters(String poolName) {
        JsonArray properties = getExtraProperties("resources/jdbc-connection-pool/" + urlEncode(poolName) + "/property");
        if (properties == null) {
            return null;
        }
        Map<String, String> result = new HashMap<>();
        for (JsonValue value : properties) {
            if (value instanceof JsonObject) {
                JsonObject entry = (JsonObject)value;
                result.put(entry.getString(JSON_OBJECT_NAME), entry.getString(JSON_OBJECT_VALUE));
            }
        }
        return result;
    }
    
    public CommandResult setJdbcConnectionPoolParameters(String poolName, Map<String, String> parameters) {
        if (parameters == null) {
            return null; 
        }
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Map.Entry<String,String> entry : parameters.entrySet()) {
            arrayBuilder.add(Json.createObjectBuilder().add(JSON_OBJECT_NAME, entry.getKey()).add(JSON_OBJECT_VALUE, entry.getValue()).build());
        }
        return handleJsonResponse(post("resources/jdbc-connection-pool/" + urlEncode(poolName) + "/property" ,arrayBuilder.build()));
    }
    
    public CommandResult pingJdbcConnectionPool(String poolName) {
        return handleJsonResponse(managementResource.path("resources/ping-connection-pool/").queryParam("id", poolName).request(MediaType.APPLICATION_JSON).get(Response.class));
    }
    
    public CommandResult createJdbcConnectionPool(String connectionPoolI) {
        return null;
    }
    
    public CommandResult deleteJdbcConnectionPool(String poolName, boolean cascade) {
        return null;
    }
    
    public CommandResult createJdbcResource(String resourceId, String poolName) {
        JsonObject parameters = Json.createObjectBuilder()
                .add("id", resourceId)
                .add("poolName", poolName)
                .build();
        return handleJsonResponse(post("resources/jdbc-resource", parameters));
    }
    
    public CommandResult deleteJdbcResource(String connectionPoolId) {
        return handleJsonResponse(delete("resources/jdbc-resource/" + urlEncode(connectionPoolId)));
    }
    

    public String getUptime() {
        return getPropertiesValue("uptime", "milliseconds");
    }

    public String getVersion() {
        return getMessage("version");
    }

    public long getLogFilePosition() {
        long position = 0;
        // Fetch only head to get the X-Text-Append-Next: header from Glassfish
        Response headResponse = managementResource.path("view-log").request(MediaType.TEXT_PLAIN).head();
        String appendNext = headResponse.getHeaderString("X-Text-Append-Next");
        Matcher startMatcher = PATTERN_VIEW_LOG_START.matcher(appendNext);
        if (startMatcher.find()) {
            position = Long.parseLong(startMatcher.group(1));
        }
        return position;
    }

    public String getLogFileContent(long fromCharacter) {
        return managementResource.path("view-log").queryParam("start", fromCharacter).request(MediaType.TEXT_PLAIN).get(String.class);
    }

    public String getManagementUrl() {
        return managementResource.getUri().toString();
    }

    /**
     * @deprecated 
     * @param path
     * @return 
     */
    public String getMessage(String path) {
        JsonObject result = managementResource.path(path).request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        return result.getString(JSON_OBJECT_MESSAGE);
    }

    public Set<String> getChildren(String path) {
        JsonObject result = managementResource.path(path).request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        JsonArray childrenEntries = result.getJsonArray(JSON_ARRAY_CHILDREN);
        if (childrenEntries == null) {
            return null;
        }
        Set<String> children = new HashSet<>();
        for (JsonValue value : childrenEntries) {
            if (value instanceof JsonObject) {
                JsonObject entry = (JsonObject)value;
                children.add(entry.getString(JSON_OBJECT_MESSAGE));
            }
        }
        return children;
    }
    
    public String getPropertiesValue(String path, String property) {
        JsonObject result = managementResource.path(path).request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        result = result.getJsonObject(JSON_OBJECT_PROPERTIES);
        if (result == null) {
            return null;
        }
        return result.getString(property);
    }
    
    public Set<String> getChildResources(String path) {
        JsonObject result = managementResource.path(path).request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        result = result.getJsonObject(JSON_OBJECT_EXTRA_PROPERTIES);
        if (result == null) {
            return null;
        }
        result = result.getJsonObject(JSON_OBJECT_CHILD_RESOURCES);
        if (result == null) {
            return null;
        }
        return result.keySet();
    }
    
    public JsonArray getExtraProperties(String path) {
        JsonObject result = managementResource.path(path).request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        result = result.getJsonObject(JSON_OBJECT_EXTRA_PROPERTIES);
        if (result == null) {
            return null;
        }
        return result.getJsonArray(JSON_ARRAY_PROPERTIES);
    }

    protected Response post(String path, JsonValue entity) {
        return managementResource.path(path)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(entity, MediaType.APPLICATION_JSON), Response.class);
    }
    
    protected Response delete(String path) {
        return managementResource.path(path)
                .request(MediaType.APPLICATION_JSON)
                .delete(Response.class);
    }
    
    protected CommandResult handleJsonResponse(Response response) {
        CommandResult result = new CommandResult();
        if (response != null) {
            // Check HTTP Status
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                result.setExitCode(response.getStatus());
                result.setExitCodeInfo(response.getStatusInfo().toString());
            }
            try {
                JsonObject json = response.readEntity(JsonObject.class);
                JsonString message = json.getJsonString(JSON_OBJECT_MESSAGE);
                if (message != null) {
                    result.setResult(message.toString());
                }
                JsonString exitCode = json.getJsonString("exit_code");
                if (exitCode != null) {
                    if ("SUCCESS".equals(exitCode.getString())) {
                        result.setExitCodeOk();
                    }
                }
            } catch (Exception ex) {
                result.setResult("Could not read result from server.");
                result.setThrowable(ex);
            }
        }
        return result;
    }
    
    private String urlEncode(String value) {
        String encoded = null;
        try {
            encoded = URLEncoder.encode(value, DEFAULT_URL_ENCODING);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            logger.log(Level.WARNING, "Could not encoded URL using ''UTF-8'': {0}", ex);
            encoded = value;
        }
        return encoded;
    }

    private String getProtocol(boolean useSSL) {
        String protocol = "http://";
        if (useSSL) {
            protocol = "https://";
            logger.config("Using SSL");
        }
        return protocol;
    }
    
    public void close() {
        client.close();
    }
}
