/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.updater;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.jfx4ee.adm4ee.business.appcast.boundary.AppcastManager;
import org.jfx4ee.adm4ee.business.appcast.control.AppcastException;
import org.jfx4ee.adm4ee.business.appcast.entity.Appcast;
import org.jfx4ee.adm4ee.business.server.boundary.Server;

/**
 *
 * @author Jens Deters
 */
public class Updater {

    private static final Logger logger = Logger.getLogger(Updater.class.getName());
    private static final String UPDATE_SCRIPT_SUFFIX = "-update.js";

    AppcastManager appcastManager;
    ZipHandler zipHandler;
    Server server;

    public Updater(Server server) {
        appcastManager = new AppcastManager();
        zipHandler = new ZipHandler();
        this.server = server;
    }

    /**
     * Get the installed application version of the specified MANIFEST.MF file path
     *
     * @param manifestPath the file path
     * @return the application version
     */
    public String getInstalledApplicationVersion(Path manifestPath) {
        String version = null;
        try {
            version = appcastManager.getManifestAppcastVersion(manifestPath);
            if (version == null) {
                // The application seems to be installed, but we could not fetch
                // the appcast version from the manifest file, so set the version to ""
                version = "";
            }
        } catch (IOException ex) {
            // Seems that we cannot access the manifest file
            // We assume that the application is not installed at all
            // So return 'null';
        }

        return version;
    }
    
    /**
     * Get the installed application update url from the specified MANIFEST.MF file path
     * @param manifestPath the file path
     * @return the application update url, or 'null' if not specified
     */
    public String getInstalledApplicationUpdateUrl(Path manifestPath) {
        String url = null;
        try {
            url = appcastManager.getManifestAppcastUrl(manifestPath);
        } catch (IOException ex) {
            // Seems that we cannot access the manifest file
        }

        return url;
    }

    /**
     * Get the update status of the application specified.
     *
     * @param localVersion The local version string, e.g. "2.0.1344"
     * @param updateUrl The update URL (Appcast URL)
     * @return The application status or 'null' if the status could not be evaluated
     */
    public ApplicationStatus getApplicationStatus(String localVersion, String updateUrl) {
        ApplicationStatus status = ApplicationStatus.UNKNOWN;
        if (localVersion != null) {
            if (!localVersion.isEmpty()) {
                if (updateUrl != null && !updateUrl.isEmpty()) {
                    // Fetch remote version
                    String remoteVersion = null;
                    try {
                        logger.log(Level.FINE, "Fetching appcast from update URL ''{0}''...", updateUrl);
                        Appcast appcast = appcastManager.fetch(updateUrl);
                        if (appcast != null) {
                            remoteVersion = appcast.getLatestRemoteVersion();
                        }
                        if (remoteVersion == null) {
                            status = ApplicationStatus.FAILURE;
                            status.setInfo("No version information found");
                        } else {
                            VersionComparator vc = new VersionComparator();
                            int compare = vc.compare(localVersion, remoteVersion);
                            if (compare == 0) {
                                status = ApplicationStatus.OK;
                            } else if (compare == -1) {
                                status = ApplicationStatus.UPDATE_AVAILABLE;
                                status.setInfo(remoteVersion);
                                status.setAppcast(appcast);
                            }
                        }
                    } catch (AppcastException aex) {
                        logger.log(Level.WARNING, "{0} ''{1}'': {2} {3}", new Object[]{aex.getMessage(), aex.getUrl(), aex.getStatus(), aex.getStatusInfo()});
                        status = ApplicationStatus.FAILURE;
                        status.setInfo(aex.getMessage() + " '" + aex.getUrl() + "': " + aex.getStatus() + " " + aex.getStatusInfo());
                    } catch (Exception ex) {
                        // Seems the be a network problem (e.g. no internet connection)
                        // Just log it, status should be unknown
                        logger.log(Level.WARNING, "Could not connect to update server: {0}", ex.toString());
                    }
                    status.setUpdateTime(new Date());
                }
            }
        } else {
            // No version information about installed application!?
            // Seems to be not installed at all
            status = ApplicationStatus.NOT_INSTALLED;
        }
        return status;
    }

    /**
     * Update with the given appcast information in the specified targetDir
     *
     * @param applicationName
     * @param appcast
     * @param targetDir
     * @return Updated files
     * @throws Exception
     */
    public Set<Path> update(String applicationName, Appcast appcast, Path targetDir) throws Exception {
        Set<Path> files = null;
        if (appcast == null) {
            throw new IllegalArgumentException("Appcast cannot be null!");
        }
        logger.log(Level.INFO, "Updating application ''{0}''...", applicationName);

        // Download the update and verfiy it
        Path downloaded = appcastManager.download(appcast, targetDir);
        if (downloaded == null) {
            throw new Exception("Could not download update package for application '" + applicationName + "'!");
        }
        logger.log(Level.INFO, "Downloaded update package ''{0}''", downloaded);

        // Unzip the update if required
        files = zipHandler.unzip(downloaded, targetDir, true);
        logger.log(Level.INFO, "Extracted files: {0}", files);

        // Check if there is an update script available and execute it if so
        for (Path filePath : files) {
            if (filePath.getFileName().toString().endsWith(UPDATE_SCRIPT_SUFFIX)) {
                executeUpdateScript(filePath, targetDir);
            }
        }

        return files;
    }

    protected void executeUpdateScript(Path filePath, Path targetDir) {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        // Fallback
        if (engine == null) {
            engine = new ScriptEngineManager().getEngineByExtension("js");
        }
        if (engine != null) {
            logger.log(Level.INFO, "Executing update script ''{0}''...", filePath);
            Bindings bindings = engine.createBindings();
            bindings.put("server", this.server);
            bindings.put("path", targetDir.toString());
            engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);

            FileReader fr = null;
            try {
                fr = new FileReader(filePath.toFile());
                engine.eval(fr);
            } catch (FileNotFoundException | ScriptException ex) {
                logger.log(Level.SEVERE, "Could not evaluate update script file ''{0}''! {1}", new Object[]{filePath, ex});
            } finally {
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException ex) { /* ignore */ }
            }
        }
    }
}
