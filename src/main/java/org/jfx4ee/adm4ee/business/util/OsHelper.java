/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.util;

/**
 * OS helper functions
 * 
 * @author Dino Tsoumakis
 */
public class OsHelper {
    /* --------------------------- Constants ---------------------------- */
    private static final String OS_NAME = System.getProperty("os.name").toLowerCase();
    private static final String JVM_BITNESS = System.getProperty("sun.arch.data.model");
    
    /* --------------------------- Attributes --------------------------- */
    
    /* --------------------------- Methods ------------------------------ */
 
    public static boolean isWindows() {
		return (OS_NAME.indexOf("win") >= 0);
 	}
 
	public static boolean isMac() {
 		return (OS_NAME.indexOf("mac") >= 0);
 	}
 
	public static boolean isUnix() {
		return (OS_NAME.indexOf("nix") >= 0 || OS_NAME.indexOf("nux") >= 0);
	}
    
    public static String getOsName() {
        return OS_NAME;
    }
    
    public static boolean is32BitVM() {
        return (JVM_BITNESS.equals("32"));
    }
    
    public static String getJvmBitness() {
        return JVM_BITNESS;
    }
}
