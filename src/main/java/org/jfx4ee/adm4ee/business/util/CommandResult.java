/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.util;

/**
 * A command result wrapper
 *
 * @author Dino Tsoumakis
 */
public class CommandResult {

    public static final int EXIT_CODE_UNKNOWN = Integer.MIN_VALUE;
    public static final int EXIT_CODE_NOT_FINISHED = -999999;
    public static final int EXIT_CODE_OK = 0;

    Throwable throwable;
    String result;
    int exitCode = EXIT_CODE_UNKNOWN;
    String exitCodeInfo;

    public CommandResult() {
        // Nothing to do
    }
    
    public CommandResult(String result) {
        this.result = result;
        this.exitCode = EXIT_CODE_OK;
    }

    public CommandResult(Throwable throwable) {
        this.throwable = throwable;
    }

    public int getExitCode() {
        return exitCode;
    }

    public String getResult() {
        return result;
    }

    public boolean isOk() {
        return exitCode == EXIT_CODE_OK;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getExitCodeInfo() {
        return exitCodeInfo;
    }

    public void setExitCodeInfo(String exitCodeInfo) {
        this.exitCodeInfo = exitCodeInfo;
    }

    public void setExitCode(int exitCode) {
        this.exitCode = exitCode;
    }

    public void setExitCodeOk() {
        this.exitCode = EXIT_CODE_OK;
    }

    @Override
    public String toString() {
        return getResult();
    }
}
