/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Execute a system command
 *
 * @author Dino Tsoumakis
 */
public class SystemCommandExecutor {

    public static final int DEFAULT_BUFFER_SIZE = 1024;
    public static final int DEFAULT_COMMAND_TIMEOUT_MSEC = 8000; // seconds

    /**
     * Execute a system command an fetch its result
     *
     * @param commandLine The command line
     * @return The result of the command
     */
    public static CommandResult execute(List<String> commandLine) {
        return execute(commandLine, DEFAULT_BUFFER_SIZE, DEFAULT_COMMAND_TIMEOUT_MSEC, true);
    }

    public static CommandResult execute(List<String> commandLine, int timeout) {
        return execute(commandLine, DEFAULT_BUFFER_SIZE, timeout, true);
    }

    /**
     * Execute a system command an fetch its result
     *
     * @param commandLine The command line
     * @param bufferSize buffer size of the output buffer
     * @param timeout The command wait timeout in milliseconds
     * @param destroyAfterTimeout If set to true, try to destroy a stalled process if not finished after timeout
     * @return The result of the command, never null
     */
    public static CommandResult execute(List<String> commandLine, int bufferSize, int timeout, boolean destroyAfterTimeout) {
        CommandResult result = new CommandResult();
        StringBuilder output = new StringBuilder(bufferSize);
        InputStream is = null;
        BufferedReader in = null;

        try {
            ProcessBuilder pb = new ProcessBuilder(commandLine).redirectErrorStream(true);
            Process process = pb.start();

            // Initialize process output stream
            is = process.getInputStream();
            in = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));

            // Wait for the process
            if (process.waitFor(timeout, TimeUnit.MILLISECONDS)) {
                // Fetch process output
                final char[] buffer = new char[bufferSize];
                while (is.available() > 0 && output.length() < bufferSize) {
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0) {
                        break;
                    }
                    output.append(buffer, 0, rsz).append("\n");
                }
                result.setExitCode(process.exitValue());
            } else {
                if (process.isAlive()) {
                    if (destroyAfterTimeout) {
                        // Try to stop the process to avoid stalled processes
                        Throwable throwable = null;
                        try {
                            process.destroy();
                            throwable = new Exception("Command not finished within wait time!");
                        } catch (Exception ex) {
                            throwable = new Exception("Command not finished within wait time! Could not abort command execution! ", ex);
                        } finally {
                            result.setThrowable(throwable);
                        }
                        result.setExitCode(CommandResult.EXIT_CODE_NOT_FINISHED);
                    } else {
                        result.setExitCodeOk();
                    }
                } else {
                    result.setExitCode(process.exitValue());
                }
            }

        } catch (IOException | InterruptedException iex) {
            result.setThrowable(new Exception("Could not execute process '" + commandLine + "'! ", iex));
        } finally {
            try { if (in != null) in.close(); } catch (IOException ex) { /* Ignore */ }
            try { if (is != null) is.close(); } catch (IOException ex) { /* Ignore */ }
        }
        result.setResult(output.toString());

        return result;
    }
}
