/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;
import javax.enterprise.util.AnnotationLiteral;
import org.jboss.weld.environment.se.Weld;
import org.jfx4ee.adm4ee.ApplicationInfo;
import org.jfx4ee.adm4ee.business.cli.CliManager;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.logging.LogManager;
import org.jfx4ee.adm4ee.presentation.cdi.DefaultStartupEvent;
import org.jfx4ee.adm4ee.presentation.cdi.StartupEvent;

/**
 *
 * @author Jens Deters
 */
public class Run extends Application {

    private static final Logger logger = Logger.getLogger(Run.class.getName());

    private Weld weld;

    @Override
    public void init() throws Exception {
        super.init();

        // Read the preferences
        ConfigurationManager config = ConfigurationManager.getInstance();

        // Init logging
        LogManager.initLogging(config.getLogLevel());
        logger.log(Level.INFO, "--------------------------------------------");
        logger.log(Level.INFO, "Starting " + ApplicationInfo.APPLICATION_NAME);

        // Init Weld CDI
        weld = new Weld();

        // Get the command line parameters
        logger.log(Level.CONFIG, "Parsing command line options...");
        CliManager cli = new CliManager();
        List<String> params = new ArrayList<>();
        params.addAll(getParameters().getRaw());
        params.addAll(getParameters().getUnnamed());
        cli.processCommandLine(params);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        weld.initialize().event().select(new AnnotationLiteral<DefaultStartupEvent>() {
        }).fire(new StartupEvent(primaryStage, getParameters()));
    }

    @Override
    public void stop() throws Exception {
        weld.shutdown();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
