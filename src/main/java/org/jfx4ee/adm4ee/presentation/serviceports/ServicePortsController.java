/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.serviceports;

import java.util.ResourceBundle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javax.inject.Inject;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Jens Deters
 */
public class ServicePortsController {

    @Inject
    private ViewContext viewContext;
    private DomainDataModel domainDataModel;
    private ModalDialog modalDialog;
    private ChangeServicePortsService changeServicePortsService;
    @FXML
    private ResourceBundle resources;
    @FXML
    private TableView<ServicePort> servicePortsTableView;
    @FXML
    private TableColumn<ServicePort, String> serviceNameColumn;
    @FXML
    private TableColumn<ServicePort, Integer> servicePortColumn;
    @FXML
    private ObservableList<ServicePort> servicePortsList;
    @FXML
    private Button saveServicePortsButton;
    @FXML
    private Button changePortBaseButton;
    @FXML
    private Button closeButton;
    @FXML
    private Label messagesLabel;
    @FXML
    private ProgressIndicator progressIndicator;

    private BooleanProperty servicePortChangedProperty;

    @FXML
    void initialize() {
        domainDataModel = viewContext.getDomainDataModel();
        modalDialog = viewContext.getModalDialog();
        changeServicePortsService = new ChangeServicePortsService();
        servicePortsList = FXCollections.observableArrayList();
        saveServicePortsButton.setVisible(false);

        domainDataModel.getServicePortsMap().addListener((MapChangeListener.Change<? extends String, ? extends Integer> change) -> {
            servicePortsList.clear();
            for (String serviceName : domainDataModel.getServicePortsMap().keySet()) {
                servicePortsList.add(new ServicePort(serviceName, domainDataModel.getServicePortsMap().get(serviceName)));
            }
        });

        servicePortsTableView.setItems(servicePortsList);
        servicePortsTableView
                .setEditable(true);
        servicePortsTableView.getSelectionModel().setCellSelectionEnabled(true);

        serviceNameColumn.setCellValueFactory(
                new PropertyValueFactory<>("serviceName"));
        servicePortColumn.setCellValueFactory(
                new PropertyValueFactory<>("port"));

        Callback<TableColumn<ServicePort, Integer>, TableCell<ServicePort, Integer>> portCellFactory
                = new Callback<TableColumn<ServicePort, Integer>, TableCell<ServicePort, Integer>>() {
                    @Override
                    public TableCell<ServicePort, Integer> call(TableColumn p) {
                        return new PortEditingCell();
                    }
                };

        servicePortColumn.setEditable(true);
        servicePortColumn.setCellFactory(portCellFactory);
        servicePortColumn.setOnEditCommit(
                (TableColumn.CellEditEvent<ServicePort, Integer> t) -> {
                    ((ServicePort) t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setPort(t.getNewValue());
        });

        bindProperties();
    }

    @FXML
    public void hideAction() {
        domainDataModel.enableAutoupdate();
        modalDialog.hide();
    }

    @FXML
    public void changePortBase() {
        if (confirmChangeServicePorts()) {
            String value = Dialogs.create().showTextInput(resources.getString("serviceports.dialog.portBaseChangeTo"));
            changeServicePortsService.setPortBase(Integer.parseInt(value));
        }
    }

    private boolean confirmChangeServicePorts() {
        Action response = Dialogs.create()
                .owner(viewContext.getPrimaryStage())
                .message(resources.getString("serviceports.dialog.servicePortChangeConfirm"))
                .nativeTitleBar()
                .actions(new Action[]{Dialog.Actions.YES, Dialog.Actions.NO})
                .showConfirm();
        return response == Dialog.Actions.YES;
    }

    @FXML
    public void saveServicePorts() {
        if (confirmChangeServicePorts()) {
            setServicePortChanged(Boolean.FALSE);
            changeServicePortsService.applyServicePortChanges(servicePortsList);
        }
    }

    public BooleanProperty servicePortChangedProperty() {
        if (servicePortChangedProperty == null) {
            servicePortChangedProperty = new SimpleBooleanProperty(Boolean.FALSE);
        }
        return servicePortChangedProperty;
    }

    public void setServicePortChanged(Boolean servicePortChanged) {
        servicePortChangedProperty().set(servicePortChanged);
    }

    public Boolean getServicePortChanged() {
        return servicePortChangedProperty().getValue();
    }

    public Boolean isServicePortChanged() {
        return servicePortChangedProperty().get();
    }

    private void bindProperties() {
        changePortBaseButton.disableProperty().bind(changeServicePortsService.runningProperty());
        changePortBaseButton.visibleProperty().bind(domainDataModel.domainRunningProperty().not());

        closeButton.disableProperty().bind(changeServicePortsService.runningProperty());

        messagesLabel.textProperty().bind(changeServicePortsService.messageProperty());
        progressIndicator.visibleProperty().bind(changeServicePortsService.runningProperty());

        servicePortsTableView.editableProperty().bind(domainDataModel.domainRunningProperty().not());
        saveServicePortsButton.visibleProperty().bind(servicePortChangedProperty());

    }

    public static class ServicePort implements Comparable<ServicePort> {

        private String serviceName;
        private Integer port;

        public ServicePort(String serviceName, Integer port) {
            this.serviceName = serviceName;
            this.port = port;
        }

        public Integer getPort() {
            return port;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setPort(Integer port) {
            this.port = port;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        @Override
        public int compareTo(ServicePort o) {
            if (o.getServiceName() == null || getServiceName() == null) {
                return 0;
            }
            return o.getServiceName().compareTo(getServiceName());
        }
    }

    class PortEditingCell extends TableCell<ServicePort, Integer> {

        private TextField textField;

        public PortEditingCell() {
        }

        @Override
        public void startEdit() {
            if (!isEmpty()) {
                domainDataModel.disableAutoUpdate();
                super.startEdit();
                createTextField();
                setText(null);
                setGraphic(textField);
                textField.selectAll();
            }
        }

        @Override
        public void commitEdit(Integer t) {
            super.commitEdit(t);
            setServicePortChanged(Boolean.TRUE);
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText(String.valueOf(getItem()));
            setGraphic(null);
            domainDataModel.enableAutoupdate();
        }

        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setText(null);
                    setGraphic(textField);
                } else {
                    setText(getString());
                    setGraphic(null);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    private class ChangeServicePortsService extends Service<Void> {

        private int portBase;
        private boolean applyChangesOnly;
        private ObservableList<ServicePortsController.ServicePort> servicePortsList;

        public ChangeServicePortsService() {
            init();
        }

        private void init() {
            applyChangesOnly = false;
            servicePortsList = FXCollections.observableArrayList();
        }

        public void setPortBase(int portBase) {
            this.portBase = portBase;
            applyChangesOnly = false;
            restart();
        }

        public void applyServicePortChanges(ObservableList<ServicePortsController.ServicePort> servicePortsList) {
            this.servicePortsList.setAll(servicePortsList);
            applyChangesOnly = true;
            restart();
        }

        @Override
        protected void succeeded() {
            domainDataModel.refreshDomainState();
        }

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() {
                    domainDataModel.disableAutoUpdate();
                    if (!applyChangesOnly) {
                        updateMessage("changing port base to " + portBase);
                        domainDataModel.getServer().setServicePorts(portBase);
                        domainDataModel.setPortBase(portBase);
                    } else {
                        updateMessage("saving service ports ");
                        for (ServicePortsController.ServicePort servicePort : servicePortsList) {
                            if (servicePort.getServiceName() != null && servicePort.getPort() != null) {
                                System.out.println(servicePort.getServiceName() + ":" + servicePort.getPort());
                                domainDataModel.getServer().setServicePort(servicePort.getServiceName(), servicePort.getPort());
                            }
                        }
                    }
                    domainDataModel.getServer().saveDomainConfiguration();
                    updateMessage("");
                    domainDataModel.enableAutoupdate();
                    return null;
                }
            };
        }
    }
}
