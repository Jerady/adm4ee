/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.adminpassword;

import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javax.inject.Inject;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Lummerland
 */
public class ChangeAdminPasswordController {

    private static final Logger logger = Logger.getLogger(ChangeAdminPasswordController.class.getName());
    @Inject
    private ViewContext viewContext;
    private DomainDataModel domainDataModel;
    private ModalDialog modalDialog;
    @FXML
    private ResourceBundle resources;
    @FXML
    private PasswordField confirmNewAdminPasswordField;
    @FXML
    private PasswordField currentAdminPasswordField;
    @FXML
    private PasswordField newAdminPasswordField;
    @FXML
    private Button applyChangeAdminPasswordButton;
    @FXML
    private Button cancelChangeAdminPasswordButton;
    @FXML
    private Button closeButton;

    @FXML
    void initialize() {
        domainDataModel = viewContext.getDomainDataModel();
        modalDialog = viewContext.getModalDialog();
    }

    @FXML
    public void hideDialog() {
        modalDialog.hide();
    }

    @FXML
    public void cancelChangeAdminPassword() {
        logger.info("Cancel Change Admin Password");
        confirmNewAdminPasswordField.setText("");
        currentAdminPasswordField.setText("");
        newAdminPasswordField.setText("");
    }

    @FXML
    public void applyChangeAdminPassword() {
        logger.info("Apply Change Admin Password");
        
        if (!domainDataModel.getDomainPassword().equals(currentAdminPasswordField.getText())) {
            Dialogs.create().lightweight().message(resources.getString("changeadminpassword.dialog.message.oldPasswordIncorrect")).showError();
        } else if (!newAdminPasswordField.getText().equals(confirmNewAdminPasswordField.getText())) {
            Dialogs.create().lightweight().message(resources.getString("changeadminpassword.dialog.message.newPasswordsDontMatch")).showError();
        } else {
            domainDataModel.getServer().changeAdminPassword(currentAdminPasswordField.getText(), newAdminPasswordField.getText());
            domainDataModel.setDomainPassword(newAdminPasswordField.getText());
            Dialogs.create().lightweight().message(resources.getString("changeadminpassword.dialog.message.adminPasswordChanged")).showInformation();
            hideDialog();
        }
    }
}
