/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.server;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.inject.Inject;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Jens Deters
 */
public class ServerController {

    @Inject
    private ViewContext viewContext;
    private UIConfiguration uiConfiguration;
    private DomainDataModel domainDataModel;
    @FXML
    private ImageView serverLogoImageView;
    @FXML
    private Label serverVersionLabel;
    @FXML
    private Label serverJdkVersionLabel;
    @FXML
    private Label javaVersionLabel;

    @FXML
    void initialize() {
        uiConfiguration = viewContext.getUiConfiguration();
        domainDataModel = viewContext.getDomainDataModel();
        domainDataModel.serverTypeProperty().addListener((ObservableValue<? extends ServerType> ov, ServerType t, ServerType t1) -> {
            setServerLogo();
        });
        setServerLogo();
        serverVersionLabel.textProperty().bind(domainDataModel.serverVersionProperty());
        serverJdkVersionLabel.visibleProperty().bind(domainDataModel.domainRunningProperty());
        serverJdkVersionLabel.textProperty().bind(domainDataModel.serverJdkVersionProperty());
        javaVersionLabel.visibleProperty().bind(serverJdkVersionLabel.visibleProperty());
    }

    private void setServerLogo() {
        if (domainDataModel.getServerType() != null) {
            switch (domainDataModel.getServerType()) {
                case GLASSFISH:
                    serverLogoImageView.setImage(new Image(uiConfiguration.getGlassfishLogoName()));
                    break;
                case WILDFLY_STANDALONE:
                    serverLogoImageView.setImage(new Image(uiConfiguration.getWildflyLogoName()));
                    break;
                case JBOSS_STANDALONE:
                    serverLogoImageView.setImage(new Image(uiConfiguration.getJBossLogoName()));
                    break;
                default:
                    serverLogoImageView.setImage(null);
            }
        }
    }
}
