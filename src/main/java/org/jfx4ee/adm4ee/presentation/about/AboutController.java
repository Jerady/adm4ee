/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.about;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.jfx4ee.adm4ee.ApplicationInfo;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Lummerland
 */
public class AboutController {

    @Inject
    private ViewContext viewContext;
    private ModalDialog modalDialog;
    @FXML
    private Label appNameLabel;
    @FXML
    private Label appVersionLabel;
    @FXML
    private Label copyrightLabel;
    @FXML
    private ImageView logoImageView;
    @FXML
    private TextArea licenseTextArea;
    @FXML
    private TextArea releaseNotesTextArea;
    @FXML
    private TextArea thirdPartyLicensesTextArea;

    @FXML
    void initialize() {        
        modalDialog = viewContext.getModalDialog();
        appNameLabel.setText(ApplicationInfo.APPLICATION_NAME);
        appVersionLabel.setText(ApplicationInfo.APPLICATION_VERSION);
        copyrightLabel.setText(ApplicationInfo.APPLICATION_COPYRIGHT);
        logoImageView.setImage(new Image(viewContext.getUiConfiguration().getAppLogoName()));

        loadReleaseNotesToTestArea();
        loadLicenseFileToTestArea();
        loadThirdPartyLicensesFileToTestArea();

    }

    @FXML
    public void hideAction() {
        modalDialog.hide();
    }

    private void loadReleaseNotesToTestArea() {
        try {
            InputStream in
                    = getClass().getResourceAsStream(viewContext.getUiConfiguration().getReleaseNotesFileName());
            releaseNotesTextArea.setText(IOUtils.toString(in));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AboutController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AboutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadLicenseFileToTestArea() {
        try {
            InputStream in
                    = getClass().getResourceAsStream(viewContext.getUiConfiguration().getLicenseFileName());
            licenseTextArea.setText(IOUtils.toString(in));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AboutController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AboutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadThirdPartyLicensesFileToTestArea() {
        try {
            InputStream in
                    = getClass().getResourceAsStream(viewContext.getUiConfiguration().getThirdPartyLicenseFileName());
            thirdPartyLicensesTextArea.setText(IOUtils.toString(in));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AboutController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AboutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
