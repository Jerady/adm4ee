/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.configuration;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import java.io.File;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javax.inject.Inject;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Jens Deters
 */
public class ConfigurationController {

    private static final Logger logger = Logger.getLogger(ConfigurationController.class.getName());
    @Inject
    private ViewContext viewContext;
    private UIConfiguration uiConfiguration;
    private DomainDataModel domainDataModel;
    private ModalDialog modalDialog;
    @FXML
    private ResourceBundle resources;
    @FXML
    private ListView<Domain> domainsListView;
    @FXML
    private TextField domainNameField;
    @FXML
    private PasswordField domainPasswordField;
    @FXML
    private TextField domainUserField;
    @FXML
    private TextField serverRootPathField;
    @FXML
    private CheckBox defaultDomainCheckBox;
    @FXML
    private CheckBox startUpDefaultDomainCheckBox;
    @FXML
    private CheckBox useSslCheckBox;
    @FXML
    private Button addDomainButton;
    @FXML
    private Button deleteDomainButton;
    @FXML
    private Button loadDomainButton;
    @FXML
    private Button cancelChangesButton;
    @FXML
    private Button chooseServerRootPathButton;
    @FXML
    private Button applyChangesButton;
    @FXML
    private ChoiceBox<ServerType> serverTypeChoiceBox;
    private ObjectProperty<Domain> selectedDomainProperty;
    private BooleanProperty dirtyProperty;

    @FXML
    void initialize() {
        uiConfiguration = viewContext.getUiConfiguration();
        domainDataModel = viewContext.getDomainDataModel();
        modalDialog = viewContext.getModalDialog();
        domainsListView.setItems(domainDataModel.getConfiguredDomainsList());
        AwesomeDude.setIcon(addDomainButton, AwesomeIcon.PLUS);
        AwesomeDude.setIcon(deleteDomainButton, AwesomeIcon.MINUS);
        serverTypeChoiceBox.setItems(FXCollections.observableArrayList(ServerType.values()));
        refreshDomainListViewCellFactory();
        bindProperties();
        attachListeners();
    }

    private void bindProperties() {
        applyChangesButton.disableProperty()
                .bind(dirtyProperty().not());
        cancelChangesButton.disableProperty()
                .bind(dirtyProperty().not());

        loadDomainButton.disableProperty()
                .bind(selectedDomainProperty().isNull());
        deleteDomainButton.disableProperty()
                .bind(selectedDomainProperty().isNull());
        startUpDefaultDomainCheckBox.selectedProperty()
                .bindBidirectional(uiConfiguration.startupWithDefaultDomainProperty());

        // disable input controls if no domain config is selected
        serverRootPathField.disableProperty()
                .bind(selectedDomainProperty().isNull());
        serverTypeChoiceBox.disableProperty()
                .bind(selectedDomainProperty().isNull());
        chooseServerRootPathButton.disableProperty()
                .bind(selectedDomainProperty().isNull());
        domainNameField.disableProperty()
                .bind(selectedDomainProperty().isNull());
        domainUserField.disableProperty()
                .bind(selectedDomainProperty().isNull());
        domainPasswordField.disableProperty()
                .bind(selectedDomainProperty().isNull());
        useSslCheckBox.disableProperty()
                .bind(selectedDomainProperty().isNull());
        defaultDomainCheckBox.disableProperty()
                .bind(selectedDomainProperty().isNull());

    }

    private void attachListeners() {
        domainsListView.setOnMouseClicked(
                (MouseEvent t) -> {
                    setSelectedDomain((Domain) domainsListView.getSelectionModel().
                            getSelectedItems().
                            get(0));
        });

        serverRootPathField.textProperty()
                .addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
                    if (!isDirty()) {
                        setDirty(!newValue.equals(getSelectedDomain().getServerRootPath()));
                    }
        });
        domainNameField.textProperty()
                .addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
                    if (!isDirty()) {
                        setDirty(!newValue.equals(getSelectedDomain().getDomainName()));
                    }
        });
        domainUserField.textProperty()
                .addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
                    if (!isDirty()) {
                        setDirty(!newValue.equals(getSelectedDomain().getDomainUser()));
                    }
        });
        domainPasswordField.textProperty()
                .addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
                    if (!isDirty()) {
                        setDirty(!newValue.equals(getSelectedDomain().getDomainPassword()));
                    }
        });
        useSslCheckBox.selectedProperty()
                .addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                    if (!isDirty()) {
                        setDirty(!newValue.equals(getSelectedDomain().isSsl()));
                    }
        });
        defaultDomainCheckBox.selectedProperty()
                .addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                    if (!isDirty()) {
                        setDirty(!newValue.equals(getSelectedDomain().isDefaultDomain()));
                    }
        });
        serverTypeChoiceBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ServerType> ov, ServerType oldValue, ServerType newValue) -> {
            if (!isDirty()) {
                setDirty(!newValue.equals(getSelectedDomain().getServerType()));
            }
        });
    }

    public BooleanProperty dirtyProperty() {
        if (dirtyProperty == null) {
            dirtyProperty = new SimpleBooleanProperty(Boolean.FALSE);
        }
        return dirtyProperty;
    }

    public void setDirty(Boolean dirty) {
        dirtyProperty().set(dirty);
    }

    public Boolean isDirty() {
        return dirtyProperty().get();
    }

    public void setSelectedDomain(Domain domainToEdit) {
        selectedDomainProperty().set(domainToEdit);
        domainsListView.getSelectionModel().select(domainToEdit);
        syncViewWithDomain();
    }

    public Domain getSelectedDomain() {
        return selectedDomainProperty().get();
    }

    public ObjectProperty<Domain> selectedDomainProperty() {
        if (selectedDomainProperty == null) {
            selectedDomainProperty = new SimpleObjectProperty<>();
        }
        return selectedDomainProperty;
    }

    private void selectFirstItem() {
        if (!domainsListView.getItems().
                isEmpty()) {
            Domain d = domainsListView.getItems().get(0);
            domainsListView.getSelectionModel().
                    select(d);
            setSelectedDomain(d);
        }
    }

    private void selectLastItem() {
        if (!domainsListView.getItems().
                isEmpty()) {
            Domain d = domainsListView.getItems().get(domainsListView.getItems().size() - 1);
            domainsListView.getSelectionModel().
                    select(d);
            setSelectedDomain(d);
        }
    }

    private void syncViewWithDomain() {
        if (getSelectedDomain() != null) {
            serverRootPathField.setText(getSelectedDomain().getServerRootPath());
            if (getSelectedDomain().getServerType() != null) {
                serverTypeChoiceBox.getSelectionModel().select(getSelectedDomain().getServerType());
            }
            domainNameField.setText(getSelectedDomain().getDomainName());
            domainUserField.setText(getSelectedDomain().getDomainUser());
            domainPasswordField.setText(getSelectedDomain().getDomainPassword());
            useSslCheckBox.setSelected(getSelectedDomain().isSsl());
            defaultDomainCheckBox.setSelected(getSelectedDomain().isDefaultDomain());
        }
    }

    private void syncDomainWithView() {
        if (getSelectedDomain() != null) {
            getSelectedDomain().setServerRootPath(serverRootPathField.getText());
            getSelectedDomain().setServerType(serverTypeChoiceBox.getSelectionModel().getSelectedItem());
            getSelectedDomain().setDomainName(domainNameField.getText());
            getSelectedDomain().setDomainUser(domainUserField.getText());
            getSelectedDomain().setDomainPassword(domainPasswordField.getText());
            getSelectedDomain().setSsl(useSslCheckBox.isSelected());
//            getSelectedDomain().setDefaultDomain(defaultDomainCheckBox.isSelected());
            handleDefaultDomainState();
        }
    }

    @FXML
    public void addDomain() {
        logger.log(Level.INFO, "addDomain");
        Domain domain = new Domain();
        domain.setDomainName("New Domain" + (domainsListView.getItems().size() + 1));
        domain.setDomainUser("");
        domain.setDomainPassword("");
        domain.setServerRootPath("");
        domain.setServerType(ServerType.GLASSFISH);
        domainDataModel.getConfiguredDomainsList().add(domain);
        refreshDomainListViewCellFactory();
        ConfigurationManager.getInstance().getDomains().add(domain);

        setDirty(Boolean.TRUE);
        setSelectedDomain(domain);
        domainNameField.requestFocus();
        domainNameField.selectAll();
    }

    @FXML
    public void deleteDomain() {
        logger.log(Level.INFO, "deleteDomain");
        Action response = Dialogs.create()
                .owner(viewContext.getPrimaryStage())
                .message(resources.getString("configuration.dialog.message.deleteDomain"))
                .nativeTitleBar()
                .actions(new Action[]{Dialog.Actions.YES, Dialog.Actions.NO})
                .showConfirm();

        if (response == Dialog.Actions.YES) {
            domainDataModel.getConfiguredDomainsList().remove(getSelectedDomain());
            ConfigurationManager.getInstance().getDomains().remove(getSelectedDomain());
            ConfigurationManager.getInstance().saveConfiguration();
            refreshDomainListViewCellFactory();
            setDirty(Boolean.FALSE);
            selectLastItem();
        }
    }

    private void refreshDomainListViewCellFactory() {
        domainsListView.setCellFactory((ListView<Domain> list) -> new DomainCell());
    }

    @FXML
    public void loadDomain() {
        logger.log(Level.INFO, "loadDomain");
        if (dirtyProperty().get()) {
            applyChanges();
        }
        domainDataModel.setDomain(getSelectedDomain());
        hideAction();
    }

    @FXML
    public void applyChanges() {
        logger.log(Level.INFO, "applyChanges");
        setDirty(Boolean.FALSE);
        syncDomainWithView();
        ConfigurationManager.getInstance().saveConfiguration();
        refreshDomainListViewCellFactory();
    }

    @FXML
    public void revertChanges() {
        logger.log(Level.INFO, "revertChanges");
        setDirty(Boolean.FALSE);
        setSelectedDomain(getSelectedDomain());
    }

    @FXML
    public void hideAction() {
        modalDialog.hide();
    }

    @FXML
    public void chooseServerRootPathAction() {
        logger.log(Level.INFO, "chooseServerRootPathAction");
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(resources.
                getString("configuration.server.rootpath.chooser.title"));
        directoryChooser.setInitialDirectory(new File(uiConfiguration.getLastChoosenDirectory()));
        File directory = directoryChooser.showDialog(viewContext.getPrimaryStage());

        if (directory != null) {
            serverRootPathField.setText(directory.getAbsolutePath());
        }
    }

    public void handleDefaultDomainState() {
        domainDataModel.getConfiguredDomainsList().stream().forEach((domain) -> {
            domain.setDefaultDomain(false);
        });

        if (defaultDomainCheckBox.isSelected()) {
            getSelectedDomain().setDefaultDomain(true);
            domainDataModel.setDefaultDomain(domainDataModel.getDomain());
        } else {
            domainDataModel.setDefaultDomain(domainDataModel.findDefaultDomain());
        }

        refreshDomainListViewCellFactory();

    }

    public class DomainCell extends ListCell<Domain> {

        @Override
        public void updateItem(Domain domain, boolean empty) {
            super.updateItem(domain, empty);
            if (domain != null && !empty) {
                setText(domain.getDomainName());
                if (domain.isDefaultDomain()) {
                    setText(domain.getDomainName().concat(" ").concat(viewContext.getResourceBundle().getString("configuration.label.domainDefault")));
                    getStyleClass().addAll("default-domain");
                }
            }
        }
    }
}
