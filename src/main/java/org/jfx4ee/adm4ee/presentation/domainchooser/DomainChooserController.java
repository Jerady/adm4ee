/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.domainchooser;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javax.inject.Inject;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.presentation.configuration.Configuration;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Jens Deters
 */
public class DomainChooserController {

    @Inject
    private ViewContext viewContext;
    private ModalDialog modalDialog;
    private UIConfiguration uiConfiguration;
    private DomainDataModel domainDataModel;
    private LoadDomainService loadDomainService;
    @Inject
    private Configuration configuration;
    @FXML
    private ListView domainsListView;
    @FXML
    private Button loadDomainButton;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private CheckBox defaultDomainCheckBox;
    @FXML
    private CheckBox startUpDefaultDomainCheckBox;
    private ObjectProperty<Domain> selectedDomainProperty;

    @FXML
    void initialize() {
        uiConfiguration = viewContext.getUiConfiguration();
        domainDataModel = viewContext.getDomainDataModel();
        modalDialog = viewContext.getModalDialog();
        loadDomainService = new LoadDomainService();

        progressIndicator.visibleProperty().bind(loadDomainService.runningProperty());
        domainsListView.setItems(domainDataModel.getConfiguredDomainsList());
        domainsListView.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Domain>() {
                    @Override
                    public void changed(ObservableValue<? extends Domain> ov,
                            Domain oldValue, Domain newValue) {
                        setSelectedDomain(newValue);
                    }
                });

        domainsListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (t.getClickCount() == 2) {
                    loadSelectedDomain();
                }
            }
        });
        refreshDomainListViewCellFactory();

        loadDomainButton.disableProperty().bind(selectedDomainProperty().isNull());
        defaultDomainCheckBox.disableProperty().bind(domainsListView.getSelectionModel().selectedItemProperty().isNull());
        startUpDefaultDomainCheckBox.disableProperty().bind(defaultDomainCheckBox.selectedProperty().not());
        startUpDefaultDomainCheckBox.selectedProperty().bindBidirectional(uiConfiguration.startupWithDefaultDomainProperty());
    }

    private void refreshDomainListViewCellFactory() {
        domainsListView.setCellFactory(new Callback<ListView<Domain>, ListCell<Domain>>() {
            @Override
            public ListCell<Domain> call(ListView<Domain> list) {
                return new DomainCell();
            }
        });
    }

    @FXML
    public void setSelectedDomainAsDefault() {
        for (Domain domain : domainDataModel.getConfiguredDomainsList()) {
            domain.setDefaultDomain(false);
        }

        if (defaultDomainCheckBox.isSelected()) {
            Domain d = (Domain) domainsListView.getSelectionModel().getSelectedItem();
            d.setDefaultDomain(true);
            domainDataModel.setDefaultDomain(d);
        } else {
            domainDataModel.setDefaultDomain(domainDataModel.findDefaultDomain());
        }
        refreshDomainListViewCellFactory();
        ConfigurationManager.getInstance().saveConfiguration();
    }

    @FXML
    public void showConfigurationViewAction() {
        viewContext.getModalDialog().show(configuration.getView());
    }

    @FXML
    public void loadSelectedDomain() {
        ConfigurationManager.getInstance().saveConfiguration();
        loadDomainService.restart();
    }

    public void setSelectedDomain(Domain domainToEdit) {
        selectedDomainProperty().set(domainToEdit);
        domainsListView.getSelectionModel().select(domainToEdit);
        syncViewWithDomain();
    }

    public Domain getSelectedDomain() {
        return selectedDomainProperty().get();
    }

    private ObjectProperty<Domain> selectedDomainProperty() {
        if (selectedDomainProperty == null) {
            selectedDomainProperty = new SimpleObjectProperty<>();
        }
        return selectedDomainProperty;
    }

    private void syncViewWithDomain() {
        if (getSelectedDomain() != null) {
            defaultDomainCheckBox.setSelected(getSelectedDomain().isDefaultDomain());
        }
    }

    private class LoadDomainService extends Service<Void> {

        @Override
        protected void succeeded() {
            super.succeeded(); //To change body of generated methods, choose Tools | Templates.
            modalDialog.hide();
        }

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            domainDataModel.setDomain(selectedDomainProperty().get());
                        }
                    });
                    return null;
                }
            };
        }
    }

    public class DomainCell extends ListCell<Domain> {

        @Override
        public void updateItem(Domain domain, boolean empty) {
            super.updateItem(domain, empty);
            if (domain != null && !empty) {
                setText(domain.getDomainName());
                if (domain.isDefaultDomain()) {
                    setText(domain.getDomainName().concat(" ").concat(viewContext.getResourceBundle().getString("configuration.label.domainDefault")));
                    getStyleClass().addAll("default-domain");
                }
            }
        }
    }
}
