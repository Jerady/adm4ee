/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.cdi;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

/**
 *
 * @author Jens Deters
 */
public class CdiFxmlLoader {

    @Inject
    private Instance<Object> instance;
    private final FXMLLoader fxmlLoader = new FXMLLoader();

    @PostConstruct
    void createFxmlLoader() {
        fxmlLoader.setControllerFactory(
                (Class<?> classType) -> classType == null ? null : instance.select(classType).get());
    }

    public void load(URL location) throws IOException, URISyntaxException {
        fxmlLoader.setLocation(location);
        fxmlLoader.load();
    }

    public void load(URL location, ResourceBundle resources) throws IOException {
        fxmlLoader.setLocation(location);
        fxmlLoader.setResources(resources);
        fxmlLoader.load();
    }

    public Parent getRoot() {
        return (Parent) fxmlLoader.getRoot();
    }
}
