/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.cdi;

import java.io.IOException;
import java.net.URL;
import javafx.scene.Parent;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Jens Deters
 *
 */
public abstract class FXMLView {

    @Inject
    private CdiFxmlLoader loader;
    @Inject
    private ViewContext viewContext;

    @PostConstruct
    public void init() {
        final URL resource = getClass().getResource(getFXMLName());
        try {
            loader.load(resource, viewContext.getResourceBundle());
        } catch (IOException ex) {
            throw new IllegalStateException("Cannot load " + getFXMLName(), ex);
        }
    }

    public Parent getView() {
        Parent parent = loader.getRoot();
        return parent;
    }

    final String getFXMLName() {
        String clazz = getClass().getSimpleName().toLowerCase();
        return clazz + ".fxml";
    }
}
