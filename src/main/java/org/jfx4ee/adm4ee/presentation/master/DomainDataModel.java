/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.master;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.server.boundary.Server;
import org.jfx4ee.adm4ee.business.server.boundary.ServerHandler;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;
import org.jfx4ee.adm4ee.business.util.CommandResult;

/**
 *
 * @author Jens Deters
 */
public class DomainDataModel {

    private static final Logger logger = Logger.getLogger(DomainDataModel.class.getName());
    private StringProperty serverRootPathProperty;
    private ObjectProperty<ServerType> serverTypeProperty;
    private StringProperty serverVersionProperty;
    private StringProperty serverJdkVersionProperty;
    private BooleanProperty useSSLProperty;
    private StringProperty domainNameProperty;
    private StringProperty domainUserProperty;
    private StringProperty domainPasswordProperty;
    private StringProperty domainUptimeProperty;
    private BooleanProperty domainRunningProperty;
    private StringProperty domainStatusMessageProperty;
    private ObjectProperty<Server> serverProperty;
    private ObjectProperty<Domain> domainProperty;
    private ObjectProperty<Domain> defaultDomainProperty;
    private StringProperty serverLogProperty;
    private BooleanProperty autoUpdateProperty;
    private Timeline updateTimer;
    private IntegerProperty updateTimerIntervalProperty;
    private BooleanProperty modelLoadedProperty;
    private IntegerProperty logFileCharacterCountProperty;
    private ObservableList<Domain> configuredDomainsList;
    private ObservableList<Application> applicationsList;
    private ObservableList<String> jdbcConnectionsPoolsList;
    private ObservableMap<String, Integer> servicePortsMap;
    private IntegerProperty portBaseProperty;

    public DomainDataModel() {
        init();
    }

    private void init() {
        clear();
        addDomainPropertyListeners();
    }

    public final void clear() {
        setModelLoaded(Boolean.FALSE);
        setAutoUpdate(true);
        stopUpdateTimer();

        setDomain(null);
        setServer(null);
        setServerRootPath("");
        setServerType(null);
        setServerVersion("");
        setServerJdkVersion("");
        setServerLog("");
        setDomainName("");
        setDomainUser("");
        setDomainPassword("");
        setDomainRunning(Boolean.FALSE);
        setDomainUptime("");
        setUseSSL(Boolean.FALSE);

        getApplicationsList().clear();
        getServicePortsMap().clear();
        getJdbcConnectionsPoolsList().clear();
    }

    private void addDomainPropertyListeners() {

        /*
         * make sure all values are passed through to Configuration:
         */
        serverRootPathProperty().addListener(
                (ObservableValue<? extends String> ov, String t, String t1) -> {
                    domainProperty().get().setServerRootPath(t1);
                });

        domainNameProperty().addListener(
                (ObservableValue<? extends String> ov, String t, String t1) -> {
                    domainProperty().get().setDomainName(t1);
                });
        domainUserProperty().addListener(
                (ObservableValue<? extends String> ov, String t, String t1) -> {
                    domainProperty().get().setDomainUser(t1);
                });
        domainPasswordProperty().addListener(
                (ObservableValue<? extends String> ov, String t, String t1) -> {
                    domainProperty().get().setDomainPassword(t1);
                });
    }

    private void load() {
        try {

            if (getDomain() != null) {

                // Need a configured domain name here!
                // At least a server type must be set!
                ServerHandler serverHandler = ServerHandler.getInstance(getDomain());
                Server server = serverHandler.getServer();

                //TODO: error message when server null
                setServer(server);

                // For local initialization (includes remote) required values are:
                // serverRootPath, domainName, domainUser, domainPassword
                if (getDomain().hasLocalConfiguration()) {
                    try {
                        getServer().initLocalServer();
                    } catch (Exception ex) {
                        // There seems to be an error in the config, so show the config dialog here
                        logger.log(Level.SEVERE, "Could not initialize local server: {0}", ex);
                        Dialogs.create().showException(ex);
                    }
                    getServer().initLocalDomain();
                } else if (getDomain().hasRemoteConfiguration()) {
                    // Only URL, domainUser, domainPassword are required
                    getServer().initRemoteDomain();
                }

                setServerRootPath(getDomain().getServerRootPath());
                setServerType(getDomain().getServerType());
                setDomainName(getDomain().getDomainName());
                setDomainUser(getDomain().getDomainUser());
                setDomainPassword(getDomain().getDomainPassword());
                setServerVersion(getServer().getVersionString());
                setServerJdkVersion(getServer().getJdkVersionString());

                //loadServerLog();
                // if we got so far model is loaded
                setModelLoaded(Boolean.TRUE);

                refreshDomainState();
                refreshApplicationsList();

                if (isAutoUpdateEnabled()) {
                    startUpdateTimer();
                }
            }
        } catch (Exception e) {
            Dialogs.create().showExceptionInNewWindow(e);
        }

    }

    //TODO: eventually extract this method from data model: 
    public void refreshDomainState() {
        Platform.runLater(() -> {
            logger.log(Level.INFO, "refreshDomainState()");
            logger.log(Level.INFO, "pinging ManagementResource");
            CommandResult result = getServer().pingManagementResource();
            if (result.getExitCode() > 0) {
                String errorMessage = result.getExitCodeInfo() + "(" + result.getExitCode()
                        + ")";
                logger.log(Level.SEVERE, errorMessage);
                Dialogs.create().title("An error occured!").message(errorMessage).
                        showError();
            }
            setDomainRunning(result.isOk());
            refreshUptimeInformation();
        });
    }

    public void refreshUptimeInformation() {
        logger.log(Level.INFO, "refreshing: Uptime imformation");
        setDomainUptime("");
        if (isDomainRunning()) {
            setDomainStatusMessage("Domain is UP");
            setDomainUptime(getUptimeString(getServer().getUptime()));
        } else {
            setDomainStatusMessage("Domain is DOWN");
        }

    }

    public void refreshApplicationsList() {
        logger.log(Level.INFO, "refreshing: ApplicationsLiat");
        getApplicationsList().clear();
        setApplications(getServer().getApplications());
    }

    public void refreshServicePortsMap() {
        logger.log(Level.INFO, "refreshing: ServicePortsMap");
        getServicePortsMap().clear();
        setServicePorts(getServer().getServicePorts());
    }

    public void refreshJdbcConnectionPoolsList() {
        logger.log(Level.INFO, "refreshing: JdbcConnectionPoolsList");
        getJdbcConnectionsPoolsList().clear();
        setJdbcConnectionsPools(getServer().getJdbcConnectionPools());
    }

    public void initWithDefaultDomain() {
        setDomain(findDefaultDomain());
    }

    public Domain findDefaultDomain() {
        for (Domain domain : ConfigurationManager.getInstance().getDomains()) {
            if (domain.isDefaultDomain()) {
                return domain;
            }
        }
        return null;
    }

    public void disableAutoUpdate() {
        stopUpdateTimer();
        setAutoUpdate(false);
    }

    public void enableAutoupdate() {
        setAutoUpdate(true);
        if (isAutoUpdateEnabled()) {
            startUpdateTimer();
        }
    }

    public ObservableList<String> getJdbcConnectionsPoolsList() {
        if (jdbcConnectionsPoolsList == null) {
            jdbcConnectionsPoolsList = FXCollections.observableArrayList();
        }
        return jdbcConnectionsPoolsList;
    }

    public void setJdbcConnectionsPools(Collection<String> jdbcConnectionsPools) {
        jdbcConnectionsPoolsList.setAll(jdbcConnectionsPools);
    }

    public ObservableList<Domain> getConfiguredDomainsList() {
        if (configuredDomainsList == null) {
            configuredDomainsList = FXCollections.observableArrayList();
        }
        return configuredDomainsList;
    }

    public void setConfiguredDomains(List<Domain> domains) {
        getConfiguredDomainsList().setAll(domains);
    }

    public ObservableList<Application> getApplicationsList() {
        if (applicationsList == null) {
            applicationsList = FXCollections.observableArrayList();
        }
        return applicationsList;
    }

    public void setApplications(Collection<Application> applications) {
        getApplicationsList().setAll(applications);
    }

    public ObservableMap<String, Integer> getServicePortsMap() {
        if (servicePortsMap == null) {
            servicePortsMap = FXCollections.observableHashMap();
        }
        return servicePortsMap;
    }

    public void setServicePorts(Map<String, Integer> servicePorts) {
        servicePorts.entrySet().stream().forEach((entry) -> {
            String string = entry.getKey();
            Integer integer = entry.getValue();
            getServicePortsMap().put(string, integer);
        });
    }

    public void startUpdateTimer() {
        stopUpdateTimer();
        updateTimer = new Timeline(new KeyFrame(Duration.seconds(getUpdateTimerInterval()), (ActionEvent event) -> {
            refreshDomainState();
        }));
        updateTimer.setCycleCount(Timeline.INDEFINITE);
        updateTimer.play();
    }

    public void restartUpdateTimer() {
        stopUpdateTimer();
        startUpdateTimer();
    }

    public void stopUpdateTimer() {
        if (updateTimer != null) {
            updateTimer.stop();
        }
    }

    public void loadServerLog() {
        setServerLog("");
        if (getServer() != null) {
            CommandResult result = getServer().getLogFileContent(getLogFileCharacterCount());
            setServerLog(result.getResult());
        }
    }

    /*
     * Properties access methods
     */
    public ObjectProperty<Server> serverProperty() {
        if (serverProperty == null) {
            serverProperty = new SimpleObjectProperty<>();
        }
        return serverProperty;
    }

    public Server getServer() {
        return serverProperty().getValue();
    }

    public void setServer(Server server) {
        serverProperty().setValue(server);
    }

    public ObjectProperty<Domain> domainProperty() {
        if (domainProperty == null) {
            domainProperty = new SimpleObjectProperty<>();
        }
        return domainProperty;
    }

    public Domain getDomain() {
        return domainProperty().getValue();
    }

    public void setDomain(Domain domain) {
        domainProperty().setValue(domain);
        load();
    }

    public StringProperty serverRootPathProperty() {
        if (serverRootPathProperty == null) {
            serverRootPathProperty = new SimpleStringProperty();
        }
        return serverRootPathProperty;
    }

    public String getServerRootPath() {
        return serverRootPathProperty().getValue();
    }

    public void setServerRootPath(String serverRootPath) {
        logger.log(Level.INFO, "serverRootPathProperty set: {0}", serverRootPath);
        serverRootPathProperty().setValue(serverRootPath);
    }

    public ObjectProperty<ServerType> serverTypeProperty() {
        if (serverTypeProperty == null) {
            serverTypeProperty = new SimpleObjectProperty<>();
        }
        return serverTypeProperty;
    }

    public ServerType getServerType() {
        return serverTypeProperty().getValue();
    }

    public void setServerType(ServerType serverType) {
        logger.log(Level.INFO, "serverRootPathProperty set: {0}", serverType);
        serverTypeProperty().setValue(serverType);
    }

    public StringProperty domainNameProperty() {
        if (domainNameProperty == null) {
            domainNameProperty = new SimpleStringProperty();
        }
        return domainNameProperty;
    }

    public String getDomainName() {
        return domainNameProperty().get();
    }

    public void setDomainName(String domainName) {
        logger.log(Level.INFO, "domainNameProperty set: {0}", domainName);
        domainNameProperty().setValue(domainName);
    }

    public StringProperty domainUserProperty() {
        if (domainUserProperty == null) {
            domainUserProperty = new SimpleStringProperty();
        }
        return domainUserProperty;
    }

    public String getDomainUser() {
        return domainUserProperty().getValue();
    }

    public void setDomainUser(String domainUser) {
        logger.log(Level.INFO, "domainUserProperty set: {0}", domainUser);
        domainUserProperty().setValue(domainUser);
    }

    public StringProperty domainPasswordProperty() {
        if (domainPasswordProperty == null) {
            domainPasswordProperty = new SimpleStringProperty();
        }
        return domainPasswordProperty;
    }

    public String getDomainPassword() {
        return domainPasswordProperty().getValue();
    }

    public void setDomainPassword(String domainPassword) {
        logger.log(Level.INFO, "domainPasswordProperty set: {0}", domainPassword);
        domainPasswordProperty().setValue(domainPassword);
    }

    public StringProperty serverVersionProperty() {
        if (serverVersionProperty == null) {
            serverVersionProperty = new SimpleStringProperty();
        }
        return serverVersionProperty;
    }

    public String getServerVersion() {
        return serverVersionProperty().getValue();
    }

    public void setServerVersion(String serverVersion) {
        logger.log(Level.INFO, "serverVersionProperty set: {0}", serverVersion);
        serverVersionProperty().setValue(serverVersion);
    }

    public StringProperty serverJdkVersionProperty() {
        if (serverJdkVersionProperty == null) {
            serverJdkVersionProperty = new SimpleStringProperty();
        }
        return serverJdkVersionProperty;
    }

    public String getServerJdkVersion() {
        return serverJdkVersionProperty().getValue();
    }

    public void setServerJdkVersion(String serverJdkVersion) {
        logger.log(Level.INFO, "serverJdkVersionProperty set: {0}", serverJdkVersion);
        serverJdkVersionProperty().setValue(serverJdkVersion);
    }

    public BooleanProperty useSSLProperty() {
        if (useSSLProperty == null) {
            useSSLProperty = new SimpleBooleanProperty();
        }
        return useSSLProperty;
    }

    public boolean isUseSSL() {
        return useSSLProperty().get();
    }

    public Boolean getUseSSL() {
        return useSSLProperty().getValue();
    }

    public void setUseSSL(Boolean useSSL) {
        logger.log(Level.INFO, "useSSLProperty set: {0}", useSSL);
        useSSLProperty().setValue(useSSL);
    }

    public BooleanProperty domainRunningProperty() {
        if (domainRunningProperty == null) {
            domainRunningProperty = new SimpleBooleanProperty();
        }
        return domainRunningProperty;
    }

    public boolean isDomainRunning() {
        return domainRunningProperty().get();
    }

    public Boolean getDomainRunning() {
        return domainRunningProperty().getValue();
    }

    public void setDomainRunning(Boolean running) {
        logger.log(Level.INFO, "domainRunningProperty set: {0}", running);
        domainRunningProperty().setValue(running);
    }

    public StringProperty domainUptimeProperty() {
        if (domainUptimeProperty == null) {
            domainUptimeProperty = new SimpleStringProperty();
        }
        return domainUptimeProperty;
    }

    public String getDomainUptime() {
        return domainUptimeProperty().getValue();
    }

    public void setDomainUptime(String uptime) {
        logger.log(Level.INFO, "domainUptimeProperty set: {0}", uptime);
        domainUptimeProperty().setValue(uptime);
    }

    public StringProperty serverLogProperty() {
        if (serverLogProperty == null) {
            serverLogProperty = new SimpleStringProperty();
        }
        return serverLogProperty;
    }

    public String getServerLog() {
        return serverLogProperty().getValue();
    }

    public void setServerLog(String log) {
        logger.log(Level.INFO, "serverLogProperty set");
        serverLogProperty().setValue(log);
    }

    public boolean isAutoUpdateEnabled() {
        return autoUpdateProperty().get();
    }

    public BooleanProperty autoUpdateProperty() {
        if (autoUpdateProperty == null) {
            autoUpdateProperty = new SimpleBooleanProperty();
        }
        return autoUpdateProperty;
    }

    public Boolean getAutoUpdate() {
        return autoUpdateProperty().getValue();
    }

    public final void setAutoUpdate(boolean autoUpdate) {
        logger.log(Level.INFO, "autoUpdateProperty set: {0}", autoUpdate);
        autoUpdateProperty().setValue(autoUpdate);
    }

    public IntegerProperty updateTimerIntervalProperty() {
        if (updateTimerIntervalProperty == null) {
            updateTimerIntervalProperty = new SimpleIntegerProperty();
        }
        return updateTimerIntervalProperty;
    }

    public Integer getUpdateTimerInterval() {
        return updateTimerIntervalProperty().getValue();
    }

    public final void setUpdateTimerInterval(int updateTimerInterval) {
        logger.log(Level.INFO, "updateTimerIntervalProperty set: {0}", updateTimerInterval);
        updateTimerIntervalProperty().setValue(updateTimerInterval);
    }

    public BooleanProperty modelLoadedProperty() {
        if (modelLoadedProperty == null) {
            modelLoadedProperty = new SimpleBooleanProperty();
        }
        return modelLoadedProperty;
    }

    public boolean isModelLoaded() {
        return modelLoadedProperty().getValue();
    }

    public Boolean getModelLoaded() {
        return modelLoadedProperty().getValue();
    }

    public final void setModelLoaded(Boolean modelLoaded) {
        logger.log(Level.INFO, "modelLoadedProperty set: {0}", modelLoaded);
        modelLoadedProperty().setValue(modelLoaded);
    }

    public IntegerProperty logFileCharacterCountProperty() {
        if (logFileCharacterCountProperty == null) {
            logFileCharacterCountProperty = new SimpleIntegerProperty();
        }
        return logFileCharacterCountProperty;
    }

    public Integer getLogFileCharacterCount() {
        return logFileCharacterCountProperty().getValue();
    }

    public final void setLogFileCharacterCount(int logFileCharacterCount) {
        logger.log(Level.INFO, "logFileCharacterCountProperty set: {0}", logFileCharacterCount);
        logFileCharacterCountProperty().setValue(logFileCharacterCount);
    }

    public ObjectProperty<Domain> defaultDomainProperty() {
        if (defaultDomainProperty == null) {
            defaultDomainProperty = new SimpleObjectProperty<>();
        }
        return defaultDomainProperty;
    }

    public Domain getDefaultDomain() {
        return defaultDomainProperty().getValue();
    }

    public void setDefaultDomain(Domain defaultDomain) {
        logger.log(Level.INFO, "defaultDomainProperty changed: {0}", defaultDomain);
        defaultDomainProperty().setValue(defaultDomain);
    }

    public boolean isDefaultDomain() {
        return getDefaultDomain() != null;
    }

    public void setPortBase(Integer portBase) {
        portBaseProperty().set(portBase);
    }

    public Integer getPortBase() {
        return portBaseProperty().get();
    }

    public IntegerProperty portBaseProperty() {
        if (portBaseProperty == null) {
            portBaseProperty = new SimpleIntegerProperty();
        }
        return portBaseProperty;
    }

    public StringProperty domainStatusMessageProperty() {
        if (domainStatusMessageProperty == null) {
            domainStatusMessageProperty = new SimpleStringProperty();
        }
        return domainStatusMessageProperty;
    }

    public void setDomainStatusMessage(String domainStatusMessage) {
        this.domainStatusMessageProperty().set(domainStatusMessage);
    }

    public String getDomainStatusMessage() {
        return domainStatusMessageProperty().get();
    }

    protected String getUptimeString(long uptime) {
        if (uptime <= 0) {
            return null;
        }
        long days = TimeUnit.MILLISECONDS.toDays(uptime);
        uptime -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(uptime);
        uptime -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(uptime);
        uptime -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(uptime);

        StringBuilder sb = new StringBuilder();
        if (days > 0) {
            sb.append(days).append(" days, ");
        }
        if (hours > 0) {
            sb.append(hours).append(" hrs ");
        }
        sb.append(minutes).append(" min ");
        sb.append(seconds).append(" sec");
        return sb.toString();
    }
}
