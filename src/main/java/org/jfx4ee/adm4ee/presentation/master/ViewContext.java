/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.master;

import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.inject.Singleton;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;

/**
 *
 * @author Jens Deters
 */
@Singleton
public class ViewContext {

    private DomainDataModel domainDataModel;
    private UIConfiguration uiConfiguration;
    private ResourceBundle resourceBundle;
    private Stage primaryStage;
    private ModalDialog modalDialog;

    public ViewContext() {
        init();
    }

    private void init() {
        modalDialog = new ModalDialog();
        resourceBundle = ResourceBundle.
                getBundle("i18n/messages");
        uiConfiguration = ConfigurationManager.getInstance().getUIConfiguration();
        domainDataModel = new DomainDataModel();
        domainDataModel.setConfiguredDomains(ConfigurationManager.getInstance().getDomains());
        domainDataModel.setAutoUpdate(uiConfiguration.isAutoUpdate());
        domainDataModel.setUpdateTimerInterval(uiConfiguration.getUpdateInterval());
        domainDataModel.setLogFileCharacterCount(uiConfiguration.getLogFileCharacterCount());
        domainDataModel.clear();

        // BIND: DomainDataModel -> UIConfiguration
        attachPropertyListeners();
    }

    private void attachPropertyListeners() {
        domainDataModel.autoUpdateProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            uiConfiguration.setAutoUpdate(t1);
        });
        domainDataModel.updateTimerIntervalProperty().addListener((ObservableValue<? extends Number> ov, Number t, Number t1) -> {
            uiConfiguration.setUpdateInterval(t1.intValue());
        });
        domainDataModel.logFileCharacterCountProperty().addListener((ObservableValue<? extends Number> ov, Number t, Number t1) -> {
            uiConfiguration.setLogFileCharacterCount(t1.intValue());
        });

    }

    public DomainDataModel getDomainDataModel() {
        return domainDataModel;
    }

    public UIConfiguration getUiConfiguration() {
        return uiConfiguration;
    }

    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            Platform.exit();
        });
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public ModalDialog getModalDialog() {
        return modalDialog;
    }

    public void exitApp() {

        Action response = Dialogs.create()
                .owner(getPrimaryStage())
                .message(getResourceBundle().getString("app.dialog.confirm.exitapp.message"))
                .nativeTitleBar()
                .actions(new Action[]{Dialog.Actions.YES, Dialog.Actions.NO})
                .showConfirm();

        if (response == Dialog.Actions.YES) {
            ConfigurationManager.getInstance().saveConfiguration();
            Platform.exit();
        }
    }
}
