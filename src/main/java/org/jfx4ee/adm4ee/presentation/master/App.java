/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.master;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.jfx4ee.adm4ee.ApplicationInfo;
import org.jfx4ee.adm4ee.presentation.cdi.DefaultStartupEvent;
import org.jfx4ee.adm4ee.presentation.cdi.StartupEvent;

/**
 *
 * @author Jens Deters
 */
public class App {

    @Inject
    private MasterView masterViewController;
    @Inject
    private ViewContext viewContext;

    public void startApplication(@Observes @DefaultStartupEvent StartupEvent startupEvent) {
        viewContext.setPrimaryStage(startupEvent.getPrimaryStage());
        Scene scene = new Scene(masterViewController.getView(), 900, 730, Color.WHITE);
        scene.getStylesheets().add("/styles/adm4ee.css");
        Platform.setImplicitExit(false);

        startupEvent.getPrimaryStage().setScene(scene);
        startupEvent.getPrimaryStage().setTitle(String.format("%s - %s", ApplicationInfo.APPLICATION_NAME, ApplicationInfo.APPLICATION_VERSION));
        startupEvent.getPrimaryStage().show();
        
        masterViewController.startUp();
    }

}
