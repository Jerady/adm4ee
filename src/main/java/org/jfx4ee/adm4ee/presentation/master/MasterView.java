/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.master;

import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.presentation.configuration.Configuration;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.dashboard.Dashboard;
import org.jfx4ee.adm4ee.presentation.domainchooser.DomainChooser;
import org.jfx4ee.adm4ee.presentation.header.Header;
import org.jfx4ee.adm4ee.presentation.server.Server;

/**
 *
 * @author Jens Deters
 */
public class MasterView {

    @Inject
    private ViewContext viewContext;
    private DomainDataModel domainDataModel;
    private UIConfiguration uiConfiguration;
    private ModalDialog modalDialog;
    @Inject
    private Header header;
    @Inject
    private Server server;
    @Inject
    private Dashboard dashboard;
    @Inject
    private DomainChooser domainChooser;
    @Inject
    private Configuration configuration;
    private AnchorPane mainViewPane;
    private AnchorPane rootPane;

    @PostConstruct
    public void init() {
        uiConfiguration = viewContext.getUiConfiguration();
        domainDataModel = viewContext.getDomainDataModel();
        modalDialog = viewContext.getModalDialog();

        rootPane = new AnchorPane();
        mainViewPane = new AnchorPane();

        AnchorPane.setTopAnchor(header.getView(), 0.0);
        AnchorPane.setRightAnchor(header.getView(), 0.0);
        AnchorPane.setLeftAnchor(header.getView(), 0.0);

        AnchorPane.setBottomAnchor(server.getView(), 0.0);
        AnchorPane.setRightAnchor(server.getView(), 0.0);
        AnchorPane.setLeftAnchor(server.getView(), 0.0);

        AnchorPane.setBottomAnchor(dashboard.getView(), 0.0);
        AnchorPane.setRightAnchor(dashboard.getView(), 0.0);
        AnchorPane.setLeftAnchor(dashboard.getView(), 0.0);
        AnchorPane.setTopAnchor(dashboard.getView(), 80.0);

        AnchorPane.setBottomAnchor(modalDialog, 0.0);
        AnchorPane.setRightAnchor(modalDialog, 0.0);
        AnchorPane.setLeftAnchor(modalDialog, 0.0);
        AnchorPane.setTopAnchor(modalDialog, 0.0);

        mainViewPane.getChildren().addAll(dashboard.getView(),
                server.getView(), header.getView(), modalDialog);

        AnchorPane.setBottomAnchor(mainViewPane, 0.0);
        AnchorPane.setRightAnchor(mainViewPane, 0.0);
        AnchorPane.setLeftAnchor(mainViewPane, 0.0);
        AnchorPane.setTopAnchor(mainViewPane, 0.0);

        rootPane.getChildren().addAll(mainViewPane);
    }

    public Parent getView() {
        return rootPane;
    }

    public void startUp() {

        // start-up first time?
        if (uiConfiguration.isFirstStart()) {
            uiConfiguration.setFirstStart(false);
            ConfigurationManager.getInstance().saveConfiguration();

            modalDialog.show(configuration.getView());
        } // start-up with default domain or show domain chooser:
        // verify there is a default domain configured 
        else if (uiConfiguration.isStartupWithDefaultDomain() && (domainDataModel.findDefaultDomain() != null)) {
            domainDataModel.initWithDefaultDomain();
        } else {
            modalDialog.show(domainChooser.getView());
        }

    }
}
