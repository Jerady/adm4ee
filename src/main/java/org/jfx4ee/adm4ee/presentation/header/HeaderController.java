/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.header;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.inject.Inject;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.presentation.about.About;
import org.jfx4ee.adm4ee.presentation.configuration.Configuration;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Jens Deters
 */
public class HeaderController {

    @Inject
    private ViewContext viewContext;
    private UIConfiguration uiConfiguration;
    private ModalDialog modalDialog;
    @Inject
    private Configuration configuration;
    @Inject
    private About about;
    @FXML
    private ImageView logoImageView;
    @FXML
    private Button aboutButton;
    @FXML
    private Button exitButton;
    @FXML
    private Button configurationButton;

    @FXML
    void initialize() {
        uiConfiguration = viewContext.getUiConfiguration();
        modalDialog = viewContext.getModalDialog();
        AwesomeDude.setIcon(aboutButton, AwesomeIcon.INFO, uiConfiguration.getIconEmSize(), ContentDisplay.TOP);
        AwesomeDude.setIcon(exitButton, AwesomeIcon.SIGN_OUT, uiConfiguration.getIconEmSize(), ContentDisplay.TOP);
        AwesomeDude.setIcon(configurationButton, AwesomeIcon.GEAR, uiConfiguration.getIconEmSize(), ContentDisplay.TOP);
        logoImageView.setImage(new Image(uiConfiguration.getAppLogoName()));
    }

    @FXML
    public void showConfiguration() {
        modalDialog.show(configuration.getView());
    }

    @FXML
    public void showAbout() {
        modalDialog.show(about.getView());
    }

    @FXML
    public void exitApp() {
        viewContext.exitApp();
    }
}
