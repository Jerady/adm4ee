/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.logviewer;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import javax.inject.Inject;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.business.configuration.entity.UIConfiguration;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Jens Deters
 */
public class LogViewerController {

    private static final Logger logger = Logger.getLogger(LogViewerController.class.getName());
    @Inject
    private ViewContext viewContext;
    private UIConfiguration uiConfiguration;
    private DomainDataModel domainDataModel;
    private ModalDialog modalDialog;
    @FXML
    private Pane logViewPane;
    @FXML
    private ResourceBundle resources;
    @FXML
    private TextArea logfileTextArea;
    @FXML
    private Button updateLogViewerButton;
    @FXML
    private Button saveLogFileButton;
    @FXML
    private TextField logFileCharacterCountField;

    @FXML
    void initialize() {
        uiConfiguration = viewContext.getUiConfiguration();
        domainDataModel = viewContext.getDomainDataModel();
        modalDialog = viewContext.getModalDialog();

        AwesomeDude.setIcon(saveLogFileButton, AwesomeIcon.SAVE, uiConfiguration.getIconEmSize(), ContentDisplay.TOP);
        AwesomeDude.setIcon(updateLogViewerButton, AwesomeIcon.REFRESH, uiConfiguration.getIconEmSize(), ContentDisplay.TOP);
        logFileCharacterCountField.setTooltip(new Tooltip(resources.getString("logviewer.label.numCharsFromLog.tooltip")));
        logfileTextArea.setScrollTop(Double.MAX_VALUE);
        bindProperties();
        attachListeners();
    }

    private void bindProperties() {
        logfileTextArea.textProperty().bind(domainDataModel.serverLogProperty());
        StringConverter<? extends Number> converter = new IntegerStringConverter();
        Bindings.bindBidirectional(logFileCharacterCountField.textProperty(), domainDataModel.logFileCharacterCountProperty(), (StringConverter<Number>) converter);
    }

    private void attachListeners() {
        logFileCharacterCountField.setOnKeyPressed((KeyEvent ke) -> {
            if (ke.getCode().equals(KeyCode.ENTER)) {
                reloadLog();
            }
        });

        domainDataModel.serverLogProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            logfileTextArea.setScrollTop(Double.MAX_VALUE);
        });

        logfileTextArea.focusedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            reloadLog();
        });
    }

    @FXML
    public void reloadLog() {
        try {
            domainDataModel.loadServerLog();
        } catch (Exception ex) {
            Dialogs.create().showException(ex);
        }
    }

    @FXML
    public void hideAction() {
        modalDialog.hide();
    }

    @FXML
    public void saveLogFile() {
        FileChooser fileChooser = new FileChooser();

        File lastDir = new File(uiConfiguration.getLastChoosenDirectory());
        if (!lastDir.exists()) {
            lastDir = new File(System.getProperty("user.home"));
        }
        fileChooser.setInitialDirectory(lastDir);
        String logFileName = domainDataModel.getDomainName() + ".log";
        fileChooser.setInitialFileName(logFileName);

        File file = fileChooser.showSaveDialog(viewContext.getPrimaryStage());

        if (file != null) {
            try {
                try (FileWriter fileWriter = new FileWriter(file)) {
                    fileWriter.write(domainDataModel.getServerLog());
                }
                uiConfiguration.setLastChoosenDirectory(fileChooser.getInitialDirectory().getAbsolutePath());
            } catch (IOException ex) {
                logger.log(Level.SEVERE, resources.getString("logviewer.message.errorSavingLogFile"));
                Dialogs.create()
                        .owner(viewContext.getPrimaryStage())
                        .message(resources.getString("logviewer.message.errorSavingLogFile"))
                        .nativeTitleBar()
                        .showException(ex);
        }
    }
}
}
