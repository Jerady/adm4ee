/*
 * Copyright 2013 jdeters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.dashboard;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import java.util.ResourceBundle;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.presentation.controls.indicator.Indicator;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author jdeters
 */
public class ApplicationCell extends ListCell<Application> {

    private ViewContext viewContext;
    private DomainDataModel domainDataModel;
    private AnchorPane anchorPane;
    private Label appNameLabel;
    private Label versionLabel;
    private Label statusLabel;
    private Label updateStatusLabel;
    private MenuButton applicationActionMenuButton;
    private Indicator statusIndicator;
    private ProgressIndicator progressIndicator;
    private ProgressBar progressBar;
    private Application application;
    private UpdateAppService updateAppService;
    private ResourceBundle resources;

    public ApplicationCell(ViewContext viewContext) {
        this.viewContext = viewContext;
        init();
    }

    private void init() {
        domainDataModel = viewContext.getDomainDataModel();
        resources = viewContext.getResourceBundle();

        updateAppService = new UpdateAppService();

        appNameLabel = new Label();
        appNameLabel.getStyleClass().add("application-name-label");
        AnchorPane.setLeftAnchor(appNameLabel, 30.0);
        AnchorPane.setTopAnchor(appNameLabel, 5.0);

        versionLabel = new Label();
        versionLabel.getStyleClass().add("application-version-label");
        AnchorPane.setLeftAnchor(versionLabel, 90.0);
        AnchorPane.setBottomAnchor(versionLabel, 5.0);

        statusLabel = new Label();
        statusLabel.getStyleClass().add("application-status-label");
        AnchorPane.setBottomAnchor(statusLabel, 5.0);
        AnchorPane.setLeftAnchor(statusLabel, 30.0);

        updateStatusLabel = new Label("Loading Update");
        updateStatusLabel.getStyleClass().add("application-status-label");

        applicationActionMenuButton = new MenuButton();
        AwesomeDude.setIcon(applicationActionMenuButton, AwesomeIcon.GEARS, "2em");
        applicationActionMenuButton.setVisible(true);
        applicationActionMenuButton.getStyleClass().add("navigation-button");
        addApplicationActionsMethods();
        AnchorPane.setBottomAnchor(applicationActionMenuButton, 0.0);
        AnchorPane.setRightAnchor(applicationActionMenuButton, 0.0);

        statusIndicator = new Indicator();
        statusIndicator.setMaxWidth(5.0);
        AnchorPane.setTopAnchor(statusIndicator, Double.MIN_VALUE);
        AnchorPane.setLeftAnchor(statusIndicator, Double.MIN_VALUE);
        AnchorPane.setBottomAnchor(statusIndicator, Double.MIN_VALUE);

        progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.setPrefWidth(50.0);
        progressIndicator.setPrefHeight(50.0);
        AnchorPane.setBottomAnchor(progressIndicator, 5.0);
        AnchorPane.setRightAnchor(progressIndicator, 5.0);

        progressBar = new ProgressBar(0.0);
        progressBar.setPrefWidth(50.0);
        progressBar.setPrefHeight(15.0);
        progressBar.setMinHeight(15.0);

        HBox updateBox = new HBox(updateStatusLabel, progressBar);
        updateBox.setAlignment(Pos.CENTER_RIGHT);
        updateBox.setSpacing(5.0);
        AnchorPane.setTopAnchor(updateBox, 5.0);
        AnchorPane.setRightAnchor(updateBox, 5.0);

        anchorPane = new AnchorPane();
        anchorPane.getStyleClass().add("app-list-cell");
        anchorPane.setPrefWidth(400.0);
        anchorPane.setPrefHeight(60.0);

        anchorPane.getChildren().addAll(appNameLabel, versionLabel, statusLabel, applicationActionMenuButton,
                statusIndicator, progressIndicator, updateBox);

        progressBar.progressProperty().bind(updateAppService.progressProperty());
        updateStatusLabel.textProperty().bind(updateAppService.messageProperty());
        updateBox.visibleProperty().bind(updateAppService.runningProperty());
        applicationActionMenuButton.visibleProperty().bind(updateAppService.runningProperty().not());
        applicationActionMenuButton.disableProperty().bind(domainDataModel.domainRunningProperty().not());
        statusLabel.visibleProperty().bind(updateAppService.runningProperty().not());
        versionLabel.visibleProperty().bind(updateAppService.runningProperty().not());
    }

    @Override
    public void updateItem(Application application, boolean empty) {
        super.updateItem(application, empty);
        setText(null);
        if (!empty && application != null) {
            this.application = application;
            appNameLabel.setText(application.getName());
            versionLabel.setText(application.getVersion());
            Indicator.Result result = Indicator.Result.INDETERMINDED;

            switch (application.getStatus()) {
                case NOT_INSTALLED:
                    statusLabel.setText(resources.getString("application.label.status.notInstalled"));
                    result = Indicator.Result.INDETERMINDED;
                    break;
                case UNKNOWN:
                    statusLabel.setText(resources.getString("application.label.status.unknown"));
                    result = Indicator.Result.INDETERMINDED;
                    break;
                case UPDATE_AVAILABLE:
                    statusLabel.setText(resources.getString("application.label.status.updateAvailable"));
                    result = Indicator.Result.INDETERMINDED;
                    break;
                case DISABLED:
                    statusLabel.setText(resources.getString("application.label.status.disabled"));
                    result = Indicator.Result.INDETERMINDED;
                    break;
                case FAILURE:
                    statusLabel.setText(resources.getString("application.label.status.failure"));
                    result = Indicator.Result.FAIL;
                    break;
                case OK:
                    statusLabel.setText(resources.getString("application.label.status.ok"));
                    result = Indicator.Result.PASS;
                    break;
            }
            if (!domainDataModel.isDomainRunning()) {
                result = Indicator.Result.FAIL;
            }
            statusIndicator.setResult(result);
            setGraphic(anchorPane);
        } else {
            setGraphic(null);
        }
    }

    private void addApplicationActionsMethods() {

        MenuItem undeployAppItem = new MenuItem(resources.getString("application.action.undeploy"));
        undeployAppItem.setOnAction((ActionEvent t) -> {
            deleteApplication();
        });
        MenuItem enableAppItem = new MenuItem(resources.getString("application.action.enable"));
        enableAppItem.setOnAction((ActionEvent t) -> {
        });
        MenuItem disableAppItem = new MenuItem(resources.getString("application.action.disable"));
        disableAppItem.setOnAction((ActionEvent t) -> {
        });
        MenuItem openInBrowserItem = new MenuItem(resources.getString("application.action.openInBrowser"));
        openInBrowserItem.setOnAction((ActionEvent t) -> {
        });
        MenuItem updateAppItem = new MenuItem(resources.getString("application.action.update"));
        updateAppItem.setOnAction((ActionEvent t) -> {
            updateApplication();
        });
        applicationActionMenuButton.getItems().addAll(undeployAppItem, enableAppItem, disableAppItem, new SeparatorMenuItem(), openInBrowserItem, new SeparatorMenuItem(), updateAppItem);
    }

    private void lock() {
        domainDataModel.stopUpdateTimer();
        applicationActionMenuButton.setVisible(false);
        progressIndicator.setVisible(true);
    }

    private void unlock() {
        applicationActionMenuButton.setVisible(true);
        progressIndicator.setVisible(false);
        domainDataModel.startUpdateTimer();
    }

    private void deleteApplication() {
        Action response = Dialogs.create()
                .owner(viewContext.getPrimaryStage())
                .message(resources.getString("application.message.confirmUndeploy").concat(" ").concat(application.getName()).concat("?"))
                .nativeTitleBar()
                .actions(new Action[]{Dialog.Actions.YES, Dialog.Actions.NO})
                .showConfirm();

        if (response == Dialog.Actions.YES) {
            try {
                domainDataModel.getServer().deleteApplication(application.getName());
            } catch (Exception e) {
                // TODO: Handle Exception and show popup window here
            }
        }
        domainDataModel.refreshDomainState();
    }

    private void updateApplication() {
        updateAppService.restart();
    }

    private void checkForUpdate() {
        updateAppService.restart();
    }

    private class UpdateAppService extends Service<Void> {

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    updateMessage("Check for Update...");
                    updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
                    Thread.sleep(2000);
                    updateMessage("Update available.");
                    updateProgress(1.0, 1);
                    Thread.sleep(2000);
                    updateMessage("Download");
                    updateProgress(0.3, 1);
                    domainDataModel.getServer().updateApplication(application.getName());
                    Thread.sleep(2000);
                    updateMessage("Extract");
                    updateProgress(0.5, 1);
                    Thread.sleep(2000);
                    updateMessage("Install");
                    updateProgress(0.8, 1);
                    Thread.sleep(2000);
                    updateMessage("Done");
                    updateProgress(1, 1);
                    Thread.sleep(2000);
                    return null;
                }
            };
        }

    }
}
