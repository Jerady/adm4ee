/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.dashboard;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.Pane;
import javax.inject.Inject;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import org.jfx4ee.adm4ee.presentation.adminpassword.ChangeAdminPassword;
import org.jfx4ee.adm4ee.presentation.controls.ModalDialog;
import org.jfx4ee.adm4ee.presentation.controls.indicator.Indicator;
import org.jfx4ee.adm4ee.presentation.jdbcpool.JdbcPool;
import org.jfx4ee.adm4ee.presentation.jdbcpool.JdbcPoolController;
import org.jfx4ee.adm4ee.presentation.logviewer.LogViewer;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;
import org.jfx4ee.adm4ee.presentation.serviceports.ServicePorts;

/**
 *
 * @author Lummerland
 */
public class DashboardController {

    private static final Logger logger = Logger.getLogger(JdbcPoolController.class.getName());
    @Inject
    private ViewContext viewContext;
    private DomainDataModel domainDataModel;
    private ModalDialog modalDialog;
    private DomainService domainService;
    @Inject
    private LogViewer logViewer;
    @Inject
    private JdbcPool jdbcPool;
    @Inject
    private ChangeAdminPassword changeAdminPassword;
    @Inject
    private ServicePorts servicePorts;
    @FXML
    private ResourceBundle resources;
    @FXML
    private Pane domainControlPane;
    @FXML
    private Label domainNameLabel;
    @FXML
    private Label domainUptimeLabel;
    @FXML
    private Label domainStatusMessageLabel;
    @FXML
    private Label domainServiceMessageLabel;
    @FXML
    private Indicator domainStatusIndicator;
    @FXML
    private MenuButton actionsMenuButton;
    @FXML
    private MenuItem domainStartMenuItem;
    @FXML
    private MenuItem domainStopMenuItem;
    @FXML
    private MenuItem showJdbcPoolMenuItem;
    @FXML
    private MenuItem changeAdminPasswordMenuItem;
    @FXML
    private ProgressIndicator domainServiceProgressIndicator;
    @FXML
    private ListView<Application> applicationListView;

    @FXML
    void initialize() {
        domainDataModel = viewContext.getDomainDataModel();
        modalDialog = viewContext.getModalDialog();
        AwesomeDude.setIcon(actionsMenuButton, AwesomeIcon.GEARS, "2em");

        domainService = new DomainService();

        applicationListView.setVisible(!domainDataModel.getApplicationsList().isEmpty());
        applicationListView.setItems(domainDataModel.getApplicationsList());
        applicationListView.setCellFactory((ListView<Application> p) -> new ApplicationCell(viewContext));
        domainStatusIndicator.setResult(domainDataModel.isDomainRunning() ? Indicator.Result.PASS : Indicator.Result.FAIL);

        bindProperties();
        attachListeners();
    }

    @FXML
    public void domainStart() {
        logger.log(Level.INFO, "domain START");
        domainService.startDomain();
    }

    @FXML
    public void domainStop() {
        logger.log(Level.INFO, "domain STOP");
        Action response = Dialogs.create()
                .owner(viewContext.getPrimaryStage())
                .message(resources.getString("dashboard.confirm.domain.stop.message").concat(" '").concat(domainNameLabel.getText().concat("'?")))
                .nativeTitleBar()
                .actions(new Action[]{Dialog.Actions.YES, Dialog.Actions.NO})
                .showConfirm();

        if (response == Dialog.Actions.YES) {

            domainService.stopDomain();
        }
    }

    @FXML
    public void showLogViewer() {
        domainDataModel.loadServerLog();
        modalDialog.show(logViewer.getView());
    }

    @FXML
    public void showJDBCPool() {
        domainDataModel.refreshJdbcConnectionPoolsList();
        modalDialog.show(jdbcPool.getView());
    }

    @FXML
    public void showChangeAdminPassword() {
        modalDialog.show(changeAdminPassword.getView());
    }

    @FXML
    public void showServicePorts() {
        domainDataModel.refreshServicePortsMap();
        modalDialog.show(servicePorts.getView());
    }

    private void bindProperties() {
        domainControlPane.visibleProperty().bind(domainDataModel.modelLoadedProperty());

        domainStartMenuItem.disableProperty().bind(domainDataModel.domainRunningProperty());
        domainStopMenuItem.disableProperty().bind(domainDataModel.domainRunningProperty().not());
        domainStatusIndicator.passProperty().bind(domainDataModel.domainRunningProperty());
        showJdbcPoolMenuItem.disableProperty().bind(domainDataModel.domainRunningProperty().not());
        changeAdminPasswordMenuItem.disableProperty().bind(domainDataModel.domainRunningProperty().not());

        domainNameLabel.textProperty().bind(domainDataModel.domainNameProperty());
        domainUptimeLabel.textProperty().bind(domainDataModel.domainUptimeProperty());
        domainStatusMessageLabel.textProperty().bind(domainDataModel.domainStatusMessageProperty());
        domainServiceMessageLabel.textProperty().bind(domainService.messageProperty());

        // progress indicator
        domainServiceProgressIndicator.progressProperty().
                bind(domainService.progressProperty());
        domainServiceProgressIndicator.visibleProperty().
                bind(domainService.runningProperty());
        actionsMenuButton.visibleProperty().bind(domainService.runningProperty().not());

    }

    private void attachListeners() {
        domainDataModel.getApplicationsList().addListener((ListChangeListener.Change<? extends Application> change) -> {
            applicationListView.setVisible(!domainDataModel.getApplicationsList().isEmpty());
        });
        applicationListView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Application> ov, Application oldValue, Application newValue) -> {
            if (newValue != null) {
                System.out.println(newValue.getName());
            }
        });
    }

    private enum ActionMode {

        START, STOP;
    }

    private class DomainService extends Service<CommandResult> {

        private ActionMode actionMode;

        public DomainService() {
            init();
        }

        private void init() {
            actionMode = ActionMode.START;
        }

        public void startDomain() {
            actionMode = ActionMode.START;
            restart();
        }

        public void stopDomain() {
            actionMode = ActionMode.STOP;
            restart();
        }

        @Override
        protected void succeeded() {
            domainDataModel.refreshDomainState();
            domainDataModel.startUpdateTimer();
        }

        @Override
        protected Task<CommandResult> createTask() {
            return new Task<CommandResult>() {
                @Override
                protected CommandResult call() {
                    domainDataModel.stopUpdateTimer();
                    CommandResult result = null;
                    switch (actionMode) {
                        case START:
                            logger.log(Level.INFO, "attempt to start domain: {0}", domainDataModel.getDomainName());
                            updateMessage(viewContext.getResourceBundle().getString("service.domain.starting.message"));
                            result = domainDataModel.getServer().startDomain(domainDataModel.getDomainName());
                            updateMessage("");
                            logger.log(Level.INFO, "domain started: {0}", domainDataModel.getDomainName());
                            break;
                        case STOP:
                            logger.log(Level.INFO, "attempt to stop domain: {0}", domainDataModel.getDomainName());
                            updateMessage(viewContext.getResourceBundle().getString("service.domain.stopping.message"));
                            result = domainDataModel.getServer().stopDomain();
                            updateMessage("");
                            logger.log(Level.INFO, "domain stopped: {0}", domainDataModel.getDomainName());
                            break;
                    }
                    return result;
                }
            };
        }
    }

}
