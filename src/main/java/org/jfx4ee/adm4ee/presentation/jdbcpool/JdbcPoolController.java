/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.jdbcpool;

import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javax.inject.Inject;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import org.jfx4ee.adm4ee.presentation.master.DomainDataModel;
import org.jfx4ee.adm4ee.presentation.master.ViewContext;

/**
 *
 * @author Lummerland
 */
public class JdbcPoolController {

    @Inject
    private ViewContext viewContext;
    private DomainDataModel domainDataModel;
    @FXML
    private ResourceBundle resources;
    @FXML
    private ListView<String> jdbcPoolNamesListView;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField urlField;
    @FXML
    private TextField usernameField;
    @FXML
    private Button pingConnectionPoolButton;
    @FXML
    private Button applyChangesButton;
    @FXML
    private Button cancelChangesButton;
    @FXML
    private Label resultMessageLabel;
    @FXML
    private ProgressIndicator progressIndicator;
    private StringProperty selectedJdpcPoolNameProperty;
    private BooleanProperty dirtyProperty;

    @FXML
    void initialize() {
        domainDataModel = viewContext.getDomainDataModel();

        jdbcPoolNamesListView.setItems(domainDataModel.getJdbcConnectionsPoolsList());
        jdbcPoolNamesListView.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
        });

        jdbcPoolNamesListView.setOnMouseClicked((MouseEvent t) -> {
            setSelectedJdpcPoolName(jdbcPoolNamesListView.getSelectionModel().getSelectedItem());
        });

        urlField.textProperty().addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
            setDirty((oldValue != null) && !oldValue.equals(newValue));
        });
        usernameField.textProperty().addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
            setDirty((oldValue != null) && !oldValue.equals(newValue));
        });
        passwordField.textProperty().addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
            setDirty((oldValue != null) && !oldValue.equals(newValue));
        });

        pingConnectionPoolButton.disableProperty().bind(selectedJdpcPoolNameProperty().isNull());

        applyChangesButton.disableProperty().bind(dirtyProperty().not());
        cancelChangesButton.disableProperty().bind(dirtyProperty().not());
    }

    public void setSelectedJdpcPoolName(String selectedJdpcPoolName) {
        selectedJdpcPoolNameProperty().set(selectedJdpcPoolName);
        syncModelWithView();
        setDirty(false);
    }

    public String getSelectedJdpcPoolName() {
        return selectedJdpcPoolNameProperty().get();
    }

    public StringProperty selectedJdpcPoolNameProperty() {
        if (selectedJdpcPoolNameProperty == null) {
            selectedJdpcPoolNameProperty = new SimpleStringProperty();
        }
        return selectedJdpcPoolNameProperty;
    }

    public Boolean isDirty() {
        return dirtyProperty().get();
    }

    public void setDirty(Boolean dirty) {
        dirtyProperty().set(dirty);
    }

    public BooleanProperty dirtyProperty() {
        if (dirtyProperty == null) {
            dirtyProperty = new SimpleBooleanProperty(Boolean.FALSE);
        }
        return dirtyProperty;
    }

    private void syncModelWithView() {
        if (getSelectedJdpcPoolName() != null) {
            Map<String, String> poolParams = domainDataModel.getServer().getJdbcConnectionPoolParameters(getSelectedJdpcPoolName());
            urlField.setText(poolParams.get("URL"));
            usernameField.setText(poolParams.get("User"));
            passwordField.setText(poolParams.get("Password"));
            resultMessageLabel.setText("");
            resultMessageLabel.getStyleClass().setAll("ping-result-label-plain");

        }
    }

    @FXML
    public void pingConnectionPool() {
        Platform.runLater(() -> {
            resultMessageLabel.setText("");
            if (getSelectedJdpcPoolName() != null) {
                progressIndicator.setVisible(true);
                CommandResult result = domainDataModel.getServer().pingJdbcConnectionPool(getSelectedJdpcPoolName());
                progressIndicator.setVisible(false);
                if (result.isOk()) {
                    resultMessageLabel.getStyleClass().setAll("ping-result-label-success");
                    resultMessageLabel.setText(resources.getString("jdbcpool.message.pingResultSuccess"));
                } else {
                    resultMessageLabel.getStyleClass().setAll("ping-result-label-failed");
                    resultMessageLabel.setText(resources.getString("jdbcpool.message.pingResultFailed"));
                }
            }
        });

    }

    @FXML
    public void applyChanges() {
        if (getSelectedJdpcPoolName() != null) {

            Map<String, String> poolParams = domainDataModel.getServer().getJdbcConnectionPoolParameters(getSelectedJdpcPoolName());
            poolParams.put("URL", urlField.getText());
            poolParams.put("User", usernameField.getText());
            poolParams.put("Password", passwordField.getText());
            resultMessageLabel.setText("");
            CommandResult result = domainDataModel.getServer().setJdbcConnectionPoolParameters(getSelectedJdpcPoolName(), poolParams);
            if (result.isOk()) {
                resultMessageLabel.getStyleClass().setAll("ping-result-label-success");
                resultMessageLabel.setText(resources.getString("jdbcpool.message.changesApplied"));
                setDirty(Boolean.FALSE);
            } else {
                resultMessageLabel.getStyleClass().setAll("ping-result-label-failed");
                resultMessageLabel.setText(resources.getString("jdbcpool.message.changesSaveError"));
            }
        }
    }

    @FXML
    public void cancelChanges() {
        syncModelWithView();
        setDirty(Boolean.FALSE);
        resultMessageLabel.getStyleClass().setAll("ping-result-label-plain");
        resultMessageLabel.setText(resources.getString("jdbcpool.message.changesCanceled"));

    }

    @FXML
    public void hideAction() {
        viewContext.getModalDialog().hide();
    }
}
