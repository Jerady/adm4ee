/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.presentation.controls;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 *
 * @author Jens Deters
 */
public class ModalDialog extends StackPane {

    private final Duration DURATION_HIDE = Duration.millis(200);
    private final Duration DURATION_SHOW = Duration.millis(200);
    private ImageView blurView;
    private Region colorGlass;

    public ModalDialog() {
        init();
    }

    private void init() {
        blurView = new ImageView();
        blurView.setFocusTraversable(false);
        BoxBlur bb = new BoxBlur();
        bb.setWidth(10);
        bb.setHeight(10);
        bb.setIterations(10);
        blurView.setEffect(bb);
        blurView.setOpacity(0.9);

        colorGlass = new Region();
        colorGlass.getStyleClass().add("modal-dialog-color-glass-pane");
        getChildren().add(colorGlass);


        setOnMouseClicked((MouseEvent t) -> {
            t.consume();
        });
        setVisible(false);
    }

    public void hide() {
        setCache(true);

        Timeline timeline = new Timeline(new KeyFrame(DURATION_HIDE, (ActionEvent t) -> {
            setCache(false);
            setVisible(false);
            getChildren().
                    clear();
        },
                new KeyValue(opacityProperty(), 0, Interpolator.EASE_BOTH)));
        timeline.play();

    }

    public void show(Node dialogPane) {
        addBlurLayer();

        dialogPane.getStyleClass().add("modal-dialog-pane");
        getChildren().add(dialogPane);
        dialogPane.toFront();
        setOpacity(0);
        setCache(true);
        setVisible(true);

        Timeline timeline = new Timeline(new KeyFrame(DURATION_SHOW, (ActionEvent t) -> {
            setCache(false);
        },new KeyValue(opacityProperty(), 1, Interpolator.EASE_BOTH)));

        timeline.play();
    }

    private void addBlurLayer() {
        setVisible(false);
        getChildren().removeAll(blurView, colorGlass);

        SnapshotParameters parameters = new SnapshotParameters();
        Point2D startPointInScene = localToScene(0, 0);
        Rectangle2D toPaint = new Rectangle2D(startPointInScene.getX(), startPointInScene.getY(), getLayoutBounds().getWidth(), getLayoutBounds().getHeight());
        parameters.setViewport(toPaint);
        WritableImage image = new WritableImage((int) toPaint.getWidth(), (int) toPaint.getHeight());
        image = getScene().getRoot().snapshot(parameters, image);
        blurView.setImage(image);
        getChildren().addAll(blurView, colorGlass);
        colorGlass.toBack();
        blurView.toBack();
        setVisible(true);
    }
}
