print("-- Glassfish Domain Setup Script processing at domain: " + server.getDomainPath());
server.initLocalDomain();
print(server.startDomain("testDomain"));
print(server.pingManagementResource());
print("Deploying application WebTest.war...");
var File = Java.type("java.io.File");
print(server.createApplication("WebTest", "/WebTest", new File(templateApplicationsPath + "/WebTest.war"), ""));
print("-- Glassfish Domain Setup Script finished.");