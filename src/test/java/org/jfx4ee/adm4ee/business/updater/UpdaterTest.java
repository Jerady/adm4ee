package org.jfx4ee.adm4ee.business.updater;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.jfx4ee.adm4ee.business.appcast.boundary.AppcastManager;
import org.jfx4ee.adm4ee.business.appcast.boundary.AppcastManagerTest;
import org.jfx4ee.adm4ee.business.appcast.control.AppcastException;
import org.jfx4ee.adm4ee.business.appcast.entity.Appcast;
import org.jfx4ee.adm4ee.business.appcast.entity.Channel;
import org.jfx4ee.adm4ee.business.appcast.entity.Enclosure;
import org.jfx4ee.adm4ee.business.appcast.entity.Item;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Jens Deters
 */
public class UpdaterTest {

    Updater updater;

    @Before
    public void init() throws IOException, AppcastException, Exception {
        updater = new Updater(null);
        AppcastManager appcastManagerMock = mock(AppcastManager.class);
        Appcast appcast = new Appcast();
        Channel c = new Channel();
        Item i = new Item();
        Enclosure e = new Enclosure();
        e.setVersion("2.0.4711");
        i.setEnclosure(e);
        List<Item> items = new ArrayList<>();
        items.add(i);
        c.setItems(items);
        appcast.setChannel(c);

        when(appcastManagerMock.fetch(anyString())).thenReturn(appcast);
        when(appcastManagerMock.getLatestRemoteVersion(anyString())).thenReturn("2.0.4711");
        when(appcastManagerMock.getManifestAppcastVersion(any(Path.class))).thenReturn("2.0.1322");
        when(appcastManagerMock.download(any(Appcast.class), any(Path.class))).thenCallRealMethod();
        updater.appcastManager = appcastManagerMock;
    }

    @Test
    public void testGetInstalledApplicationVersion() {
        URL url = getClass().getResource("/MANIFEST.MF");
        String version = null;
        try {
            version = updater.getInstalledApplicationVersion(Paths.get(url.toURI()));
        } catch (URISyntaxException ex) {
            fail(ex.toString());
        }
        assertEquals("2.0.1322", version);
    }
    
    @Test
    public void testGetInstalledApplicationVersionNotInstalled() {
        // We need a real updater instance here
        Updater u = new Updater(null);
        String version = null;
        version = u.getInstalledApplicationVersion(Paths.get("/_NOT_EXISTING_FILE_"));
        assertNull(version);
    }
    
    @Test
    public void testGetInstalledApplicationVersionNoAppcast() {
        // We need a real updater instance here
        Updater u = new Updater(null);
        URL url = getClass().getResource("/MANIFEST_wo_appcast.MF");
        String version = null;
        try {
            version = u.getInstalledApplicationVersion(Paths.get(url.toURI()));
        } catch (URISyntaxException ex) {
            fail(ex.toString());
        }
        assertEquals("", version);
    }

    @Test
    public void testGetApplicationStatus() {
        ApplicationStatus applicationStatus = updater.getApplicationStatus("2.0.1044", "TESTURL");
        assertEquals(ApplicationStatus.UPDATE_AVAILABLE, applicationStatus);
        assertEquals("2.0.4711", applicationStatus.getInfo());
        assertNotNull(applicationStatus.getUpdateTime());

        applicationStatus = updater.getApplicationStatus("2.0.4711", "TESTURL");
        assertEquals(ApplicationStatus.OK, applicationStatus);
        assertNotNull(applicationStatus.getUpdateTime());

        applicationStatus = updater.getApplicationStatus("2.2.4711", "TESTURL");
        assertEquals(ApplicationStatus.UNKNOWN, applicationStatus);
        assertNotNull(applicationStatus.getUpdateTime());
    }
    
    @Test
    public void testUpdate() throws Exception {
        try {
            AppcastManagerTest test = new AppcastManagerTest();
            Appcast appcast = test.getAppcast();
            appcast.getChannel().getItems().get(0).getEnclosure().setUrl(getClass().getResource("/WebTest.zip").toURI().toURL().toString());
            appcast.getChannel().getItems().get(0).getEnclosure().setLength(3997);
            Path createdTempDirectory = null;
            try {
                createdTempDirectory = Files.createTempDirectory("ac-");
            } catch (IOException ex) {
                fail(ex.toString());
            }
            try {
                updater.update("TestApp", appcast, createdTempDirectory);
            } finally {
                try { Files.deleteIfExists(createdTempDirectory); } catch (Exception e) { /* Ignore */ }
            }
        } catch (Exception exception) {
            fail("Could not update: " + exception.toString());
        }
    }

}
