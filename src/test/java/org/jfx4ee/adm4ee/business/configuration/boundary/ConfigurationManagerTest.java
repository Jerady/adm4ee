package org.jfx4ee.adm4ee.business.configuration.boundary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.configuration.entity.Database;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
public class ConfigurationManagerTest {

    ConfigurationManager config;

    public ConfigurationManagerTest() {
        try {
            config = new ConfigurationManager();
            URL url = getClass().getResource("/sample-config.xml");
            config.configFile = Paths.get(url.toURI());
            config.init();
        } catch (Exception e) {
            // Inital load configuration might fail
            System.out.println("Initial configuration loading failed --> OK");
        }
    }

    @Test
    public void testLoadConfiguration() {
        try {
            URL url = getClass().getResource("/sample-config.xml");
            config.configFile = Paths.get(url.toURI());
            boolean loaded = config.loadConfiguration();
            assertTrue(loaded);

            Level logLevel = config.getLogLevel();
            assertEquals(Level.INFO, logLevel);
            List<Domain> domains = config.getDomains();
            assertNotNull(domains);
            assertTrue(domains.size() > 0);
            Domain domain = domains.get(0);
            assertNotNull(domain);
            assertEquals("testdomain1", domain.getDomainName());
            assertNotNull(domain.getServerType());

            List<Application> applications = domain.getApplications();
            assertNotNull(applications);
            assertTrue(applications.size() > 0);

            Application app = applications.get(0);
            assertNotNull(app);
            assertEquals("WebTest", app.getName());

            List<Database> databases = domain.getDatabases();
            assertNotNull(databases);

            Database db = databases.get(0);
            assertEquals("Master", db.getName());

            String pingAction = db.getPingAction();
            assertEquals("ping", pingAction.toLowerCase());

            Map<String, String> properties = db.getProperties();
            assertNotNull(properties);
            String dburl = properties.get("url");
            assertNotNull(dburl);
            assertEquals("APP", properties.get("username"));

        } catch (Exception ex) {
            fail("Could not load configuration: " + ex);
        }
    }

    @Test
    public void testSaveConfiguration() {
        Database db = config.getDomains().get(0).getDatabases().get(0);
        db.setProperty("TestName", "TestValue");
        config.saveConfiguration();
    }

    @Test
    public void testGetDomainWithDomainName() {
        String serverRootPath = "/path/to/glassfish-3.1.2.2";
        String domainName = "testdomain1";
        Domain domain = config.getDomain(serverRootPath, domainName);
        assertNotNull(domain);
        assertTrue(domain.getDomainName().equals(domainName));
    }

    public Domain getDomainMock() {
        return config.getDomains().get(0);
    }
}
