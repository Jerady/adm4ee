package org.jfx4ee.adm4ee.business.server.boundary;

import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManagerTest;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
public class ServerHandlerTest {

    Domain domain;

    public ServerHandlerTest() {
        ConfigurationManagerTest configTest = new ConfigurationManagerTest();
        domain = configTest.getDomainMock();
    }

    @Test
    public void testGetInstance() {
        ServerHandler serverHandler = ServerHandler.getInstance(domain);
        assertNotNull(serverHandler);
        assertNotNull(serverHandler.getServer());
        assertNotNull(serverHandler.getServerType());
    }

}
