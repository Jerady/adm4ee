package org.jfx4ee.adm4ee.business.server.control.glassfish;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.Application;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import org.jfx4ee.adm4ee.business.util.OsHelper;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
@Ignore
public class GlassfishIT {

    Glassfish server;
    Domain domain;
    private ConfigurationManager config;

    @Before
    public void init() throws Exception {
        config = ConfigurationManager.getInstance();
        config.setLogLevel(Level.FINER);
        List<Domain> domains = config.getDomains();
        domains.clear();
        domain = new Domain();
        domain.setServerType(ServerType.GLASSFISH);
        File templateFile = new File(getClass().getResource("/testTemplate").toURI());
        domain.setTemplate(templateFile.getCanonicalPath());
        if (OsHelper.isWindows()) {
            domain.setServerRootPath("C:\\glassfish-3.1.2.2");
        } else {
            domain.setServerRootPath("/Applications/Netbeans/glassfish-3.1.2.2");
        }
        domain.setDomainName("domain1");
        domain.setDomainUser("admin");
        domain.setDomainPassword("");
        domains.add(domain);
        server = new Glassfish(domain);
        server.initLocalServer();
        server.initLocalDomain();
    }
    
    @Test
    public void createDomainIT() {
        // We have to init again here, because we should not use initLocalDomain() in case of createDomain test case
        domain.setDomainName("testDomain");
        domain.setDomainUser("testuser");
        domain.setDomainPassword("testpassword");
        server = new Glassfish(domain);
        server.initLocalServer();
        System.out.print("createDomainIT(): ");
        URL url = getClass().getResource("/testTemplate");
        File domainXmlTemplate = new File(url.getFile());
        CommandResult result = server.createDomain(domain.getDomainName(), 12000, domainXmlTemplate, domain.getDomainUser(), domain.getDomainPassword(), false);
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
    }
    
    @Test
    public void deleteDomainIT() {
        domain.setDomainName("testDomain");
        domain.setDomainUser("testuser");
        domain.setDomainPassword("testpassword");
        server = new Glassfish(domain);
        server.initLocalServer();
        server.initLocalDomain();
        System.out.println("deleteDomainIT(...): ");
        CommandResult result = server.deleteDomain(domain.getDomainName());
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
    }

    @Test
    public void pingManagementResouceIT() {
        System.out.print("pingManagementResource(): ");
        CommandResult result = server.pingManagementResource();
        if (result.isOk()) {
            System.out.println("Running");
        } else {
            if (result.getExitCode() == CommandResult.EXIT_CODE_UNKNOWN) {
                System.out.println("Stopped - " + result.getThrowable().getCause());
            } else {
                System.out.println("ERROR: " + result.getExitCode() + " " + result.getResult());
            }
        }
    }

    @Test
    public void getLocalVersionStringIT() {
        System.out.print("getLocalVersionString(): ");
        String localVersionString = server.getLocalVersionString();
        System.out.println(localVersionString);
        assertTrue(localVersionString != null && localVersionString.contains("GlassFish Server"));
    }

    @Test
    public void getLocalJdkVersionString() {
        System.out.print("getLocalJdkVersionString(): ");
        String localJdkVersionString = server.getLocalJdkVersionString();
        System.out.println(localJdkVersionString);
        assertTrue(localJdkVersionString != null && !localJdkVersionString.isEmpty());
    }

    @Test
    public void startDomainIT() {
        System.out.println("startDomain(): ");
        CommandResult result = server.startDomain(domain.getDomainName());
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
    }

    @Test
    public void stopDomainIT() {
        System.out.println("stopDomain(): ");
        CommandResult result = server.stopDomain();
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
    }

    @Test
    public void getLogFileContentIT() {
        System.out.println("getLogFileContent(): ");
        int characters = 5000;
        CommandResult result = server.getLogFileContent(characters);
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
        assertTrue(result.getResult(), result.getResult().length() > 0);
        System.out.println(result.toString());
    }

    @Test
    public void verifyDomainConfigurationIT() {
        System.out.println("verifyDomainConfiguration(): ");
        CommandResult result = server.verifyDomainConfiguration();
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
    }

    @Test
    public void getInstalledApplicationsIT() {
        System.out.println("getInstalledApplications(): ");
        Set<String> applications = server.getInstalledApplications();
        System.out.println(applications);
        assertTrue(applications != null);
    }
    
    @Test
    public void getApplicationsIT() {
        System.out.println("getApplications(): ");
        List<Application> apps = new ArrayList<>();
        Application app = new Application("WebTest3");
        app.setUpdateUrl("http://localhost:8080/WebTEst4/appcast/appcast.xml");
        apps.add(app);
        domain.setApplications(apps);

        server.refreshApplicationStatus();        
        List<Application> applications = server.getApplications();
        
        System.out.println("Status: " + applications);
        assertNotNull(applications);
        assertTrue(applications.size() > 0);
        Application a = applications.get(0);
        assertNotNull(a.getVersion());
    }
    
    @Test
    public void updateApplicationIT() throws Exception {
        System.out.println("updateApplication(): ");
        List<Application> apps = new ArrayList<>();
        Application app = new Application("WebTest");
        app.setUpdateUrl(getClass().getResource("/appcast.xml").toURI().toURL().toString());
        apps.add(app);
        domain.setApplications(apps);
        //server.domain = domain;
        server.refreshApplicationStatus();
        System.out.println(domain.getApplications());
        
        server.updateApplication("WebTest");
    }

    @Test
    public void getJdbcConnectionPoolsIT() {
        System.out.println("getJdbcConnectionPools(): ");
        Set<String> pools = server.getJdbcConnectionPools();
        System.out.println(pools);
        assertTrue(pools != null);
    }
    
    @Test
    public void getJdbcConnectionPoolParametersIT() {
        System.out.println("getJdbcConnectionPoolParameters(...): ");
        Map<String, String> jdbcConnectionPoolParameters = server.getJdbcConnectionPoolParameters("SamplePool");
        System.out.println(jdbcConnectionPoolParameters);
        assertTrue(jdbcConnectionPoolParameters != null);
    }
    
    @Test
    public void pingJdbcConnectionPoolIT() {
        System.out.println("pingJdbcConnectionPool(): ");
        CommandResult result = server.pingJdbcConnectionPool("SamplePool");
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
        //System.out.println(result.getExitCode() + " " + result.getExitCodeInfo() + " :: " + result);
    }
    
    @Test
    public void setJdbcConnectionPoolParametersIT() {
        System.out.println("getJdbcConnectionPoolParameters(...): ");
        Map<String,String> parameters = new HashMap<>();
        parameters.put("User", "app");
        parameters.put("Password", "app");
        parameters.put("DatabaseName", "sample");
        parameters.put("serverName", "localhost");
        parameters.put("PortNumber", "1527");
        parameters.put("URL", "jdbc:derby://localhost:1527/sample");
        CommandResult result = server.setJdbcConnectionPoolParameters("SamplePool", parameters);
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
    }
    
    @Test
    public void createJdbcResourceIT() {
        System.out.println("createJdbcResourceIT(...): ");
        CommandResult result = server.createJdbcResource("jdbc/samplePoolTest", "SamplePool");
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
    }
    
    @Test
    public void deleteJdbcResourceIT() {
        System.out.println("deleteJdbcResourceIT(...): ");
        CommandResult result = server.deleteJdbcResource("jdbc/samplePoolTest");
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
    }
    
    @Test
    public void readBytesFromFileEndIT() throws Exception {
        Path logFilePath = server.getDomainPath().resolve("logs").resolve("server.log");
        String content = server.readBytesFromFileEnd(logFilePath.toString(), 100);
        System.out.println(content);
    }
    
    @Test
    public void createApplicationIT() {
        System.out.println("createApplicationIT(...): ");
        URL url = getClass().getResource("/testTemplate/applications/WebTest.war");
        File file = new File(url.getFile());
        CommandResult result = server.createApplication("WebTest3", "/WebTest4", file, "Test description");
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
    }
    
    @Test
    public void deleteApplicationIT() {
        System.out.println("deleteApplicationIT(...): ");
        CommandResult result = server.deleteApplication("WebTest");
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
    }
    
    @Test
    public void changeAdminPasswordIT() {
        System.out.println("changeAdminPasswordIT(...): ");
        CommandResult result = server.changeAdminPassword("", "testpass");
        System.out.println(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable());
        CommandResult result2 = server.pingManagementResource();
        System.out.println(result2.getExitCode() + " " + result2.getResult() + " :: " + result2.getThrowable());
    }
    
    @Test
    public void listLibrariesIT() {
        System.out.println("listLibrariesIT(...): ");
        Set<String> libraries = server.listLibraries();
    }
}
