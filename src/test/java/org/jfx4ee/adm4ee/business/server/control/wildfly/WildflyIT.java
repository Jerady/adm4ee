/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.wildfly;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import org.jfx4ee.adm4ee.business.configuration.boundary.ConfigurationManager;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import org.jfx4ee.adm4ee.business.util.OsHelper;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
public class WildflyIT {

    Wildfly server;
    Domain domain;
    private ConfigurationManager config;

    @Before
    public void init() throws Exception {
        config = ConfigurationManager.getInstance();
        config.setLogLevel(Level.FINER);
        List<Domain> domains = config.getDomains();
        domains.clear();
        domain = new Domain();
        domain.setServerType(ServerType.WILDFLY_STANDALONE);
        File templateFile = new File(getClass().getResource("/testTemplate").toURI());
        domain.setTemplate(templateFile.getCanonicalPath());
        if (OsHelper.isWindows()) {
            domain.setServerRootPath("C:\\jboss-as-7.1.1.Final");
        } else {
            domain.setServerRootPath("/Applications/Netbeans/jboss-as-7.1.1.Final");
        }
        domain.setDomainName("domain1");
        domain.setDomainUser("admin");
        domain.setDomainPassword("testpass");
        domains.add(domain);
        server = new Wildfly(domain);
        server.initLocalServer();
        server.initLocalDomain();
    }

    @Test
    public void startDomainIT() {
        System.out.println("startDomain(): ");
        CommandResult result = server.startDomain(domain.getDomainName());
        System.out.println(result.getResult());
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
    }
    
    @Test
    public void stopDomainIT() {
        System.out.println("stopDomain(): ");
        CommandResult result = server.stopDomain();
        System.out.println(result.getResult());
        assertTrue(result.getExitCode() + " " + result.getResult() + " :: " + result.getThrowable(), result.isOk());
    }
    
    @Test
    public void getVersionStringIT() {
        System.out.print("getVersionString(): ");
        String versionString = server.getVersionString();
        System.out.println(versionString);
        assertTrue(versionString != null && versionString.contains("JBoss"));
    }

    @Test
    public void getJdkVersionString() {
        System.out.print("getLocalJdkVersionString(): ");
        String localJdkVersionString = server.getJdkVersionString();
        System.out.println(localJdkVersionString);
        assertTrue(localJdkVersionString != null && !localJdkVersionString.isEmpty());
    }
    
    @Test
    public void getUptimeIT() {
        System.out.println("getUptime(): ");
        long uptime = server.getUptime();
        System.out.println(uptime);
        assertTrue(uptime > 0);
    }
}
