package org.jfx4ee.adm4ee.business.server.control.glassfish;

import java.nio.file.Path;
import org.jfx4ee.adm4ee.business.configuration.entity.Domain;
import org.jfx4ee.adm4ee.business.server.boundary.ServerType;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
public class GlassfishTest {

    Glassfish glassfish;
    Domain domain;

    @Before
    public void init() {
        domain = new Domain();
        domain.setServerType(ServerType.GLASSFISH);
        domain.setServerRootPath("C:\\glassfish-3.1.2.2");
        domain.setUrl("localhost:4848");
        domain.setDomainName("domain1");
        domain.setDomainUser("admin");
        domain.setDomainPassword("");
    }
        
    @Test
    public void testGlassfish() {
        try {
            glassfish = new Glassfish(domain);
        } catch (Exception ex) {
            System.err.println("ERROR: " + ex);
            ex.printStackTrace();
            fail("Could not initialize Glassfish: " + ex);
        }
    }
    
    @Test
    public void testInitRemote() {
        glassfish = new Glassfish(domain);
        try {
            glassfish.initRemoteDomain();
            assertNotNull(glassfish.restManager);
        } catch (Exception ex) {
            fail("Could not initialize remote client: " + ex);
        }
    }
    
    @Test
    public void testPingManagementResource() {
        glassfish = new Glassfish(domain);
        glassfish.initRemoteDomain();
        CommandResult result = glassfish.pingManagementResource();
        assertNotNull(result);
    }
    
    @Test
    public void testGetDomainConfigPath() {
        glassfish = new Glassfish(domain);
        Path domainConfigPath = glassfish.getDomainConfigPath();
        assertNotNull(domainConfigPath);
        assertTrue(domainConfigPath.toString().contains("domains"));
    }
}
