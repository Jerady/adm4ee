/*
 * Copyright 2014 dino.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.wildfly;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 *
 * @author dino
 */
public class ConfigXmlManagerTest {

    ConfigXmlManager configXmlManager;

    @Before
    public void init() {
        try {
            // test file
            File f = new File("target/test-classes/wildfly/standalone.xml");
            configXmlManager = new ConfigXmlManager();
            configXmlManager.parseFile(f);
        } catch (Exception ex) {
            System.err.println("ERROR: " + ex);
        }
    }

    @Test
    public void testParseFile() {
        assertNotNull(configXmlManager);
        assertNotNull(configXmlManager.doc);
        Document doc = configXmlManager.doc;
        NodeList nodes = doc.getElementsByTagName("server");
        assertNotNull(nodes);
        assertTrue(nodes.getLength() > 0);
    }

    @Test
    public void testGetServicePorts() {
        Map<String, Integer> servicePorts = configXmlManager.getServicePorts();
        assertTrue(servicePorts.size() > 0);
        assertTrue(servicePorts.get(ServiceType.MANAGEMENT_HTTP.name()) == 9990);
        System.out.println("ServicePorts: " + servicePorts);
    }
}
