/*
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jfx4ee.adm4ee.business.server.control.wildfly;

import java.util.Set;
import org.jfx4ee.adm4ee.business.util.CommandResult;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
public class WildflyRestManagerIT {

    WildflyRestManager manager;

    @Before
    public void init() {
        manager = new WildflyRestManager("localhost:9990", "admin", "password", false);
    }

    @Test
    public void testGetUptime() {
        System.out.println("testGetUptime(): ");
        String uptime = manager.getUptime();
        System.out.println(uptime);
    }

    @Test
    public void testStopServer() {
        System.out.println("testStopServer(): ");
        manager.stopServer();
    }

    @Test
    public void testPing() {
        System.out.println("testPing(): ");
        CommandResult result = manager.ping();
        System.out.println(result.getResult());
    }

    @Test
    public void testGetApplications() {
        Set<String> applications = manager.getApplications();
        System.out.println(applications);
    }

}
