package org.jfx4ee.adm4ee.business.server.control.glassfish;

import java.io.File;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
public class DomainXmlManagerTest {

    DomainXmlManager domainXml;

    @Before
    public void init() {
        try {
            // test file
            File f = new File("src/test/resources/domain.xml");
            domainXml = new DomainXmlManager();
            domainXml.parseFile(f);
        } catch (Exception ex) {
            System.err.println("ERROR: " + ex);
        }
    }

    @Test
    public void testGetAdminPort() {
        Integer adminPort = domainXml.getAdminPort();
        assertEquals(new Integer(4848), adminPort);
    }

    @Test
    public void testGetServicePort() {
        Integer httpPort = domainXml.getServicePort(ServiceType.HTTP);
        assertEquals(new Integer(8080), httpPort);
        Integer httpsPort = domainXml.getServicePort(ServiceType.HTTPS);
        assertEquals(new Integer(8181), httpsPort);
        Integer iiopPort = domainXml.getServicePort(ServiceType.IIOP);
        assertEquals(new Integer(3700), iiopPort);
    }

    @Test
    public void testSetServicePort() {
        int port = 1234;
        Integer newPort = domainXml.setServicePort(ServiceType.HTTP, port);
        assertEquals(new Integer(port), newPort);
        newPort = domainXml.setServicePort(ServiceType.JMS, port);
        assertNull(newPort);
    }

    @Test
    public void testGetServicePorts() {
        Map<String, Integer> servicePorts = domainXml.getServicePorts();
        assertTrue(servicePorts.size() > 0);
        assertTrue(servicePorts.get(ServiceType.ADMIN.name()) == 4848);
        System.out.println("ServicePorts: " + servicePorts);
    }
    
    @Test
    public void testSetServicePorts() {
        int portBase = 12000;
        domainXml.setServicePorts(portBase);
        assertEquals(new Integer(12048), domainXml.getAdminPort());
    }

    @Test
    public void testSetServicePortForPortBase() {
        int portBase = 9000;
        Integer port = domainXml.setServicePortForPortBase(ServiceType.HTTP, portBase);
        assertEquals(new Integer(9080), port);
        port = domainXml.setServicePortForPortBase(ServiceType.ADMIN, portBase);
        assertEquals(new Integer(9048), port);
    }

    @Ignore
    @Test
    public void testSave() {
        int portBase = 9000;
        domainXml.setServicePortForPortBase(ServiceType.HTTP, portBase);
        domainXml.save();
    }
}
